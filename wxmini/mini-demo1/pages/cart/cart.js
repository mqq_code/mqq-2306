// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 0,
    arr: ['a', 'b', 'c', 'd', 'e', 'f']
  },

  add(e) {
    console.log('获取自定义属性', e.currentTarget.dataset)
    const { num } = e.currentTarget.dataset
    this.setData({
      num: this.data.num + num
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})