import { ref, watch, onMounted, onBeforeUnmount } from 'vue'

function useCount(n = 10) {
  const num = ref(n)

  let timer = null
  const start = () => {
    timer = setInterval(() => {
      num.value--
      console.log(num.value)
      if (num.value === 0) {
        clearInterval(timer)
      }
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer)
  }
  const reset = () => {
    clearInterval(timer)
    num.value = 10
  }

  watch(num, () => {
    console.log('num改变了', num.value)
  })


  onMounted(() => {
    console.log('组件挂载完成')
  })
  onBeforeUnmount(() => {
    console.log('组件即将销毁')
    clearInterval(timer)
  })

  return {
    num,
    start,
    stop,
    reset
  }

}

export default useCount