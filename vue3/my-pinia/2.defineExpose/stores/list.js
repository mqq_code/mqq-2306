import { ref, reactive, computed } from 'vue'
import { defineStore } from 'pinia'

const useListStore = defineStore('list', () => {
  const title = ref('默认标题')
  const list = reactive([1,2,3])
  const total = computed(() => {
    return list.reduce((prev, val) => prev + val)
  })

  return {
    title,
    list,
    total
  }
})

export default useListStore