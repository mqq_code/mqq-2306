import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

const useCounterStore = defineStore('counter', () => {
  const num = ref(0)

  const add = () => {
    num.value ++
  }

  return {
    num,
    add
  }
})

export default useCounterStore