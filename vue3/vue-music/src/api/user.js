
import request from './request'

// 获取验证码
export const captchaSentApi = (phone) => {
  return request.post(`/captcha/sent?timestamp=${Date.now()}`, {
    phone
  })
}
// 校验验证码
export const captchaVerifyApi = ({ phone, captcha }) => {
  return request.post(`/captcha/verify?timestamp=${Date.now()}`, {
    phone,
    captcha
  })
}
// 手机号登录
export const loginCellphoneApi = ({ phone, captcha }) => {
  return request.get(`/login/cellphone`, {
    params: {
      phone,
      captcha
    }
  })
}

// 获取二维码的key
export const qrKeyApi = () => {
  return request.post(`/login/qr/key?timestamp=${Date.now()}`)
}
// 生成二维码
export const qrCreateApi = (key) => {
  return request.post(`/login/qr/create?timestamp=${Date.now()}`, {
    key,
    qrimg: 'qrimg'
  })
}
// 二维码检测扫码状态接口
export const qrCheckApi = (key) => {
  return request.post(`/login/qr/check?timestamp=${Date.now()}`, {
    key
  })
}
// 登录状态
export const loginStatusApi = () => {
  return request.get(`/login/status?timestamp=${Date.now()}`)
}
// 账号信息
export const userAccountApi = () => {
  return request.get(`/user/account?timestamp=${Date.now()}`)
}
