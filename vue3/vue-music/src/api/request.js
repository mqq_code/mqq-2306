import axios from 'axios'

// let HOST  = 'http://121.89.213.194'
// let HOST = 'https://zyxcl.xyz/music_api'
let HOST = '/mqq'

// 根据环境变量判断请求的接口域名
if (process.env.NODE_ENV === 'production') {
  HOST  = 'https://zyxcl.xyz/music_api'
}

// 创建一个 axios 实例
const request = axios.create({
  baseURL: HOST,
  timeout: 3000
})

// 添加请求拦截器，可以添加公共参数
request.interceptors.request.use(function (config) {
  // console.log(config)
  // if (config.params) {
  //   config.params.abc = 'ABCCCCCCCCC'
  // }
  // 在发送请求之前做些什么
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  // console.log('.then之前会先经过此函数')
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  return Promise.reject(error);
});


export default request