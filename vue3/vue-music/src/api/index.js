import request from './request'

// 轮播图
export const bannerApi = () => {
  return request.get('/banner')
}

// 推荐歌单
export const personalizedApi = (limit = 6) => {
  return request.get('/personalized', {
    params: {
      limit
    }
  })
}

// 主播电台
export const djprogramApi = (limit = 6) => {
  return request.get('/personalized/djprogram', {
    params: {
      limit
    }
  })
}
// 歌单详情
export const playlistDetailApi = (id) => {
  return request.get('/playlist/detail', {
    params: {
      id
    }
  })
}
// 歌单评论
export const commentPlaylistlApi = (id) => {
  return request.get('/comment/playlist', {
    params: {
      id
    }
  })
}
// 歌曲详情
export const songDetailApi = (ids) => {
  return request.get('/song/detail', {
    params: {
      ids
    }
  })
}
// 歌词
export const lyriclApi = (id) => {
  return request.get('/lyric', {
    params: {
      id
    }
  })
}
// 歌曲 url
export const getSongUrlApi = (id) => {
  return request.get('/song/url', {
    params: {
      id
    }
  })
}


