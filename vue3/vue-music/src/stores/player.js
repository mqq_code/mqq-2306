import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { getSongUrlApi } from '@/api'


export const usePlayerStore = defineStore('player', () => {
  // 播放列表
  const playlist = ref([])
  // 正在播放的下标
  const playIndex = ref(0)
  // 播放标签
  const audioRef = ref()
  // 是否暂停
  const paused = ref(true)
  // 当前歌曲总时长
  const duration = ref(0)
  // 歌曲当前播放位置
  const currentTime = ref(0)
  // 倍速
  const playbackRate = ref(1)

  // 正在播放的歌曲
  const curPlaySong = computed(() => {
    return playlist.value[playIndex.value]
  })

  // 播放
  const play = () => {
    if (audioRef.value.paused) {
      audioRef.value.play()
    } else {
      audioRef.value.pause()
    }
  }

  const getUrl = async () => {
    const ids = playlist.value.map(v => v.id).join(',')
    // 把当前播放列表的 url 存到playlist
    const res = await getSongUrlApi(ids)
    playlist.value.forEach(item => {
      item.url = res.data.data.find(v => v.id === item.id).url
    })
    // 自动播放歌曲
    audioRef.value.autoplay = true
  }
  // 添加一首歌到播放列表
  const addSong = async song => {
    const index = playlist.value.findIndex(v => v.id === song.id)
    if (index > -1) {
      playIndex.value = index
    } else {
      // 添加一首歌到播放列表
      playlist.value.push(song)
      // 获取播放地址，添加到播放列表
      const res = await getSongUrlApi(song.id)
      song.url = res.data.data[0].url
      playIndex.value = playlist.value.length - 1
    }
    // 自动播放歌曲
    audioRef.value.autoplay = true
  }
  // 添加一个歌单到播放列表
  const setPlayList = (songs) => {
    playlist.value = songs
    getUrl()
  }

  const getAudioRef = el => {
    audioRef.value = el
  }

  const next = () => {
    playIndex.value++
    if (playIndex.value > playlist.value.length - 1) {
      playIndex.value = 0
    }
  }
  const prev = () => {
    playIndex.value--
    if (playIndex.value < 0) {
      playIndex.value = playlist.value.length - 1
    }
  }
  const changeRate = () => {
    audioRef.value.playbackRate += 0.5
    if (audioRef.value.playbackRate > 5) {
      audioRef.value.playbackRate = 1
    }
    playbackRate.value = audioRef.value.playbackRate
  }
  

  return {
    playlist,
    playIndex,
    curPlaySong,
    audioRef,
    paused,
    duration,
    currentTime,
    playbackRate,
    addSong,
    setPlayList,
    getAudioRef,
    play,
    prev,
    next,
    changeRate
  }
})
