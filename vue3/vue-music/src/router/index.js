import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/home/Home.vue'
import First from '../views/home/first/First.vue'
import NewSong from '../views/home/first/newSong/NewSong.vue'
import SongList from '../views/home/first/songList/SongList.vue'
import TopList from '../views/home/first/topList/TopList.vue'
import Second from '../views/home/second/Second.vue'
import Third from '../views/home/third/Third.vue'
import Search from '../views/search/Search.vue'
import SongListDetail from '../views/songListDetail/SongListDetail.vue'
import Player from '../views/player/Player.vue'
import Login from '../views/login/Login.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/first',
      children: [
        {
          path: '/first',
          name: 'first',
          component: First,
          redirect: '/first/newSong',
          children: [
            {
              path: '/first/newSong',
              name: 'newSong',
              component: NewSong
            },
            {
              path: '/first/songList',
              name: 'songList',
              component: SongList
            },
            {
              path: '/first/topList',
              name: 'topList',
              component: TopList
            }
          ]
        },
        {
          path: '/second',
          name: 'second',
          component: Second
        },
        {
          path: '/third',
          name: 'third',
          component: Third
        }
      ]
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/songListDetail',
      name: 'songListDetail',
      component: SongListDetail
    },
    {
      path: '/player',
      name: 'player',
      component: Player
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})

export default router
