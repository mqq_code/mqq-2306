function format(date) {
  return new Date(date).toLocaleString()
}

// 单独抛出，后边必须跟一个变量声明，其他文件引用时变量名必须跟抛出的名字一致
export let a = 100
export const b = 'BBBb'
export const c = () => {
  console.log('cccccc')
}

// 默认抛出
export default format