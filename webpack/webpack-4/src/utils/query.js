export default function () {
  const arr = location.search.slice(1).split('&')
  const obj = {}
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })
  return obj
}