import axios from 'axios'
import query from '../utils/query'
import { $ } from '../utils/$'

const { id } = query()


axios.get(`https://zyxcl.xyz/music_api/song/detail?ids=${id}`)
.then(res => {
  console.log(res.data)
})


axios.get(`https://zyxcl.xyz/music_api/comment/music?id=${id}`)
.then(res => {
  console.log(res.data)
})

axios.get(`https://zyxcl.xyz/music_api/lyric?id=${id}`)
.then(res => {
  console.log(res.data)
})


const audio = $('audio')
axios.get(`https://zyxcl.xyz/music_api/song/url?id=${id}`)
.then(res => {
  audio.src = res.data.data[0].url
})


$('.play').addEventListener('click', () => {
  if (audio.paused) {
    audio.play()
  } else {
    audio.pause()
  }
})