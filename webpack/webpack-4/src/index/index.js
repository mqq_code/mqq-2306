import '../style/index.scss'
import { $, $gets } from '../utils/$'
import axios from 'axios'



$gets('nav .item').forEach((item, index) => {
  item.addEventListener('click', () => {
    $('nav .active').classList.remove('active')
    item.classList.add('active')
    $('.show').classList.remove('show')
    $gets('.content')[index].classList.add('show')
  })
})


// 热歌榜
function renderHot() {
  axios.get('https://zyxcl.xyz/music_api/playlist/detail?id=3778678')
  .then(res => {
    $('.hot').innerHTML = res.data.playlist.tracks.map((item, index) => {
      const ar = item.ar.map(v => v.name).join('/')
      return `
        <div class="hot-item" data-id="${item.id}">
          <div class="num">${index + 1}</div>
          <div class="hot-song">
            <h3>${item.name}</h3>
            <div class="ar">
              <span>${ar}</span> - <span>${item.al.name}</span>
            </div>
          </div>
        </div>
      `
    }).join('')
  })
}
renderHot()

$('.hot').addEventListener('click', e => {
  
  if (e.target.classList.contains('hot-item')) {
    const id = e.target.getAttribute('data-id')
    location.href = `./detail.html?id=${id}`
  }
})