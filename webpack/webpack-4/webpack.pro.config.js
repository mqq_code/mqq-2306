const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


// webpack 配置项，执行 webpack 命令时读取此文件的配置项
module.exports = {
  // 打包模式：development ｜ production
  mode: 'production',
  // webpack 执行时的入口文件
  // 多入口文件，一个出口 entry: ['./src/index.js', './src/detail.js', ...],
  // 多入口多出口，通过对象形式配置
  entry: {
    index: './src/index/index.js',
    detail: './src/detail/detail.js'
  },
  // 输出配置
  output: {
    // 输出的文件夹路径
    path: path.join(__dirname, 'build'),
    // 输出的文件名称
    filename: 'js/[name]_[hash:8].js',
    // 每次打包时自动删除上一次打包的内容
    clean: true,
    // 配置通过 asset 解析的文件名
    assetModuleFilename: 'assets/[name]_[hash:8].[ext]'
  },
  resolve: {
    // 配置路径别名
    alias: {
      // 配置文件夹别名
      '@': path.resolve(__dirname, 'src/')
    },
    // 自动解析文件后缀
    extensions: ['.js', '.json', '.scss'],
  },
  // 配置 loader（加载器），让 js 文件可以引入和解析其他类型的文件
  module: {
    rules: [
      // 解析 sass 和 css 文件, npm i style-loader css-loader sass-loader sass
      {
        test: /\.(css|sass|scss)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg)/i,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 30 * 1024 // 4kb
          }
        }
      },
      {
        test: /\.js$/,
        // 转译时排除的文件夹
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      }
    ]
  },
  // 配置插件
  plugins: [
    // webpack 打包时复制 html 文件到输出目录, 并且自动引入打包后的 js 文件
    new HtmlWebpackPlugin({
      template: './template/index.html',
      filename: 'index.html',
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      template: './template/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('pro'),
    }),
    // 提取 css 到单独的文件
    new MiniCssExtractPlugin({
      filename: "css/[name].css",
    })
  ]
}