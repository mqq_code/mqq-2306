function ajax(url) {
  const xhr = new XMLHttpRequest()
  xhr.open('get', url)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      console.log(xhr.responseText)
    }
  }
  xhr.send()
}

const a = 'ajax 文件'
console.log(a)

module.exports = ajax