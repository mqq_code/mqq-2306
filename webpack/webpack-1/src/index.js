// es module 规范
// 1. 引入 import
// 2. 抛出 export
import format, {
  // 引入 format 中单独抛出的变量
  b,
  c,
  // 改名字
  a as aaa
} from './utils/format'
import * as obj from './utils/sum'
import axios from 'axios'
import './scss/style.scss'
import img1 from './assets/1.png'
import img2 from './assets/2.jpeg'

// console.log(img1)

// const image1 = new Image()
// image1.src = img1
// image1.onload = () => {
//   document.body.appendChild(image1)
// }

// console.log(img1)

// const image2 = new Image()
// image2.src = img2
// image2.onload = () => {
//   document.body.appendChild(image2)
// }

console.log(obj)

// axios.get('https://www.baidu.com').then(res => {
//   console.log(res)
// })

// console.log(format)
// console.log(aaa, b)
// console.log(c)






// CommonJS 规范
// const ajax = require('./utils/ajax')




// console.log(sum(Math.random(), Math.random()))
// console.log(format(Date.now()))


document.querySelector('h1').style.color = 'red'

// ajax('https://www.baidu.com')


const a = '这是index.js'
console.log(a)
