const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// webpack 配置项，执行 webpack 命令时读取此文件的配置项
module.exports = {
  // 打包模式：development ｜ production
  mode: 'development',
  // 打包后和打包前的代码映射关系
  devtool: 'source-map',
  // webpack 执行时的入口文件
  entry: './src/index.js',
  // 输出配置
  output: {
    // 输出的文件夹路径
    path: path.join(__dirname, 'build'),
    // 输出的文件名称
    filename: 'main.js'
  },
  // 配置 loader（加载器），让 js 文件可以引入和解析其他类型的文件
  module: {
    rules: [
      // 解析 sass 和 css 文件, npm i style-loader css-loader sass-loader sass
      {
        test: /\.(css|sass|scss)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg)/i,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 30 * 1024 // 4kb
          }
        }
      }
    ]
  },
  // 配置插件
  plugins: [
    // webpack 打包时复制 html 文件到输出目录, 并且自动引入打包后的 js 文件
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    })
  ],
  // 配置本地开发服务应用
  devServer: {
    port: 8000,
    open: true
  }
}