import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Child1 from '../pages/Child1.vue'
import Child2 from '../pages/Child2.vue'
import Child3 from '../pages/Child3.vue'
import Child4 from '../pages/Child4.vue'
import Child5 from '../pages/Child5.vue'
// import Detail from '../pages/Detail.vue'
// import Player from '../pages/Player.vue'

import Login from '../pages/Login.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/child1',
      children: [
        {
          path: '/home/child1',
          name: 'child1',
          component: Child1,
          // meta: 路由元信息，可以存数据，在组件中可以通过 this.$route 获取
          meta: {
            title: '页面1'
          }
        },
        {
          path: '/home/child1/detail',
          name: 'detail',
          // 组件懒加载，打包时把此组件打包成单独的js文件，组件调用时加载对应的js文件
          component: () => import('../pages/Detail.vue'),
          meta: {
            title: '歌单列表'
          }
        },
        {
          path: '/home/child1/player/:id',
          name: 'player',
          component: () => import('../pages/Player.vue'),
          meta: {
            title: '歌曲详情'
          }
        },
        {
          path: '/home/child2',
          name: 'child2',
          component: Child2,
          meta: {
            title: '创建试卷'
          },
          // 路由独享守卫
          // beforeEnter: (to, from, next) => {
          //   console.log('from', from)
          //   console.log('to', to)
          //   next('/login')
          // }
        },
        {
          path: '/home/child3',
          name: 'child3',
          component: Child3,
          meta: {
            title: '页面3'
          }
        },
        {
          path: '/home/child4',
          name: 'child4',
          component: Child4,
          meta: {
            title: '页面4'
          }
        },
        {
          path: '/home/child5',
          name: 'child5',
          component: Child5,
          meta: {
            title: '页面5'
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: '登录'
      }
    }
  ]
})

// 路由全局前置守卫
// router.beforeEach((to, from, next) => {
//   // console.log('从哪来', from)
//   // console.log('到哪去', to)
//   const testtoken = localStorage.getItem('testtoken')
//   if (to.name === 'child1' || to.name === 'child2') {
//     if (!testtoken) {
//       next('/login')
//       return
//     }
//   }
//   next()
// })

// 路由全局后置守卫
// router.afterEach((to, from) => {
//   // ...
//   console.log(from, to)
//   document.title = to.meta.title
// })
export default router
