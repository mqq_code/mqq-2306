export default {
  data() {
    return {
      abcd: 'ABCDEFG',
      pos: {
        x: 0,
        y: 0
      }
    }
  },
  methods: {
    mouseMove(e) {
      console.log(e)
      this.pos = {
        x: e.clientX,
        y: e.clientY
      }
    },
    fn(n) {
      return n.toFixed(2)
    }
  },
  activated() {
    document.addEventListener('mousemove', this.mouseMove)
  },
  deactivated() {
    document.removeEventListener('mousemove', this.mouseMove)
  },
  created() {
    console.log('pos的 created')
  }
}