import Vue from 'vue'
import App from './App.vue'
import 'animate.css'
import './style/common.scss'
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
