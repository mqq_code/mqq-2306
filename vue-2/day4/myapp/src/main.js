import Vue from 'vue'
import App from './App.vue'
import 'animate.css'
import './style/common.scss'
Vue.config.productionTip = false

// 注册自定义指令
Vue.directive('loading', {
  // 初始化设置，只执行一次
  bind(el, binding) {
    if (binding.value) {
      el.classList.add('por')
      // 创建元素，存到el中
      el.$loadingBox = document.createElement('div')
      el.$loadingBox.className = 'loadingBox'
      el.$loadingBox.innerHTML = 'loading...'
      // 添加元素
      el.appendChild(el.$loadingBox)
    }
  },
  update(el, binding) {
    console.log('update', binding.value, binding.oldValue)
    if (binding.value === binding.oldValue) return
    if (binding.value) {
      el.classList.add('por')
      el.$loadingBox = document.createElement('div')
      el.$loadingBox.className = 'loadingBox'
      el.$loadingBox.innerHTML = 'loading...'
      el.appendChild(el.$loadingBox)
    } else {
      el.removeChild(el.$loadingBox)
      el.$loadingBox = null
    }
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app')
