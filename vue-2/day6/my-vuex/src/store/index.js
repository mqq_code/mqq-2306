import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import mouduleA from './modules/mouduleA'
import mouduleB from './modules/mouduleB'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存全局数据
  state: {
    num: 0,
    arr: [1,2,3,4,5,6],
    info: {
      username: '-'
    }
  },
  // 修改state的唯一方式，必须是同步函数
  mutations: {
    setInfo(state, payload) {
      state.info = payload
    },
    setNum(state, payload) {
      state.num += payload
    },
    addArr(state, payload) {
      state.arr.push(payload)
    }
  },
  // 处理异步
  actions: {
    async getUserInfo(context) {
      const res = await axios.get('https://zyxcl.xyz/exam_api/user/info',{
        headers: {
          Authorization: localStorage.getItem('token')
        }
      })
      context.commit('setInfo', res.data.data)
    }
  },
  // getter: 类似计算属性
  getters: {
    total(state) {
      return state.arr.reduce((prev, val) => prev + val) * state.num
    }
  },
  // 拆分模块
  modules: {
    mouduleA,
    mouduleB
  }
})
