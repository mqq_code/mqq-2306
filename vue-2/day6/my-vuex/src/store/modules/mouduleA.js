export default {
  // 命名空间
  namespaced: true,
  state: () => ({
    title: '模块1',
    num: 10
  }),
  mutations: {
    setNum(state, payload) {
      state.num += payload
    }
  },
  actions: {},
  getters: {}
}