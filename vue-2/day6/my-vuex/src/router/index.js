import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Child1 from '../views/Child1.vue'
import Child2 from '../views/Child2.vue'
import Child3 from '../views/Child3.vue'
import Child4 from '../views/Child4.vue'
import Child5 from '../views/Child5.vue'
import EditUser from '../views/EditUser.vue'

import Login from '../views/Login.vue'
import Notfound from '../views/404.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home,
    redirect: '/home/child1',
    children: [
      {
        path: '/home/child1',
        name: 'child1',
        component: Child1
      },
      {
        path: '/home/child2',
        name: 'child2',
        component: Child2
      },
      {
        path: '/home/child3',
        name: 'child3',
        component: Child3
      },
      {
        path: '/home/child4',
        name: 'child4',
        component: Child4
      },
      {
        path: '/home/child5',
        name: 'child5',
        component: Child5
      },
      {
        path: '/home/child5/edit',
        name: 'edit',
        component: EditUser
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '*',
    component: Notfound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
