import request from './request'

// 获取验证码接口
export const getCaptchaApi = () => {
  return request.get('/login/captcha')
}
// 登录接口
export const loginApi = (data = {}) => {
  return request.post('/login', data)
}
// 用户信息接口
export const userInfoApi = () => {
  return request.get('/user/info')
}
// 编辑用户信息
export const updateUserApi = (data = {}) => {
  return request.post('/user/update/info', data)
}
// 用户列表
export const userlistApi = (params = {}) => {
  return request.get('/user/list', { params })
}
// 角色列表
export const rolelistApi = (params = {}) => {
  return request.get('/role/list', { params })
}
// 权限列表
export const permissionApi = (params = {}) => {
  return request.get('/permission/list', { params })
}
// 题目类型
export const questionTypeApi = () => {
  return request.get('question/type/list')
}
// 科目列表
export const classifyListApi = (params = {}) => {
  return request.get('/classify/list', { params })
}
// 创建试题
export const createQuestionApi = (data = {}) => {
  return request.post('/question/create', data)
}
// 试题列表
export const getQuestionApi = (params = {}) => {
  return request.get('/question/list',{ params })
}
// 编辑题目
export const updateQuestionApi = (data = {}) => {
  return request.post('/question/update', data)
}
// 批量创建试题
export const createMultipleApi = (list = []) => {
  return request.post('/question/create/multiple', {
    list
  })
}

