import axios from 'axios'
import { Message } from 'element-ui'

// 根据环境变量判断请求哪个接口
const baseURL = process.env.NODE_ENV === 'development' ? '/mqq' : 'https://zyxcl.xyz/exam_api'

const request = axios.create({
  baseURL: baseURL, // 设置默认的接口前缀
  timeout: 5000 // 超时时间
})

// 请求拦截器
request.interceptors.request.use(config => {
  console.log('请求拦截器', config)
  // 把 token 统一传给接口
  config.headers.Authorization = localStorage.getItem('token')
  return config
}, err => {
  return Promise.reject(Error(err))
})

// 响应拦截器
request.interceptors.response.use(response => {
  console.log('响应拦截器', response)
  return response.data
}, err => {
  // 统一处理接口错误信息
  switch(err.response.status) {
    case 500:
      Message.error('网络错误，请稍后刷新重试！')
      return
    case 401:
      Message.error('登录失效，请重新登录！')
      window.location.href = '/login'
      return
    case 403:
      Message.error('没有权限，请联系管理员！')
      return
  }
  return Promise.reject(Error(err))
})

export default request