import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import First from '../views/First.vue'
import UserInfo from '../views/UserInfo.vue'
import EditUser from '../views/EditUser.vue'
import UserList from '../views/UserList.vue'
import RoleList from '../views/RoleList.vue'
import Permission from '../views/Permission.vue'
import ClassList from '../views/ClassList.vue'
import StudentList from '../views/StudentList.vue'
import CreateClassify from '../views/CreateClassify.vue'
import CreateQuestion from '../views/CreateQuestion.vue'
import QuestionList from '../views/QuestionList.vue'
import TestPaper from '../views/TestPaper.vue'
import CreateTestPaper from '../views/CreateTestPaper.vue'
import ExamList from '../views/ExamList.vue'
import CreateExam from '../views/CreateExam.vue'
import Login from '../views/Login.vue'
import Notfound from '../views/404.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home,
    redirect: '/home/first',
    // 路由元信息
    meta: {
      isAuth: true
    },
    children: [
      {
        path: '/home/first',
        name: 'first',
        component: First
      },
      {
        path: '/home/userInfo',
        name: 'userInfo',
        component: UserInfo
      },
      {
        path: '/home/userInfo/edit',
        name: 'edit',
        component: EditUser
      },
      {
        path: '/home/userlist',
        name: 'userlist',
        component: UserList
      },
      {
        path: '/home/rolelist',
        name: 'rolelist',
        component: RoleList
      },
      {
        path: '/home/permission',
        name: 'permission',
        component: Permission
      },
      {
        path: '/home/classlist',
        name: 'classlist',
        component: ClassList
      },
      {
        path: '/home/studentlist',
        name: 'studentlist',
        component: StudentList
      },
      {
        path: '/home/createclassify',
        name: 'createclassify',
        component: CreateClassify
      },
      {
        path: '/home/createquestion',
        name: 'createquestion',
        component: CreateQuestion
      },
      {
        path: '/home/questionlist',
        name: 'questionlist',
        component: QuestionList
      },
      {
        path: '/home/testpaper',
        name: 'testpaper',
        component: TestPaper
      },
      {
        path: '/home/createtestpaper',
        name: 'createtestpaper',
        component: CreateTestPaper
      },
      {
        path: '/home/examlist',
        name: 'examlist',
        component: ExamList
      },
      {
        path: '/home/createexam',
        name: 'createexam',
        component: CreateExam
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '*',
    component: Notfound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // 判断是否需要登录
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next('/login')
      return
    }
  }
  next()
})
export default router
