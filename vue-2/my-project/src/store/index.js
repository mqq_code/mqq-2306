import Vue from 'vue'
import Vuex from 'vuex'
import { userInfoApi } from '../api'
Vue.use(Vuex)

export default new Vuex.Store({
  // 存全局数据
  state: {
    info: {
      username: '-'
    }
  },
  // 修改state的唯一方式，必须是同步函数
  mutations: {
    setInfo(state, payload) {
      state.info = payload
    }
  },
  // 处理异步
  actions: {
    async getUserInfo(context) {
      try {
        const res = await userInfoApi()
        context.commit('setInfo', res.data)
      } catch(e) {
        console.log(e)
      }
    }
  }
})
