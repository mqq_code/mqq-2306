const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/mqq': {
        // target: 'http://localhost:3001',
        target: 'https://zyxcl.xyz/exam_api',
        pathRewrite: { '^/mqq': '' },
        changeOrigin: true
      }
    }
  }
})
