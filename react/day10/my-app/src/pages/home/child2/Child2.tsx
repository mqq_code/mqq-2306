import React from 'react'
import { Row, Col } from 'antd'

const Child2: React.FC = () => {
  return (
    <div>
      <h2>Child2</h2>
      <Row gutter={20}>
        <Col span={24}><div className="box">24</div></Col>
      </Row>
      <Row gutter={20}>
        <Col span={12}><div className="box">12</div></Col>
        <Col span={12}><div className="box green">12</div></Col>
      </Row>
      <Row gutter={20}>
        <Col span={8}><div className="box">8</div></Col>
        <Col span={8}><div className="box green">8</div></Col>
        <Col span={8}><div className="box">8</div></Col>
      </Row>
      <Row gutter={20}>
        <Col span={6}><div className="box">6</div></Col>
        <Col span={6}><div className="box green">6</div></Col>
        <Col span={6}><div className="box">6</div></Col>
        <Col span={6}><div className="box green">6</div></Col>
      </Row>
      <Row gutter={20}>
        <Col span={8} offset={6}><div className="box">6</div></Col>
        <Col span={4} offset={6}><div className="box green">6</div></Col>
      </Row>
    </div>
  )
}

export default Child2