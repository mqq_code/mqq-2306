import React, { useEffect, useState } from 'react'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Space, Table, Tag, Button, message, Modal } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import axios from 'axios'

interface DataType {
  id: string;
  name: string;
  age: number;
  address: string;
  sex: 0 | 1 | 2;
  no: number;
  image: string;
  num: number;
}

const Sex = {
  0: { text: '男', color: 'green' },
  1: { text: '女', color: 'red' },
  2: { text: '其他', color: 'volcano' }
}

const Child1: React.FC = () => {
  const [data, setData] = useState<DataType[]>([])
  const [total, setTotal] = useState(0)
  const [query, setQuery] = useState({ page: 1, pagesize: 5 })
  const [modal, contextHolder] = Modal.useModal();

  const del = async (id: string, name: string) => {
    modal.confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: <div>确定删除 <b style={{ color: 'red' }}>{name}</b> 吗？</div>,
      okText: '删除',
      cancelText: '取消',
      onCancel: () => {
        message.warning('取消删除')
      },
      onOk: async () => {
        const res = await axios.post('/api/del', { id })
        if (res.data.code === 0) {
          message.success(res.data.msg)
          getList()
        } else {
          message.error(res.data.msg)
        }
      }
    });
  }

  const columns: ColumnsType<DataType> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '序号',
      dataIndex: 'no',
      key: 'no',
    },
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
      render: text => <i>{text}</i>
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (_, record) => {
        const cur = Sex[record.sex]
        return <Tag color={cur.color}>{cur.text}</Tag>
      }
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button type='primary' size='small'>编辑</Button>
          <Button danger size='small' onClick={() => del(record.id, record.name)}>删除</Button>
        </Space>
      ),
    },
  ];

  const getList = async () => {
    const res = await axios.post<{ total: number; values: DataType[] }>('/api/list', query)
    console.log(res.data)
    setData(res.data.values)
    setTotal(res.data.total)
  }

  useEffect(() => {
    getList()
  }, [query])

  return (
    <div>
      <h2>Child1</h2>
      <Table
        bordered
        columns={columns}
        dataSource={data}
        rowKey="id"
        pagination={{
          total,
          current: query.page,
          pageSize: query.pagesize,
          pageSizeOptions: [5, 10, 20, 50],
          onChange: (page, pagesize) => {
            setQuery({ page, pagesize })
          }
        }}
      />
      {contextHolder}
    </div>
  )
}

export default Child1