import React, { useMemo, useState } from 'react'
import style from './Home.module.scss'
import { Outlet } from 'react-router-dom'
import {
  DesktopOutlined,
  FileOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  DownOutlined,
  SmileOutlined
} from '@ant-design/icons';
import { Link, useLocation, useNavigate } from 'react-router-dom'
import type { MenuProps } from 'antd';
import { Breadcrumb, Layout, Menu, Avatar, Image, Dropdown, Space } from 'antd';
import img from '../../assets/1.jpeg'

const { Header, Content, Footer, Sider } = Layout;

type MenuItem1 = Required<MenuProps>['items'][number];

type MenuItem = {
  label: string;
  title?: string;
  key: string;
  icon?: React.ReactNode;
  children?: MenuItem1[]
}

const items: MenuItem[] = [
  {
    label: '分类一',
    title: '分类1',
    key: '1',
    icon: <DesktopOutlined />,
    children: [
      {
        label: <Link to="/">菜单一</Link>,
        title: '菜单1',
        key: '/',
        icon: <DesktopOutlined />
      },
      {
        label: <Link to="/child2">菜单二</Link>,
        title: '菜单2',
        key: '/child2',
        icon: <FileOutlined />
      },
    ]
  },
  {
    label: '分类二',
    title: '分类2',
    key: '2',
    icon: <UserOutlined />,
    children: [
      {
        label: <Link to="/child3">菜单三</Link>,
        title: '菜单3',
        key: '/child3',
        icon: <PieChartOutlined />
      },
      {
        label: <Link to="/child4">菜单四</Link>,
        title: '菜单4',
        key: '/child4',
        icon: <TeamOutlined />
      }
    ]
  }
]

const getParendKey = (key: string) => {
  const cur = items.find(item => {
    return (item!.children as any[]).find((v: any) => v.key === key)
  })
  return cur!.key
}

const Home: React.FC = () => {
  const [collapsed, setCollapsed] = useState(false)
  const location = useLocation()
  const navigate = useNavigate()

  const DropdownItems: MenuProps['items'] = [
    {
      key: '1',
      label: <span onClick={() => console.log('点击了个人信息')}>个人信息</span>,
      icon: <UserOutlined />,
    },
    {
      key: '2',
      label: <span onClick={() => navigate('/login')}>退出登录</span>,
      icon: <SmileOutlined />,
      danger: true
    }
  ];


  const BreadcrumbList = useMemo(() => {
    const arr: any[] = []
    items.forEach(item => {
      (item.children as any).forEach((val: any) => {
        if (val.key === location.pathname) {
          arr.push(item, val)
        }
      })
    })
    return arr
  }, [location.pathname])
  console.log(BreadcrumbList)

  return (
    <Layout className={style.home}>
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
        <div className={style.logo}>Logo</div>
        <Menu
          theme="dark"
          mode="inline"
          items={items}
          defaultSelectedKeys={[location.pathname]}
          defaultOpenKeys={[getParendKey(location.pathname)]}
        />
      </Sider>
      <Layout className={style.right}>
        <Header className={style.header}>
          <Space>
            <Avatar size={40} src={<Image width={40} src={img} />}>zyx</Avatar>
            <Dropdown menu={{ items: DropdownItems }}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>zyx<DownOutlined /></Space>
              </a>
            </Dropdown>
          </Space>
        </Header>
        <Content className={style.content}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            {BreadcrumbList.map(item =>
              <Breadcrumb.Item key={item.key}>{item.icon} {item.label}</Breadcrumb.Item>
            )}
          </Breadcrumb>
          <div className={style.outletWrap}>
            <Outlet />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2023 Created by Ant UED</Footer>
      </Layout>
    </Layout>
  )
}

export default Home