import React from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, message } from 'antd';
import style from './Login.module.scss'
import { useNavigate } from 'react-router-dom'

const Login: React.FC = () => {
  // 创建form实例
  const [loginForm] = Form.useForm()
  const navigate = useNavigate()
  const login = async () => {
    try {
      // 手动触发表单校验
      const res = await loginForm.validateFields()
      console.log(res)
      message.success('登录成功')
      navigate('/')
    } catch(e) {
      console.log(e)
    }
  }

  return (
    <div className={style.login}>
      <Form form={loginForm} className={style.form}>
        <h2>登录</h2>
        <Form.Item
          name="user"
          rules={[
            // { required: true, message: '请输入用户名!', validateTrigger: ['blur'] },
            // { max: 5, message: '输入的位数不能超过5位!' },
            // { min: 2, message: '输入的位数不能小于2位!' },
            // { pattern: /^1[3-9]\d{9}$/, message: '手机号格式错误' }
            // { type: 'email', message: '邮箱格式错误', validateTrigger: ['blur']}
            ({ getFieldValue }) => {
              return {
                validator(_, value) {
                  // 自定义校验
                  if (value === '1234' && getFieldValue('pwd') === '3456') {
                    return Promise.resolve()
                  }
                  return Promise.reject('校验失败')
                },
              }
            },
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[{ required: true, message: '请输入密码!' }]}
        >
          <Input.Password prefix={<LockOutlined/>} placeholder="密码" />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>记住账号</Checkbox>
          </Form.Item>
          <a className={style.forget}>忘记密码</a>
        </Form.Item>

        <Form.Item>
        {/* <Button block type="primary" htmlType='submit'>登录</Button> */}
          <Button block type="primary" onClick={login}>登录</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login

