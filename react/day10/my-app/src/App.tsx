import React from 'react'
import Home from './pages/home/Home'
import Child1 from './pages/home/child1/Child1'
import Child2 from './pages/home/child2/Child2'
import Child3 from './pages/home/child3/Child3'
import Child4 from './pages/home/child4/Child4'
import Login from './pages/login/Login'
import { useRoutes } from 'react-router-dom'

const App: React.FC = () => {
  const router = useRoutes([
    {
      path: '/',
      element: <Home />,
      children: [
        {
          path: '/',
          element: <Child1 />,
        },
        {
          path: '/child2',
          element: <Child2 />,
        },
        {
          path: '/child3',
          element: <Child3 />,
        },
        {
          path: '/child4',
          element: <Child4 />,
        }
      ]
    },
    {
      path: '/login',
      element: <Login />
    }
  ])
  return router
}

export default App