import Mock from 'mockjs'
import data from './data.json'

Mock.mock('/api/list', 'post', (options) => {
  const { page, pagesize } = JSON.parse(options.body)
  return {
    total: data.length,
    values: data.slice((page - 1) * pagesize, page * pagesize)
  }
})

Mock.mock('/api/del', 'post', (options) => {
  const { id } = JSON.parse(options.body)
  const index = data.findIndex(v => v.id === id)
  if (index > -1) {
    data.splice(index, 1)
    return {
      code: 0,
      msg: '成功'
    }
  }
  return {
    code: -1,
    msg: '删除失败，参数错误'
  }
})