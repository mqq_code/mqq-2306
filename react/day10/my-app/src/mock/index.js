const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

const data = Mock.mock({
  "list|500": [
    {
      "id": "@id",
      "no|+1": 1,
      "name": "@cname",
      "age|18-35": 18,
      "sex|0-2": 0,
      "address": "@city(true)",
      "image": "@image(100x100)",
      "num|0-100": 100 
    }
  ]
})

fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data.list))