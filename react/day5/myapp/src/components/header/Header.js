import React from 'react'
import style from './Header.module.scss'
import classNames from 'classnames'

const Header = (props) => {
  return (
    <div className={style.header}>
      <button
        className={classNames({ [style.active]: props.list.length > 1 })}
        onClick={props.onClick}
      >比较</button>
      <ul>
        {props.list.map(item =>
          <li key={item.title} className={style.isImg}>
            <img src={item.src} alt="" />
            <span onClick={() => props.changeHeader(item.title)}>x</span>
          </li>
        )}
        {props.list.length < 3 && <li></li>}
      </ul>
    </div>
  )
}

export default Header