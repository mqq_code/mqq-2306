import React from 'react'
import style from './Goods.module.scss'
import classNames from 'classnames'

const Goods = (props) => {
  return (
    <div className={classNames(style.goods, { [style.active]: props.isActive })}>
      <img src={props.src} alt="" />
      <h3>{props.title}</h3>
      <p>{props.price}</p>
      <button>Add to cart</button>
      <span onClick={() => props.addHeader(props.title)}>
        {props.isActive ? '✅' : '+'}
      </span>
    </div>
  )
}

export default Goods