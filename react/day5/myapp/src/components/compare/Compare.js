import React, { useState } from 'react'
import style from './Compare.module.scss'
import classNames from 'classnames'

const Compare = (props) => {
  const [isClose, setIsClose] = useState(false)
  return (
    <div className={classNames(style.compare, { [style.isClose]: isClose })}>
      {props.list.map(item =>
        <div key={item.title} className={style.item}>
          <img src={item.src} alt="" />
          <h2>{item.title}</h2>
          <p>{item.price}</p>
          <p>{item.alcohol}</p>
          <p>{item.region}</p>
          <p>{item.varietal}</p>
          <p>{item.year}</p>
        </div>
      )}
      <div className={style.close} onClick={() => {
        setIsClose(true)
        setTimeout(() => {
          props.onClose()
        }, 1000)
      }}>x</div>
    </div>
  )
}

export default Compare