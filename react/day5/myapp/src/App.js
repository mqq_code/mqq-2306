import React, { useState, useEffect } from 'react'
import style from './App.module.scss'
import axios from 'axios'
import Goods from './components/goods/Goods'
import Header from './components/header/Header'
import Compare from './components/compare/Compare'
const App = () => {
  const [list, setList] = useState([])
  const [headerList, setHeaderList] = useState([])
  const [showCompare, setShowCompare] = useState(false)

  const getList = async () => {
    const res = await axios.get('https://zyxcl.xyz/exam_api/bottle')
    setList(res.data.value)
  }
  useEffect(() => {
    getList()
  }, [])

  const changeHeader = (title) => {
    // 拷贝数据
    const newList = [...list]
    // 根据点击的标题查找下标
    const index = list.findIndex(v => v.title === title)
    // 头部列表超过3个并且当前点击的元素没有高亮就禁止添加
    if (headerList.length >= 3 && !newList[index].isActive) {
      return
    }
    // 修改当前高亮
    newList[index].isActive = !newList[index].isActive
    // 更新数据
    setList(newList)
    // 判断当前点击的是否高亮
    if (newList[index].isActive) {
      // 如果高亮就添加到头部列表
      setHeaderList([...headerList, newList[index]])
    } else {
      // 取消高亮就头部列表，查找下标，删除
      const newHeaderList = [...headerList]
      const index = newHeaderList.findIndex(v => v.title === title)
      newHeaderList.splice(index, 1)
      setHeaderList(newHeaderList)
    }
  }

  const show = () => {
    if (headerList.length > 1) {
      setShowCompare(true)
    }
  }
  const close = () => {
    setShowCompare(false)
  }

  return (
    <div className={style.app}>
      {headerList.length > 0 && <Header list={headerList} changeHeader={changeHeader} onClick={show} />}
      <main>
        {list.map(item =>
          <Goods key={item.title} {...item} addHeader={changeHeader} />
        )}
      </main>
      {showCompare && <Compare list={headerList} onClose={close}/>}
    </div>
  )
}


export default App
