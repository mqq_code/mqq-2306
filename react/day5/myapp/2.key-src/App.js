import React, { useState, Component, createContext } from 'react'


const App = () => {
  const [arr, setArr] = useState([{id: 1, text: '1111111'}])
  const [text, setText] = useState('')
  const add = () => {
    setArr([{ id: Date.now(), text }, ...arr])
    setText('')
  }
  return (
    <div>
      <h1>App</h1>
      <input type="text" value={text} onChange={e => setText(e.target.value)} />
      <button onClick={add}>添加</button>
      <ul>
        {arr.map((item, index) =>
          // key必须是唯一值，是元素的唯一标识，提高diff算法的效率
          <li key={item.id}>
            <input type="checkbox" />
            {item.text}
          </li>
        )}
      </ul>
    </div>
  )
}

// key 是唯一值
// 更新前 a b c d e
// 更新后 a b d

// key 是下标
// 更新前 0
// 更新后 0 1


export default App
