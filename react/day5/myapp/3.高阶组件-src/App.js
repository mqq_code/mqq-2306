import React, { useState, Component, createContext } from 'react'
import Child1 from './components/Child1'

const App = () => {

  return (
    <div>
      <h1>App</h1>
      <Child1 />
    </div>
  )
}


export default App
