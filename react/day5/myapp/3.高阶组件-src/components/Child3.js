import React, { Component } from 'react'
import withCountDown from '../hoc/withCountDown'

class Child3 extends Component {

  render() {
    console.log('通过props使用高阶组件传过来的数据', this.props)
    return (
      <div className='box'>
        <h1>Child3</h1>
        <p>倒计时：{this.props.num}</p>
        <button onClick={this.props.start}>开始</button>
        <button onClick={this.props.stop}>暂停</button>
        <button onClick={this.props.reset}>重置</button>

        <table border="1">
          <thead>
            <tr>
              <th>111</th>
              <th>222</th>
              <th>333</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>111</td>
              <td>222</td>
              <td>333</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default withCountDown(Child3)
