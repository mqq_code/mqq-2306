import React, { Component } from 'react'
import Child3 from './Child3'

export default class Child2 extends Component {
  render() {
    return (
      <div className='box'>
        <h1>Child2</h1>
        <Child3 title="AABBCCDD" />
      </div>
    )
  }
}
