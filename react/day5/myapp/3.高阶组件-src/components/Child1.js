import React, { Component } from 'react'
import Child2 from './Child2'
import withCountDown from '../hoc/withCountDown'

class Child1 extends Component {

  render() {
    console.log(this.props)
    return (
      <div className='box'>
        <h1>Child1 - {this.props.num}秒</h1>
        <p>姓名： <input type="text" /></p>
        <button onClick={this.props.start}>获取验证码</button>
        <Child2 />
      </div>
    )
  }
}

export default withCountDown(Child1)
