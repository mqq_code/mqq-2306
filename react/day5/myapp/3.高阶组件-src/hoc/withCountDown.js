import React from 'react'

// 高阶组件：是一个参数为组件，返回值为新组件的函数，可以抽离公用逻辑

function withCountDown(Com) {

  class CountDown extends React.Component {

    state = {
      num: 10
    }
    start = () => {
      this.timer = setInterval(() => {
        if (this.state.num - 1 <= 0) {
          clearInterval(this.timer)
        }
        this.setState({
          num: this.state.num - 1
        })
      }, 1000);
    }
    stop = () => {
      clearInterval(this.timer)
    }
    reset = () => {
      this.setState({
        num: 10
      })
      clearInterval(this.timer)
    }
    componentWillUnmount() {
      clearInterval(this.timer)
    }

    render() {
      const newProps = {
        num: this.state.num,
        start: this.start,
        stop: this.stop,
        reset: this.reset
      }
      return (
        <Com {...newProps} {...this.props}  />
      )
    }
  }
  return CountDown
}


export default withCountDown