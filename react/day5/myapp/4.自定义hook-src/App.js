import React, { useState, Component, createContext } from 'react'
import Child1 from './components/Child1'
import useMousePos from './hooks/useMousePos'

const App = () => {
  const { x, y } = useMousePos()
  return (
    <div>
      <h1>App</h1>
      <p>当前鼠标位置：x: {x}, y: {y}</p>
      <Child1 />
    </div>
  )
}


export default App
