import { useState, useRef, useEffect } from 'react'

// 函数组件使用自定义hook抽取公用逻辑
// 自定义hook：名字以 use 开头的函数，函数内部可以使用 hook
const useCountDown = (n = 10) => {
  const [num, setNum] = useState(n)
  const timer = useRef(null)

  const start = () => {
    timer.current = setInterval(() => {
      setNum(n => {
        if (n - 1 <= 0) {
          clearInterval(timer.current)
        }
        return n - 1
      })
    }, 1000);
  }
  const stop = () => clearInterval(timer.current)
  const reset = () => {
    clearInterval(timer.current)
    setNum(n)
  }
  useEffect(() => {
    return () => {
      clearInterval(timer.current)
    }
  }, [])

  return {
    num,
    start,
    stop,
    reset
  }
}

export default useCountDown