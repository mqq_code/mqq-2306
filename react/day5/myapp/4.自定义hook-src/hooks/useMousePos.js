import { useState, useEffect } from 'react'


const useMousePos = () => {
  const [pos, setPos] = useState({ x: 0, y: 0 })

  useEffect(() => {
    const mousemove = (e) => {
      setPos({ x: e.clientX, y: e.clientY })
    }
    document.addEventListener('mousemove', mousemove)
    return () => {
      document.removeEventListener('mousemove', mousemove)
    }
  }, [])

  return pos
}

export default useMousePos