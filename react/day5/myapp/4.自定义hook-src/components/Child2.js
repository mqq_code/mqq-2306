import React from 'react'
import Child3 from './Child3'
const Child2 = () => {
  return (
    <div className='box'>
      <h1>Child2</h1>
      <Child3 title="AABBCCDD" />
    </div>
  )
}

export default Child2