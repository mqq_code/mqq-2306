import React, { useState, useRef, useEffect } from 'react'
import useCountDown from '../hooks/useCountDown'

const Child3 = (props) => {
  
  const { num, start, stop, reset } = useCountDown()

  return (
    <div className='box'>
      <h1>Child3</h1>
      <p>倒计时：{num}</p>
      <button onClick={start}>开始</button>
      <button onClick={stop}>暂停</button>
      <button onClick={reset}>重置</button>

      <table border="1">
        <thead>
          <tr>
            <th>111</th>
            <th>222</th>
            <th>333</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>111</td>
            <td>222</td>
            <td>333</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}


export default Child3
