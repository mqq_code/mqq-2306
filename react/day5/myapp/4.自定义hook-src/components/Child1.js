import React from 'react'
import Child2 from './Child2'
import useCountDown from '../hooks/useCountDown'

const Child1 = () => {
  const { num, start } = useCountDown(100)

  return (
    <div className='box'>
      <h1>Child1 - {num}秒</h1>
      <p>姓名： <input type="text" /></p>
      <button onClick={start}>获取验证码</button>
      <Child2 />
    </div>
  )
}

export default Child1