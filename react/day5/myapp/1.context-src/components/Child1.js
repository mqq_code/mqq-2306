import React, { Component } from 'react'
import Child2 from './Child2'

export default class Child1 extends Component {
  render() {
    return (
      <div className='box'>
        <h1>Child1</h1>
        <Child2 />
      </div>
    )
  }
}
