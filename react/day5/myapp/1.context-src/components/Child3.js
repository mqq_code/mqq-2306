import React, { useContext, Component } from 'react'
import ctx from '../context/ctx'

const Child3 = () => {
  const value = useContext(ctx)
  console.log(value)

  return (
    <div className='box'>
      <h1>Child3</h1>
      <p>
        <button onClick={() => value.changeCount(-1)}>-</button>
        {value.num}
        <button onClick={() => value.changeCount(-1)}>+</button>
      </p>
    </div>
  )
}

export default Child3

// export default class Child3 extends Component {

//   render() {
//     return (
//       <ctx.Consumer>
//         {(value) => {
//           // value: 接收通过Provider传过来的数据
//           console.log(value)
//           return (
//             <div className='box'>
//               <h1>Child3</h1>
//               <p>
//                 <button onClick={() => value.changeCount(-1)}>-</button>
//                 {value.num}
//                 <button onClick={() => value.changeCount(-1)}>+</button>
//               </p>
//             </div>
//           )
//         }}
//       </ctx.Consumer>
//     )
//   }
// }
