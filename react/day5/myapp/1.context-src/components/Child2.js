import React, { Component } from 'react'
import Child3 from './Child3'
import ctx from '../context/ctx'

export default class Child2 extends Component {
  render() {
    return (
      <ctx.Consumer>
        {vlaue => (
          <div className='box'>
            <h1>Child2</h1>
            <p>
              <button onClick={() => vlaue.changeCount(-1)}>-</button>
              {vlaue.num}
              <button onClick={() => vlaue.changeCount(1)}>+</button>
            </p>
            <Child3 />
          </div>
        )}
      </ctx.Consumer>
    )
  }
}
