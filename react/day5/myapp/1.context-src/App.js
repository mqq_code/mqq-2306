import React, { useState, Component, createContext } from 'react'
import Child1 from './components/Child1'
import ctx from './context/ctx'


const App = () => {
  const [num, setNum] = useState(0)
  const changeCount = n => {
    setNum(num + n)
  }
  
  const providerValue = {
    num,
    changeCount,
    a: 'aaaaaa'
  }

  return (
    // ctx.Provider: 给所有的后代组件传数据
    <ctx.Provider value={providerValue}>
      <div>
        <h1>App</h1>
        <button onClick={() => changeCount(-1)}>-</button>
        {num}
        <button onClick={() => changeCount(1)}>+</button>
        <Child1 />
      </div>
    </ctx.Provider>
  )
}

export default App

// export default class App extends Component {
//   state = {
//     num: 0
//   }
//   changeCount = n => {
//     this.setState({
//       num: this.state.num + n
//     })
//   }
//   render() {
//     const providerValue = {
//       num: this.state.num,
//       changeCount: this.changeCount,
//       flag: true,
//       arr: [1,2,3,4,5]
//     }

//     return (
//       // ctx.Provider: 给所有的后代组件传数据
//       <ctx.Provider value={providerValue}>
//         <div>
//           <h1>App</h1>
//           <button onClick={() => this.changeCount(-1)}>-</button>
//           {this.state.num}
//           <button onClick={() => this.changeCount(1)}>+</button>
//           <Child1 />
//         </div>
//       </ctx.Provider>
//     )
//   }
// }
