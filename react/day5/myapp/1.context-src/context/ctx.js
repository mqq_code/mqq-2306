import { createContext } from 'react'

// 创建 context 对象
const ctx = createContext()

export default ctx