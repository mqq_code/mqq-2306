import React, { Component } from 'react'
import ClassNames from 'classnames'

// react、vue: 数据驱动视图改变

class App extends Component {

  // 定义组件数据
  state = {
    curIndex: 0,
    tabs: [
      {
        title: '徐良',
        songs: ['坏女孩', '客官不可以', '犯贱', '后会无期']
      },
      {
        title: '许嵩',
        songs: ['素颜', '有何不可', '断桥残雪', '半城烟沙']
      },
      {
        title: '汪苏泷',
        songs: ['有点甜', '一笑倾城', '小星星', '风度']
      }
    ]
  }


  render() {
    const { tabs, curIndex } = this.state
    return (
      <div>
        <h1>tab切换</h1>
        <nav>
          {tabs.map((item, index) => 
            <span
              className={ClassNames('abc', { active: curIndex === index })}
              key={item.title}
              onClick={() => {
                console.log(index)
                this.setState({ curIndex: index })
              }}
            >
              {item.title} - {index}
            </span>
          )}
        </nav>
        <ul>
          {tabs[curIndex].songs.map(song =>
            <li key={song}>{song}</li>
          )}
        </ul>
      </div>
    )
  }
}

export default App
