import React, { Component } from 'react'

// jsx 语法是 React.createElement 的语法糖

// 组件：复用逻辑和布局
// class 组件
// 函数组件

// extends 继承 Component
class App extends Component {

  // 定义组件数据
  state = {
    num: 10,
    title: '我是标题'
  }

  sub = () => {
    this.setState({
      num: this.state.num - 1
    })
  }

  add = () => {
    console.log('调用了 add 函数', this.state.num)
    // 改变state数据更新页面需要使用 this.setState()
    // setState 会把传入的对象和原本的state进行合并，然后更新组件数据
    this.setState({
      num: this.state.num + 1
    })
  }

  changeNum = (n) => {
    this.setState({
      num: this.state.num + n
    })
  }

  // 必须定义render函数，返回react元素
  render() {
    // this => 组件实例对象
    console.log('render', this)

    return (
      <div>
        <h1>{this.state.title}</h1>
        <p>
          {/* <button onClick={this.sub}>-</button> */}
          <button onClick={() => this.changeNum(-1)}>-</button>
          {this.state.num}
          <button onClick={() => this.changeNum(1)}>+</button>
        </p>
        <ul>
          <li>1</li>
          <li>2</li>
        </ul>
      </div>
      // React.createElement('div', {}, ['App'])
    )
  }
}

export default App












// import React from 'react'

// const App = () => {
//   return (
//     <div>函数组件</div>
//   )
// }

// export default App




// class 是 es5 构造函数写法的语法糖
// class Person {
// }

// function Person() {
//   console.log(this)
// }

// const xm = new Person()