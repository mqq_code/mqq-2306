import React, { Component, createRef } from 'react'
import ClassNames from 'classnames'

class App extends Component {

  state = {
    arr: []
  }
  inp = createRef()
  push = () => {
    console.log('添加数据', this.inp.current.value)
    this.setState({
      arr: [
        ...this.state.arr,
        {
          id: Date.now(),
          text: this.inp.current.value
        }
      ]
    })
    this.inp.current.value = ''
  }
  remove = (id) => {
    // 拷贝数据
    const newArr = [...this.state.arr]
    // 从数组中根据id查找下表
    const index = newArr.findIndex(v => v.id === id)
    // 根据id删除
    newArr.splice(index, 1)
    // 更新页面
    this.setState({
      arr: newArr
    })
  }
  render() {
    const { arr } = this.state
    return (
      <div>
        <h1>todolist</h1>
        <input ref={this.inp} type="text" onKeyDown={(e) => {
          if (e.keyCode === 13) {
            this.push()
          }
        }} />
        <button onClick={this.push}>提交</button>
        <ul>
          {arr.map(item =>
            <li key={item.id}>
              {item.text}
              <button onClick={() => this.remove(item.id)}>删除</button>
            </li>
          )}
        </ul>
      </div>
    )
  }
}

export default App
