import React from 'react'; // react 核心包（创建react元素，创建组件...）
import ReactDOM from 'react-dom/client'; // 渲染react元素
import './style.css'
import App from './App'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App />
);

