import React, { Component, createRef } from 'react'
import ClassNames from 'classnames'

class App extends Component {

  state = {
    title: '我是标题'
  }

  clear = () => {
    this.setState({
      title: ''
    })
  }

  // 定义ref对象
  inp = createRef()

  submit = () => {
    // 通过 ref 获取 dom 元素
    console.log('获取input的value', this.inp.current.value)
  }
  render() {
    const { title } = this.state
    return (
      <div>
        <h1>{title}</h1>
        {/* 受控组件：input 的 value 受 state 控制，必须添加 onChange 改变 state */}
        <input type="text" value={title} onChange={(e) => {
          this.setState({
            title: e.target.value
          })
        }} />
        <button onClick={this.clear}>清空</button>

        <hr />
        {/* 非受控组件: value 不受 state 控制 */}
        <input ref={this.inp} type="text" defaultValue={100} />
        <button onClick={this.submit}>获取input的value</button>
      </div>
    )
  }
}

export default App
