import React, { Component } from 'react'

class ButtonCounter extends Component {
  state = {
    num: 0,
  }
  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    const { num } = this.state
    console.log('接收父组件传过来的参数', this.props)
    return (
      <div className='box'>
        <h2>{this.props.title}</h2>
        <div>
          <button onClick={() => {
            // 想要改变 props 的值必须调用父组件的函数，子组件不可以直接修改
            // 子组件可以调用父组件传过来的函数修改props
            this.props.setTitle(100)
          }}>修改标题</button>
        </div>
        {num > 0 &&
          <>
            <button onClick={() => this.changeNum(-1)}>-</button>
            {this.state.num}
          </>
        }
        <button onClick={() => this.changeNum(1)}>+</button>
      </div>
    )
  }
}

export default ButtonCounter
