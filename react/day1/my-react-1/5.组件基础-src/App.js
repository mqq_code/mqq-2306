import React, { Component, createRef } from 'react'
import ClassNames from 'classnames'
import ButtonCounter from './components/ButtonCounter'

class App extends Component {

  state = {
    title: '组件11111111'
  }

  setTitle = (p = Math.random()) => {
    this.setState({
      title: p
    })
  }

  render() {
    return (
      <div>
        <h1>组件</h1>
        <button onClick={() => this.setTitle()}>改变title</button>
        <ButtonCounter
          title={this.state.title}
          setTitle={this.setTitle}
        />
        <ButtonCounter
          title={this.state.title}
          setTitle={this.setTitle}
        />
        <ButtonCounter
          title={this.state.title}
          setTitle={this.setTitle}
        />
        <ButtonCounter
          title={this.state.title}
          setTitle={this.setTitle}
        />
      </div>
    )
  }
}

export default App

