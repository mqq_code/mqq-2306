import React from 'react'; // react 核心包（创建react元素，创建组件...）
import ReactDOM from 'react-dom/client'; // 渲染react元素
import './style.css'

// class => className
// style 的值必须是对象
// 创建react元素，虚拟dom
// const wrap = React.createElement('div', {
//   id: 'wrap-id',
//   className: 'wrap',
//   style: { color: 'red' }
// }, [React.createElement('h1', {}, ['开始学习', React.createElement('b', {}, 'react!')])])

// jsx: js + xml
// jsx 本质上是 React.createElement 的语法糖，编译时通过 babel 转译成 React.createElement
// const wrap = <div className='wrap'>
//   <h1>开始学习 <b>react！</b></h1>
// </div>



const title = '我是二级标题'
const h2Style = {
  background: 'tomato',
  color: '#ffffff'
}
const imgUrl = 'http://p1.music.126.net/rSsW9DbaHzYjvKtODq8xgQ==/109951169152444751.jpg'
const sum = (a, b) => {
  return a + b
}
const flag = false
const arr = ['a', 'b', 'c', 'd']

const renderNum = num => {
  if (num < 10) {
    return <i>num 小于 10</i>
  } else if (num >= 10 && num < 20) {
    return <b>num 大于 10 并且小于 20</b>
  } else if (num >= 20 && num < 30) {
    return <b>num 大于 20 并且小于 30</b>
  } else if (num >= 40 && num < 50) {
    return <b>num 大于 40 并且小于 50</b>
  } else {
    return <div>其他</div>
  }
}
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div className='wrap'>
    <h1>开始学习 <b>react！</b></h1>
    <h2 style={h2Style}>{ title } ----- 我是普通文字</h2>
    <img src={imgUrl} width={300} alt="" />
    {/* {} 中只能渲染表达式，不能写语句，不能渲染对象 */}
    <p>字符串：{ 'abc' }</p>
    <p>数字：{ 123 }</p>
    <p>布尔值：{ false }</p>
    <p>null：{ null }</p>
    <p>undefined: { undefined }</p>
    <p>数组: { [1,2,3,4,5] }</p>
    <p>调用函数: { sum(1, 2) }</p>
    {/* <p>对象: { h2Style }</p> */}
    <p>三元表达式：{ Math.random() > 0.5 ? '成功' : 123 }</p>
    <hr />
    <h3>判断条件</h3>
    {/* <div>
      {flag ?
        <p>我是p标签</p>
      :
        <div>
          <h4>h4标签</h4>
          <p>我是false展示的div</p>
        </div>
      }
    </div> */}
    <div>
      {/* {flag ?
        <p>我是p标签</p>
      : null} */}
      {flag && <p>我是p标签</p>}
    </div>
    {/* {renderNum(22)} */}

    
    <hr />
    <h4>渲染列表</h4>
    <ul>
      {arr.map(item => {
        return <li key={item}>{item}</li>
      })}
    </ul>
  </div>
);



// 配置淘宝镜像，更换 npm 下载地址
// `npm config get registry` 查看包的下载源，默认下载源 `https://registry.npmjs.org/`
// `npm config set registry https://registry.npmmirror.com` 把下载源改成淘宝镜像地址