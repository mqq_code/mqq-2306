import React, { useState, useEffect } from 'react'
import style from './Home.module.scss'
import withAuth from '../../hoc/withAuth'
import classNames from 'classnames'
import { NavLink, useHistory, useLocation } from 'react-router-dom'

const tabList = [
  {
    title: '资金管理',
    list: [
      {
        text: '导航一',
        path: '/home/child1'
      },
      {
        text: '导航二',
        path: '/home/child2'
      },
      {
        text: '导航三',
        path: '/home/child3'
      }
    ]
  },
  {
    title: '系统管理',
    list: [
      {
        text: '导航四',
        path: '/home/child4'
      },
      {
        text: '导航五',
        path: '/home/child5'
      }
    ]
  }
]

const Home = (props) => {
  const [curIndex, setCurIndex] = useState(0)
  const [title, setTitle] = useState('')
  const [navList, setNavList] = useState([])
  const history = useHistory()
  const location = useLocation()
  const changeTab = index => {
    setCurIndex(index)
    // 跳转子列表第一个路由
    history.push(tabList[index].list[0].path)
  }

  useEffect(() => {
    tabList.forEach((item, index) => {
      item.list.forEach(val => {
         // 根据当前路由地址查找对应的数据
        if (val.path === location.pathname) {
          // 修改当前高亮tab
          setCurIndex(index)
          // 修改标题
          setTitle(val.text)
          // 添加到右侧导航
          if (!navList.find(v => v.path === location.pathname)) {
            setNavList([...navList, val])
          }
        }
      })
    })
  }, [location])


  const close = (e, index) => {
    e.preventDefault()
    const newNavList = [...navList]
    // 如果只有一个导航
    if (newNavList.length === 1) {
      setNavList([])
      localStorage.removeItem('token')
      history.push('/login')
      return
    }
    if (newNavList[index].path === location.pathname) {
      // 如果要删除的是当前标签页，判断是否有下一项
      if (newNavList[index + 1]) {
        history.push(newNavList[index + 1].path)
      } else {
        history.push(newNavList[index - 1].path)
      }
    }
    newNavList.splice(index, 1)
    setNavList(newNavList)
  }

  return (
    <div className={style.home}>
      <div className={style.menu}>
        <div className={style.tabNav}>
          {tabList.map((item, index) =>
            <p
              key={item.title}
              className={classNames({
                [style.active]: curIndex === index
              })}
              onClick={() => changeTab(index)}
            >{item.title}</p>
          )}
        </div>
        <div className={style.tabContent}>
          <h3>{title}</h3>
          {tabList[curIndex].list.map(item =>
            <NavLink key={item.path} to={item.path}>{item.text}</NavLink>
          )}
        </div>
      </div>
      <main>
        <nav>
          {navList.map((item, index) =>
            <NavLink key={item.path} to={item.path}>
              {item.text}
              <span onClick={e => close(e, index)}>x</span>
            </NavLink>
          )}
        </nav>
        <div className={style.content}>
          {props.children}
        </div>
      </main>
    </div>
  )
}

export default withAuth(Home)