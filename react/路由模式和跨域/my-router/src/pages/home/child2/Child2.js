import React, { useEffect, useRef } from 'react'
import * as echarts from 'echarts'

function genData(count) {
  // prettier-ignore
  const nameList = [
        '赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许', '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章', '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳', '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常', '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹', '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞', '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危'
    ];
  const legendData = [];
  const seriesData = [];
  for (var i = 0; i < count; i++) {
    var name =
      Math.random() > 0.65
        ? makeWord(4, 1) + '·' + makeWord(3, 0)
        : makeWord(2, 1);
    legendData.push(name);
    seriesData.push({
      name: name,
      value: Math.round(Math.random() * 100000)
    });
  }
  return {
    legendData: legendData,
    seriesData: seriesData
  };
  function makeWord(max, min) {
    const nameLen = Math.ceil(Math.random() * max + min);
    const name = [];
    for (var i = 0; i < nameLen; i++) {
      name.push(nameList[Math.round(Math.random() * nameList.length - 1)]);
    }
    return name.join('');
  }
}
const pieData = genData(50);


const Child2 = () => {
  const chartRef = useRef()
  const myChart = useRef()
  const startBar = () => {
    // 绘制图表
    myChart.current.setOption({
      title: {
        text: '柱状图'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['邮件', '广告', '视频广告', 'Direct', '搜索引擎']
      },
      grid: {
        left: '30px',
        right: '30px',
        bottom: '30px',
        top: '50px',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: '邮件',
          type: 'bar',
          data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
          name: '广告',
          type: 'bar',
          data: [220, 1802, 191, 234, 290, 330, 310]
        },
        {
          name: '视频广告',
          type: 'bar',
          data: [150, 232, 1000, 154, 190, 330, 410]
        },
        {
          name: 'Direct',
          type: 'bar',
          data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
          name: '搜索引擎',
          type: 'bar',
          data: [820, 932, 901, 934, 1290, 1330, 1320]
        }
      ]
    }, true);
  }
  const startLine1 = () => {
    // 绘制图表
    myChart.current.setOption({
      title: {
        text: '折线图'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['邮件', '广告']
      },
      grid: {
        left: '30px',
        right: '30px',
        bottom: '30px',
        top: '50px',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: '邮件',
          type: 'line',
          data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
          name: '广告',
          type: 'line',
          data: [220, 1802, 191, 234, 290, 330, 310]
        }
      ]
    }, true);
  }
  const startLine2 = () => {
    // 绘制图表
    myChart.current.setOption({
      title: {
        text: '折线图'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['视频广告', 'Direct', '搜索引擎']
      },
      grid: {
        left: '30px',
        right: '30px',
        bottom: '30px',
        top: '50px',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日']
      },
      yAxis: {
        type: 'value'
      },
      series: [
        {
          name: '视频广告',
          type: 'line',
          data: [150, 232, 1000, 154, 190, 330, 410]
        },
        {
          name: 'Direct',
          type: 'line',
          data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
          name: '搜索引擎',
          type: 'line',
          data: [820, 932, 901, 934, 1290, 1330, 1320]
        }
      ]
    }, true);
  }
  const startPie = () => {
    myChart.current.setOption({
      title: {
        text: '同名数量统计',
        subtext: '纯属虚构',
        left: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        type: 'scroll',
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: pieData.legendData
      },
      series: [
        {
          name: '姓名',
          type: 'pie',
          radius: '55%',
          center: ['40%', '50%'],
          data: pieData.seriesData,
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    }, true)
  }

  useEffect(() => {
    // 基于准备好的dom，初始化echarts实例
    myChart.current = echarts.init(chartRef.current);
  }, [])

  return (
    <div>
      <h2>图表</h2>
      <button onClick={startLine1}>折线图1</button>
      <button onClick={startLine2}>折线图2</button>
      <button onClick={startBar}>柱状图</button>
      <button onClick={startPie}>饼图</button>
      <div className="chart" ref={chartRef}></div>
    </div>
  )
}

export default Child2