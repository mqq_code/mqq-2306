import React, { useEffect, useState } from 'react'
import axios from 'axios'


// 浏览器的同源策略：浏览器为了保护用户数据安全，只允许向和当前页面地址的协议、端口、域名一致的接口发送 ajax 请求
// 跨域：请求接口的协议、端口、域名和当前页面地址只要有一个不同就会跨域
// 解决夸域:
// 1. jsonp（只能处理get请求）
//   原理：利用 script 标签不会跨域的特点，动态创建script标签向服务端发送请求，因为script标签只能发送get请求，所以有限制
//   服务端返回一段调用函数的 js 代码

// 2. 后端配置cors，接口配置允许夸域访问的响应头
// 3. 前端代理
//   跨域是浏览器的限制，当前页面向自己的服务器应用发送请求没有跨域限制，服务端向另一个服务端发送请求没有跨域
//   请求： 浏览器 => 本身服务器 => 第三方服务器
//   响应： 第三方服务器 => 本身服务器 => 浏览器
//   本地开发: webpack 的 proxy 代理
//   线上:  nginx 代理





const Child1 = () => {
  const [state, setState] = useState(null)

  useEffect(() => {
    axios.get('/mqq/music/list').then(res => {
      console.log(res.data)
    })
  }, [])

  return (
    <div>Child1</div>
  )
}

export default Child1


// jsonp
// const script = document.createElement('script')
// script.src = 'http://192.168.28.11:3001/abc/def'
// document.body.appendChild(script)

// window.getJsonpRes = function (data) {
//   console.log(data)
// }