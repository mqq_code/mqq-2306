// 高阶组件：参数为组件，返回值为新组件的函数
import { Redirect } from 'react-router-dom'

const withAuth = (Com) => {
  const Auth = (props) => {
    // 判断是否登录
    const token =localStorage.getItem('token')
    if (token) {
      return <Com {...props} />
    }
    return <Redirect to="/login" />
  }

  return Auth
}

export default withAuth