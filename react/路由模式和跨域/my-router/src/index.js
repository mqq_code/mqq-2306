import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import {
  HashRouter, // hash模式，url有#，利用 hashchange事件监听#后的内容变化，动态改变组件
  BrowserRouter // 浏览器模式，url没有#，通过 history 的 pushState 修改历史记录，需要在服务端添加配置，访问不到静态资源的时候返回 index.html 
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

