import React, { Component } from 'react'
import style from './App.module.scss'
import BigImg from './components/BigImg'
export default class App extends Component {
  state = {
    pics: [
      "https://images.unsplash.com/photo-1656173460244-faad38c026e6",
      "https://images.unsplash.com/photo-1656380605767-28a92b240053",
      "https://images.unsplash.com/photo-1656145076222-d43e10c4338f",
      "https://images.unsplash.com/photo-1656147084224-3ec22fbd3e96",
      "https://images.unsplash.com/photo-1656328670689-06f48c226aaf",
      "https://images.unsplash.com/photo-1656329020255-b5b450d45704",
      "https://images.unsplash.com/photo-1655853494101-712187ddd4e6"
    ],
    curIndex: 0, // 当前展示第一章图片的下标
    bigIndex: -1, // 展示的大图的下标
  }
  changeIndex = (n) => {
    let index = this.state.curIndex + n
    if (index < 0) {
      index = 0
    } else if (index > this.state.pics.length - 4) {
      index = this.state.pics.length - 4
    }
    this.setState({
      curIndex: index
    })
  }
  closeBigImg = () => {
    this.setState({ bigIndex: -1 })
  }
  changeBigIndex = (n) => {
    let bigIndex = this.state.bigIndex + n
    if (bigIndex < 0) {
      bigIndex = this.state.pics.length - 1
    } else if (bigIndex > this.state.pics.length - 1) {
      bigIndex = 0
    }
    this.setState({ bigIndex })
  }
  render() {
    const { pics, curIndex, bigIndex } = this.state
    return (
      <div className={style.app}>
        <div className={style.content}>
          <div className={style.btns}>
            <div className={style.btn} onClick={() => this.changeIndex(-1)}></div>
            <div className={style.btn} onClick={() => this.changeIndex(1)}></div>
          </div>
          <div className={style.pics}>
            <ul style={{ transform: `translateX(-${curIndex * 205}px)` }}>
              {pics.map((item, index) =>
                <li key={item} onClick={() => this.setState({ bigIndex: index })}>
                  <p style={{ backgroundImage: `url(${item})` }}></p>
                  <div className={style.mask}>查看大图</div>
                </li>
              )}
            </ul>
          </div>
        </div>
        {bigIndex > -1 &&
          <BigImg
            url={pics[bigIndex]}
            closeBigImg={this.closeBigImg}
            changeBigIndex={this.changeBigIndex}
          />
        }
      </div>
    )
  }
}
