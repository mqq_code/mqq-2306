import React, { Component, createRef } from 'react'
import style from './BigImg.module.scss'

export default class BigImg extends Component {
  // 存跟页面渲染相关的数据
  state = {
    scale: 1, // 缩放比例
    rotate: 0, // z轴旋转
    rotateY: 0, // y轴旋转
    rotateX: 0 // x轴旋转
  }
  // 跟页面渲染无关的变量直接存到 class 中
  curScale = 1 // 存当前缩放的大小
  isMax = false // 当前是scale === 2
  imgRef = createRef() // 存图片的dom元素
  timer = null // 存定时器
  changeScale = (scale) => {
    if (scale < 0.2) {
      scale = 0.2
    } else if (scale > 2) {
      scale = 2
    }
    this.setState({ scale })
    this.curScale = scale
  }
  scaleMax = () => {
    if (this.isMax) {
      this.setState({ scale: this.curScale })
    } else {
      this.setState({ scale: 2 })
    }
    this.isMax = !this.isMax
  }
  fullScreen = () => {
    // 让图片全屏
    this.imgRef.current.requestFullscreen()
    this.timer = setInterval(() => {
      this.props.changeBigIndex(1)
    }, 3000)
  }
  // 监听是否全屏
  fullscreenHandler = () => {
    if (document.fullscreenElement) {
      console.log('进入全屏')
    } else {
      console.log('退出全屏')
      clearInterval(this.timer)
    }
  }
  // 组件挂载完成
  componentDidMount(){
    console.log('bigimg加载完成')
    document.addEventListener('fullscreenchange', this.fullscreenHandler)
  }
  // 组件销毁之前清除定时器和原生事件
  componentWillUnmount() {
    console.log('bigimg即将销毁')
    clearInterval(this.timer)
    document.removeEventListener('fullscreenchange', this.fullscreenHandler)
  }
  render() {
    const { scale, rotate, rotateX, rotateY } = this.state
    const { url, closeBigImg, changeBigIndex } = this.props
    return (
      <div className={style.bigImg}>
        <img
          ref={this.imgRef}
          style={{
            transform: `translate(-50%, -50%) scale(${scale}) rotate(${rotate}deg)  rotateX(${rotateX}deg) rotateY(${rotateY}deg)`
          }}
          src={url}
        />
        <div className={style.close} onClick={closeBigImg}>关闭</div>
        <div className={style.control}>
          <i className='iconfont' onClick={() => this.changeScale(scale + 0.2)}>+</i>
          <i className='iconfont' onClick={() => this.changeScale(scale - 0.2)}>-</i>
          <i className='iconfont icon-icon-test' onClick={this.scaleMax}></i>
          <i className='iconfont icon-huifu' onClick={() => this.changeScale(1)}></i>
          <i className='iconfont icon-xiangzuo' onClick={() => changeBigIndex(-1)}></i>
          <i className='iconfont icon-yunhang' onClick={this.fullScreen}></i>
          <i className='iconfont icon-xiangyou1' onClick={() => changeBigIndex(1)}></i>
          <i className='iconfont icon-xiangzuo4' onClick={() => this.setState({ rotate: rotate - 90 })}></i>
          <i className='iconfont icon-xiangyou4' onClick={() => this.setState({ rotate: rotate + 90 })}></i>
          <i className='iconfont icon-link-arrow-v-full' onClick={() => this.setState({ rotateX: rotateX + 180 })}></i>
          <i className='iconfont icon-link-arrow-h-full' onClick={() => this.setState({ rotateY: rotateY + 180 })}></i>
        </div>
      </div>
    )
  }
}
