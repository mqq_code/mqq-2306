import React, { Component, PureComponent } from 'react'

class Child extends Component {

  state = {
    count: this.props.num
  }

  // 组件挂载前和更新前会执行此函数，可以获取到最新的 props 和当前的 state
  static getDerivedStateFromProps(props, state) {
    console.log(props, state)
    // 返回的对象会合并到当前组件的state中
    return {
      count: props.num
    }
  }


  render() {
    return (
      <div className='child'>
        {this.props.num}
        <p>count: {this.state.count}</p>
      </div>
    )
  }
}

// PureComponent: 内部实现 shouldComponentUpdate 浅比较state和props的所有属性是否有更新
class Child1 extends PureComponent {

  state = {
    count: 100
  }

  // 可以比较更新前后的state和props的数据是否改变，判断该组件是否需要更新，可以优化性能
  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('更新后', nextProps, nextState)
  //   console.log('更新前', this.props, this.state)
  //   if (nextState.count !== this.state.count) {
  //     return true
  //   }
  //   return false
  // }

  render() {
    console.log('child1 render')
    return (
      <div className='child'>
        <h3>child1组件</h3>
        <p>
          <button onClick={() => this.setState({ count: this.state.count - 1 })}>-</button>
          {this.state.count}
        </p>
      </div>
    )
  }
}

// 生命周期：组件从创建到销毁的过程
// 生命周期钩子函数：在特定时机自动执行的函数
class App extends Component {

  state = {
    arr: [],
    num: 0
  }
  add = () => {
    this.setState({
      arr: [...this.state.arr, this.state.arr.length]
    })
  }

  // render 之后 componentDidUpdate 之前执行，可以获取组件更新前的dom相关的数据传给 componentDidUpdate
  // getSnapshotBeforeUpdate() {
  //   console.log('getSnapshotBeforeUpdate', document.querySelector('ul').clientHeight)
  //   return document.querySelector('ul').clientHeight
  // }

  // componentDidUpdate(prevProps, preState, clientHeight) {
  //   console.log('componentDidUpdate', `页面更新之前ul的高度为${clientHeight}`)
  //   console.log( document.querySelector('ul').clientHeight)
  // }


  render() {
    console.log('render')
    return (
      <div>
        <h1>生命周期</h1>
        <button onClick={this.add}>+</button>
        <ul>
          {this.state.arr.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
        <hr />
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>按钮点击了 {this.state.num} 次</button>
        {/* <Child num={this.state.num} /> */}
        <Child1 />
      </div>
    )
  }
}


export default App



// 挂载阶段
// 1. constructor 初始化 state 和 props
// 2. componentWillMount 组件挂载之前(新版已弃用)
// 3. render 渲染虚拟dom
// 4. componentDidMount 组件挂载完成(可以获取页面的dom元素，调用接口、定义定时器...)

// 更新阶段(state和props改变会触发更新阶段)
// 1. shouldComponentUpdate 判断组件是否需要更新
// 2. componentWillUpdate（新版本已弃用） 组件更新之前
// 3. render 渲染虚拟dom
// 4. componentDidUpdate 组件更新完成，可以获取最新的dom和数据
// props 更新
// 1. componentWillReceiveProps（新版本已弃用） 当state的值始终依赖props时使用

// 销毁阶段
// 1. componentWillUnmount 清除异步任务，例如定时器，原生事件...






// class Person {
//   name = '小明'
//   say() {
//     // console.log(this.name)
//     console.log(this.abc)
//   }
//   // 静态方法, 类本身的方法， 需要通过 Person.abc() 调用
//   static abc() {
//     console.log('我是静态方法')
//   }
// }

// // 实例化对象
// const xm = new Person()
// console.log(xm)
// // 调用实例方法
// xm.say()

// // 调用静态方法
// Person.abc()



// function Person(name) {
//   this.name = name
//   this.say = function() {

//   }
// }
// // 定义静态方法
// Person.abc = function () {

// }
// const xm = new Person()





// const obj = {
//   name: '0',
//   info: {
//     a: {
//       b: '000'
//     }
//   }
// }

// const obj1 = {
//   name: '1',
//   info: {
//     a: {
//       b: '111'
//     }
//   }
// }

// obj.name === obj1.name || obj.info === obj1.info
