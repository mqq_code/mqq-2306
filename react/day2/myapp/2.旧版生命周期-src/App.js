import React, { Component } from 'react'

class Child extends Component {

  state = {
    // count: this.props.num,
    newCount: 50
  }
  // 新版本已弃用（props更新时执行此生命周期）
  // componentWillReceiveProps(nextProps) {
  //   // 可以在组件更新之前获取到最新的props，当state的值根据props改变时可以在此函数更新state
  //   // console.log('nextProps', nextProps)
  //   // console.log('this.props', this.props)
  //   this.setState({
  //     count: nextProps.num
  //   })
  // }

  handleClick = () => {
    console.log('点击页面')
  }

  componentDidMount() {
    this.timer = setInterval(() => {
      console.log(this.state.newCount)
      this.setState({ newCount: this.state.newCount - 1 })
    }, 1000)
    // document.addEventListener('click', this.handleClick)
  }

  // 组件销毁之前，可以清除异步任务，例如定时器，原生js的事件绑定
  componentWillUnmount() {
    console.log('组件要销毁了')
    clearInterval(this.timer)
    // document.removeEventListener('click', this.handleClick)
  }

  render() {
    // console.log('child render')
    return (
      <div className='child'>
        {/* <h2>父组件的num - {this.props.num}</h2> */}
        {/* <h2>child 组件 - {this.state.count}</h2>
        <button onClick={() => this.setState({ count: this.state.count - 1 })}>count -</button> */}
        <p>倒计时: {this.state.newCount}</p>
      </div>
    )
  }
}



// 生命周期：组件从创建到销毁的过程
// 生命周期钩子函数：在特定时机自动执行的函数
class App extends Component {

  // // 1. 初始化阶段，初始化 state 和 props
  // constructor(props) {
  //   // super: 父类的构造函数，把props传给 Component 的构造函数，才能使用 this.props 获取父组件传过来的参数
  //   super(props)
  //   // 定义state
  //   this.state = {
  //     num: 10
  //   }
  //   console.log('%c初始化阶段 constructor', 'color: red;font-size: 20px')
  // }
  // // 挂载阶段，组件挂载之前(新版已弃用)
  // componentWillMount() {
  //   console.log('%c组件挂载之前 componentWillMount', 'color: red;font-size: 20px')
  // }
  // // 挂载阶段，组件挂载完成(可以获取页面的dom元素，调用接口、定义定时器...)
  // componentDidMount() {
  //   console.log('%c组件挂载完成 componentDidMount', 'color: red;font-size: 20px')
  // }

  // // 更新阶段，可以判断组件是否需要更新（性能优化）
  // shouldComponentUpdate() {
  //   console.log('%c shouldComponentUpdate', 'color: orange;font-size: 20px')
  //   // 返回 true 允许页面更新，返回 false 阻止页面更新
  //   return true
  // }
  // // 组件更新之前(新版本已弃用)
  // componentWillUpdate() {
  //   console.log('%c componentWillUpdate', 'color: orange;font-size: 20px')
  // }
  // // 组件更新完成（可以获取到最新的数据和最新的dom元素）
  // componentDidUpdate() {
  //   console.log('%c componentDidUpdate', 'color: orange;font-size: 20px')
  //   console.log(document.querySelector('.num').outerHTML, this.state)
  // }

  state = {
    num: 0
  }

  // 挂载阶段和更新阶段都会执行，渲染虚拟dom
  render() {
    console.log('%c render', 'color: red;font-size: 20px')
    return (
      <div>
        <h1>生命周期</h1>
        <p className='num'>{this.state.num}</p>
        <button onClick={() => {
          // 调用 setState 或者 props 改变会触发组件更新
          this.setState({
            num: this.state.num + 1
          })
        }}>+</button>
        {this.state.num <= 5 && <Child num={this.state.num} />}
      </div>
    )
  }
}


export default App



// class Person {
//   constructor(a) {
//     console.log('a', a)
//   }
// }

// const xm = new Person(100)
// console.log(xm)

