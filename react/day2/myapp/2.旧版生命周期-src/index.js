import React from 'react';
import ReactDOM from 'react-dom/client';
// npm i sass -D 使用 sass 
import './index.scss';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App a="100" b={true} c={[1,2,3,4]} />
);
