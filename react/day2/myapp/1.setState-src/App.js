import React, { Component } from 'react'

class App extends Component {

  // 存数据
  state = {
    num: 0,
    title: '默认标题'
  }

  add = () => {
    // 1. setState 更新数据是异步的
    // 2. 调用 setState 会把传入的对象跟原来的 state 进行合并，然后触发 render 函数渲染组件
    // this.setState({ num: this.state.num + 1 }) // 0 + 1 = 1
    // this.setState({ num: this.state.num + 1 }) // 0 + 1 = 1
    // this.setState({ num: this.state.num + 1 }) // 0 + 1 = 1

    // 3. 连续调用多次 setState 会收集多次传入的对象然后合并对象，最终合并到组件的 state 中
    // 连续多次调用会合并更新 render 函数只执行一次
    // this.setState({ num: 3 })
    // this.setState({ num: 2, title: 'App' })
    // this.setState({ num: 1 })

    // 4. 连续调用时想要获取最新的 state 可以给 setState 传入一个函数，函数的参数就是最新的 state，
    // 函数的返回值会和组件的 state 进行合并
    // this.setState(state => {
    //   return { num: state.num + 1 }
    // })
    // this.setState(state => {
    //   return { num: state.num + 1 }
    // })
    // this.setState(state => {
    //   return { num: state.num + 1 }
    // })

    // 5. setState 可以传入第二个参数，是页面更新完成的回调函数
    this.setState({ num: this.state.num + 1}, () => {
      // 页面更新完成的回调函数，可以获取到最新的数据和 dom元素
      console.log(document.querySelector('p').outerHTML, this.state.num)
    })
  }

  render() {
    console.log('render', this.state.num)
    return (
      <div>
        <h1>{this.state.title}</h1>
        <p>{this.state.num}</p>
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}


export default App
