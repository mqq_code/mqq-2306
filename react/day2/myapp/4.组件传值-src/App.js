import React, { Component, PureComponent, createRef } from 'react'
import Dialog from './components/Dialog'
// css module
import style from './App.module.scss'
import ButtonCounter from './components/ButtonCounter'

class App extends Component {

  state = {
    show: false,
    showCreate: false
  }
  closeDel = () => {
    console.log('app组件的close函数')
    this.setState({ show: false })
  }
  cancelDel = () => {
    console.log('取消删除')
  }
  okDel = () => {
    console.log('确定删除')
  }

  closeCreate = () => {
    this.setState({ showCreate: false })
  }

  changeNum = (num) => {
    console.log('接收子组件传过来的数据', num)
  }
  // 创建ref对象
  CounterRef = createRef()
  render() {
    const { show, showCreate } = this.state
    return (
      <div className={style.app}>
        <h1 className={style.title}>组件</h1>
        <button onClick={() => this.setState({ show: true })}>删除</button>
        {show &&
          <Dialog
            title="警告123"
            cancelText="我在想想"
            okText="残忍删除"
            close={this.closeDel}
            header={
              <i>我是头部</i>
            }
            cancel={this.cancelDel}
            ok={this.okDel}
          >
            <div>
              <img src="https://img1.baidu.com/it/u=557715362,3518113401&fm=253&fmt=auto&app=138&f=JPEG?w=255&h=255" alt="" />
              <h2>确定要删除吗？</h2>
            </div>
          </Dialog>
        }

        <button onClick={() => this.setState({ showCreate: true })}>新增</button>
        {showCreate &&
          <Dialog
            title="新增"
            close={this.closeCreate}
          >
            <p>姓名: <input type="text" /></p>
            <p>年龄: <input type="text" /></p>
          </Dialog>
        }
        <hr />
        <p>ButtonCounter组件内的num：</p>
        <button onClick={() => {
          // 通过 ref 获取子组件实例对象，可以调用子组件的数据和方法
          console.log(this.CounterRef.current)
          // console.log(this.CounterRef.current?.state.num)
          this.CounterRef.current.change(2)
        }}>调用子组件的函数</button>
        <ButtonCounter ref={this.CounterRef} changeNum={this.changeNum} />


      </div>
    )
  }
}


export default App