import React, { Component } from 'react'

class ButtonCounter extends Component {
  state = {
    num: 0
  }
  change = n => {
    const num = this.state.num + n
    this.setState({ num })
    // 调用父组件的函数，给父组件传数据
    this.props.changeNum(num)
  }
  render() {
    
    return (
      <div>
        <button onClick={() => this.change(-1)}>-</button>
        {this.state.num}
        <button onClick={() => this.change(1)}>+</button>
      </div>
    )
  }
}

export default ButtonCounter
