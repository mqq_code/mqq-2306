import React, { Component } from 'react'
import style from './Dialog.module.scss'

class Dialog extends Component {

  // 定义props默认值
  static defaultProps = {
    cancelText: '取消按钮',
    okText: '确定按钮'
  }

  render() {
    console.log('父组件传过来的数据', this.props)
    return (
      <div className={style.dialog}>
        <div className={style.content}>
          <div className={style.close} onClick={this.props.close}>x</div>
          <div className={style.header}>
            {this.props.header ?
              this.props.header
            :
              <h2 className={style.title}>{this.props.title}</h2>
            }
          </div>
          <div className={style.center}>
            {this.props.children}
          </div>
          <div className={style.btm}>
            <button onClick={this.props.cancel}>{this.props.cancelText}</button>
            <button onClick={this.props.ok}>{this.props.okText}</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Dialog
