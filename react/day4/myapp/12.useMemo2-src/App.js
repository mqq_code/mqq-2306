import React, { useEffect, useRef, useState, useMemo } from 'react'

const App = () => {
  const [title, setTitle] = useState('标题 useMemo')
  const [list, setList] = useState([{
    title: '苹果',
    price: 10,
    count: 1
  },{
    title: '香蕉',
    price: 5,
    count: 1
  },{
    title: '梨',
    price: 4,
    count: 1
  }])

  const changeCount = (index, n) => {
    const newList = [...list]
    newList[index].count += n
    setList(newList)
  }

  // const totalFn = () => {
  //   return list.reduce((prev, val) => {
  //     console.log(val)
  //     return prev + val.count * val.price
  //   }, 0)
  // }
  
  const total = useMemo(() => {
    return list.reduce((prev, val) => {
      console.log(val)
      return prev + val.count * val.price
    }, 0)
  }, [list])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <ul>
        {list.map((item, index) =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>价格: ¥{item.price}</p>
            <button onClick={() => changeCount(index, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(index, 1)}>+</button>
          </li>
        )}
      </ul>
      <p>总价: ¥{total}</p>
      <p>总价: ¥{total}</p>
      <p>总价: ¥{total}</p>
      <p>总价: ¥{total}</p>
      <p>总价: ¥{total}</p>
    </div>
  )
}

export default App