import React, { useState, useEffect } from 'react'

const Child = () => {
  const [count, setCount] = useState(0)

  // useEffect(() => {
  //   console.log('Child 组件挂载完成')
  //   const handleClick =  () => {
  //     console.log('点击页面了')
  //   }
  //   document.addEventListener('click', handleClick)

  //   return () => {
  //     // 4. 组件销毁时执行，类似 componentWillUnmounte, 可以清除定时器、原生事件...
  //     console.log('Chidl 组件销毁了')
  //     document.removeEventListener('click', handleClick)
  //   }
  // }, [])

  // count 变量改变时，先执行上一次 useEffect return 的函数，然后再等页面更新后执行本次的函数
  // useEffect(() => {
  //   console.log('count 改变了', count)
  //   return () => {
  //     console.log('useEffect return的函数执行了', count)
  //   }
  // }, [count])

  return (
    <div className='box'>
      <h2>Child</h2>
      <button onClick={() => setCount(count + 1)}>按钮点击次数: {count}</button>
    </div>
  )
}


// useEffect: 处理组件的副作用（调接口、定时器、绑定原生事件）
// useEffect 可以实现类似 class 组件中的生命周期的功能
// useEffect(callback, [依赖项1，依赖项2...])
const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('useEffect')

  // 1. 第二个参数不传，只要有state更新就会执行函数，类似 componentDidUptate
  // useEffect(() => {
  //   // 等待组件渲染完成后执行
  //   console.log('组件加载完成', document.querySelector('p').outerHTML)
  // })

  // 2. 第二个参数传空数组，函数只执行一次，类似 componentDidMount
  // useEffect(() => {
  //   console.log('组件加载完成', document.querySelector('p').outerHTML)
  // }, [])

  // 3. 依赖项传入具体变量, 依赖项改变时执行
  // useEffect(() => {
  //   console.log('title 改变了', title)
  // }, [title, num])


  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <p>{num}</p>
      <button onClick={() => setNum(num + 1)}>+</button>
      {num <= 5 && <Child />}
    </div>
  )
}



export default App

// state 和 setState
// 生命周期
// 组件传值