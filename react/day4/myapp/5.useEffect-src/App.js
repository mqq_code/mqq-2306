import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Child = () => {
  const [count, setCount] = useState(60)

  // useEffect(() => {
  //   const timer = setInterval(() => {
  //     setCount(c => {
  //       console.log('获取最新的 count', c)
  //       return c - 1
  //     })
  //   }, 1000)
  //   return () => {
  //     clearInterval(timer)
  //   }
  // }, [])

  const start = () => {
    setInterval(() => {
      setCount(c => c - 1)
    }, 1000)
  }

  return (
    <div className='box'>
      <h2>Child</h2>
      <p>倒计时: {count}</p>
      <button onClick={start}>开始</button>
    </div>
  )
}

const App = () => {
  const [show, setShow] = useState(true)
  const [list, setList] = useState([])

  // 正确用法
  const getBanner = async () => {
    const res = await axios.get('https://zyxcl.xyz/music_api/banner')
    setList(res.data.banners)
  }
  useEffect(() => {
    getBanner()
  }, [])

  // 错误用法，因为 useEffect 的 callback 需要 return 一个函数在组件销毁时执行，async 函数返回的是一个 promise 不是函数，无法调用
  // useEffect(async () => {
  //   const res = await axios.get('https://zyxcl.xyz/music_api/banner')
  //   setList(res.data.banners)
  // }, [])

  return (
    <div>
      <h1>useEffect</h1>
      <button onClick={() => setShow(!show)}>toggle</button>
      {show && <Child />}
      <ul>
        {list.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width={400} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}



export default App

// state 和 setState
// 生命周期
// 组件传值


// const fn = async () => {
//   return 11
// }

// console.log(fn())