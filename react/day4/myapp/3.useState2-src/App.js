import React, { useState } from 'react'

const App = () => {
  const [num, setNum] = useState(0)

  const add = () => {
    // 异步更新，多次调用会合并更新
    // setNum(num + 1)
    // setNum(num + 1)
    // setNum(num + 1)

    setNum(n => {
      // n: 最新的数据
      // 返回值覆盖之前的数据
      return n + 1
    })
    setNum(n => n + 1)
    setNum(n => n + 1)
  }

  console.log('App 渲染')

  return (
    <div>
      <h1>useState</h1>
      <p>{num}</p>
      <button onClick={add}>+</button>
    </div>
  )
}



export default App

// state 和 setState
// 生命周期
// 组件传值