import React, { useEffect, useRef, useState, useMemo } from 'react'

const App = () => {
  const [num, setNum] = useState(0)
  const [firstName, setFistName] = useState('王')
  const [lastName, setLastName] = useState('小明')
  const renderDesc = () => {
    console.log('执行 renderDesc')
    if (firstName.length < 1 || firstName.length > 2) {
      return `姓氏格式错误, 名字: ${lastName}`
    } else if (lastName.length < 1 || lastName.length > 4) {
      return `姓氏: ${firstName}， 名字格式错误 `
    }
    return `姓氏: ${firstName}，名字: ${lastName}`
  }
  // useMemo：返回一个缓存的值，依赖项更新自动执行函数，依赖项没有更新从缓存中读取值
  const desc = useMemo(() => {
    console.log('执行 renderDesc')
    if (firstName.length < 1 || firstName.length > 2) {
      return `姓氏格式错误, 名字: ${lastName}`
    } else if (lastName.length < 1 || lastName.length > 4) {
      return `姓氏: ${firstName}， 名字格式错误 `
    }
    return `姓氏: ${firstName}，名字: ${lastName}`
  }, [firstName, lastName])

  return (
    <div>
      <h1>useMemo</h1>
      <p>姓氏：{firstName} <input type="text" value={firstName} onChange={e => setFistName(e.target.value)} /></p>
      <p>名字：{lastName} <input type="text" value={lastName} onChange={e => setLastName(e.target.value)} /></p>
      {/* <p>{renderDesc()}</p>
      <p>{renderDesc()}</p>
      <p>{renderDesc()}</p>
      <p>{renderDesc()}</p>
      <p>{renderDesc()}</p> */}
      <p>{desc}</p>
      <p>{desc}</p>
      <p>{desc}</p>
      <p>{desc}</p>
      <p>{desc}</p>
      <hr />
      <button onClick={() => setNum(num + 1)}>{num}+</button>
    </div>
  )
}

export default App