import React, { useEffect, useRef, useState } from 'react'
import Child from './components/Child'

const App = () => {
  const ChildRef = useRef(null)
  const addChildNum = () => {
    // 调用子组件的方法和数据
    console.log(ChildRef.current)
    ChildRef.current.add(2)
  }
  return (
    <div>
      <h1>useRef</h1>
      <button onClick={addChildNum}>Child + 2</button>
      {/*
        使用ref调用函数子组件的方法:
          1. 在父组件把 ref 传给子组件
      */}
      <Child ref={ChildRef} a="100" />
    </div>
  )
}

export default App