import React, { useState, useImperativeHandle, forwardRef } from 'react'

const Child = (props, ref) => {
  const [num, setNum] = useState(0)

  const add = (n) => {
    setNum(num + n)
  }
  // 3. 给父组件传过来的 ref 添加数据
  useImperativeHandle(ref, () => {
    console.log('num更新', num)
    return {
      num,
      a: 100,
      add
    }
  }, [num, add])

  return (
    <div className='box'>
      <h2>Child</h2>
      <button onClick={() => add(-1)}>-</button>
      {num}
      <button onClick={() => add(1)}>+</button>
    </div>
  )
}

// 2. forwardRef: 转发 ref 到 Child 的第二个参数
export default forwardRef(Child)