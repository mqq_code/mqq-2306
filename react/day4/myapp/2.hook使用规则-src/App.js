import React, { useState } from 'react'
// hooks: 是一个名称以 use 开头函数
// hook使用规则：
// 不能在 if、for、子函数中使用hook，必须在函数组件的最顶层使用
// 不能在普通函数中使用hook，只能函数组件和自定义hook中使用


const App = () => {
  // useSate: 在函数组件中定义状态
  // const [变量名, 修改变量的函数] = useState(初始值)
  const [obj, setObj] = useState({ name: '小明', age: 22 })
  
  const changeName = e => {
    // const newObj = {...obj}
    // newObj.name = e.target.value
    // setObj(newObj)
    setObj({
      ...obj,
      name: e.target.value
    })
  }

  console.log('App渲染了')

  return (
    <div>
      <h1>useState</h1>
      <p>姓名: {obj.name} <input type="text" value={obj.name} onChange={changeName} /></p>
    </div>
  )
}



export default App

// state 和 setState
// 生命周期
// 组件传值