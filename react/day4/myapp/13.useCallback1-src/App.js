import React, { useEffect, useRef, useState, useMemo, useCallback } from 'react'

const arr = []

const App = () => {
  const [title, setTitle] = useState('标题 useMemo')
  const [list, setList] = useState([{
    title: '苹果',
    price: 10,
    count: 1
  },{
    title: '香蕉',
    price: 5,
    count: 1
  },{
    title: '梨',
    price: 4,
    count: 1
  }])

  // useCallback: 返回一个缓存的函数，依赖项更新重新创建函数，依赖项没有更新从缓存中读取函数
  const changeCount = useCallback((index, n) => {
    const newList = [...list]
    newList[index].count += n
    setList(newList)
  }, [list])

  if (!arr.includes(changeCount)) {
    arr.push(changeCount)
  }
  console.log('渲染App', arr)
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <ul>
        {list.map((item, index) =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>价格: ¥{item.price}</p>
            <button onClick={() => changeCount(index, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(index, 1)}>+</button>
          </li>
        )}
      </ul>
    </div>
  )
}

export default App