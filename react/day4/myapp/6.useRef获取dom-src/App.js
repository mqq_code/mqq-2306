import React, { useEffect, useRef } from 'react'

const App = () => {
  const title = useRef(null)
  const inp = useRef(null)

  useEffect(() => {
    console.log(title.current)
  }, [])

  return (
    <div>
      {/* 获取dom */}
      <h1 ref={title}>App</h1>
      <input ref={inp} type="text" />
      <button onClick={() => {
        console.log(title.current)
        console.log(inp.current.value)
      }}>获取dom</button>
    </div>
  )
}

export default App