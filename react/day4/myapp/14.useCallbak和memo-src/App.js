import React, { useEffect, useRef, useState, useMemo, useCallback } from 'react'
import List from './components/List'


const App = () => {
  const [title, setTitle] = useState('标题 useCallback')
  const [subTitle, setSubTitle] = useState('子组件标题')

  const changeSubTitle = useCallback((text) => {
    setSubTitle(text)
  }, [])
  
  console.log('App组件渲染了')
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <h2>{subTitle}</h2>
      <input type="text" value={subTitle} onChange={e => setSubTitle(e.target.value)} />
      <List listTitle={subTitle} changeTitle={changeSubTitle} />
    </div>
  )
}

export default App