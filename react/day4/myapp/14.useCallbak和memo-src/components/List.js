import React, { useState, memo } from 'react'

const List = (props) => {
  const [list, setList] = useState([{
    title: '苹果',
    price: 10,
    count: 1
  },{
    title: '香蕉',
    price: 5,
    count: 1
  },{
    title: '梨',
    price: 4,
    count: 1
  }])

  const changeCount = (index, n) => {
    const newList = [...list]
    newList[index].count += n
    setList(newList)
  }

  console.log('List 组件渲染了')
  return (
    <div className="box">
      <h2>{props.listTitle}</h2>
      <button onClick={() => props.changeTitle(Math.random())}>修改 listTitle</button>
      <ul>
        {list.map((item, index) =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>价格: ¥{item.price}</p>
            <button onClick={() => changeCount(index, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(index, 1)}>+</button>
          </li>
        )}
      </ul>
    </div>
  )
}

// memo: 高阶组件，性能优化，父组件更新时会浅比较上一次的 props 和当前的 props 是否有更新，有更新就渲染组件，没有就不渲染
export default memo(List)

// 第二个参数是一个函数，可以手动比较
// export default memo(List, (prevProps, props) => {
//   // console.log('prevProps', prevProps)
//   // console.log('props', props)
//   // return true 不更新组件， return false 更新组件
//   return false
// })



// const prevProps = {
//   a: 1,
//   b: 2,
//   c: {
//     d: {
//       e: 100
//     }
//   }
// }

// const props = {
//   a: 1,
//   b: 2,
//   c: {
//     d: {
//       e: 200
//     }
//   }
// }
