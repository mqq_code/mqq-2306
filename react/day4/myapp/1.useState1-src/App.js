import React, { useState } from 'react'
// hooks: 是一个名称以 use 开头函数


const App = () => {
  // useSate: 在函数组件中定义状态
  // const [变量名, 修改变量的函数] = useState(初始值)
  const [num, setNum] = useState(100)
  const [title, setTitle] = useState('默认标题')
  const [flag, setFlag] = useState(true)
  const [text, setText] = useState('')
  const [arr, setArr] = useState([{ id: 0, text: '000000000' }])

  const add = () => {
    // 使用传入的数据覆盖之前的数据，触发组件更新
    setNum(num + 1)
  }
  const pushArr = () => {
    const newArr = [...arr]
    newArr.push({ id: Date.now(), text })
    setArr(newArr)
    setText('')
    // setArr([
    //   ...arr,
    //   {
    //     id: Date.now(),
    //     text
    //   }
    // ])
  }

  console.log('渲染App')
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <button onClick={() => setFlag(!flag)}>{flag ? '隐藏' : '显示'}</button>
      {flag &&
        <div className="box">
          <p>{num}</p>
          <button onClick={add}>+</button>
        </div>
      }
      <hr />
      <input type="text" value={text} onChange={e => setText(e.target.value)} />
      <button onClick={pushArr}>添加</button>
      <ul>
        {arr.map(item =>
          <li key={item.id}>{item.text}</li>
        )}
      </ul>
    </div>
  )
}



export default App

// state 和 setState
// 生命周期
// 组件传值