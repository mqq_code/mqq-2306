import React, { useEffect, useRef, useState } from 'react'

const App = () => {
  // ref存数据，数据改变不会触发组件更新
  const n = useRef(0)
  const timer = useRef(null)
  // useState 的数据改变会渲染组件
  const [num, setNum] = useState(0)

  const fn = () => {
    n.current++
    console.log(n)
  }

  const start = () => {
    timer.current = setInterval(() => {
      setNum(num => num + 1)
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer.current)
  }

  console.log('渲染App')

  return (
    <div>
      <h1>useRef</h1>
      <button onClick={fn}>log 次数</button>
      <p>{num} <button onClick={() => setNum(num + 1)}>num+</button></p>
      <button onClick={start}>开始计时</button>
      <button onClick={stop}>暂停</button>
    </div>
  )
}

export default App