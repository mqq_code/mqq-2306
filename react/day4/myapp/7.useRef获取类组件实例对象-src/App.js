import React, { useEffect, useRef } from 'react'
import Child from './components/Child'
import ChildFunc from './components/ChildFunc'

const App = () => {
  const ChildRef = useRef(null)
  const ChildFuncRef = useRef(null)
  const handleClick1 = () => {
    // 通过 ref 调用 class 子组件实例对象
    console.log(ChildRef.current)
    ChildRef.current.add()
  }
  const handleClick2 = () => {
    // ref 无法直接获取函数子组件的方法和数据
    console.log(ChildFuncRef)
  }
  return (
    <div>
      <h1>useRef</h1>
      <button onClick={handleClick1}>获取Child组件实例</button>
      <Child ref={ChildRef} />
      <hr />
      <button onClick={handleClick2}>获取函数子组件实例对象</button>
      <ChildFunc ref={ChildFuncRef} />
    </div>
  )
}

export default App