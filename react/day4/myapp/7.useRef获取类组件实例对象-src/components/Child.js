import React, { Component } from 'react'

export default class Child extends Component {
  state = {
    num: 0
  }
  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }
  render() {
    console.log(this)
    return (
      <div className='box'>
        <h2>Child</h2>
        {this.state.num}
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}
