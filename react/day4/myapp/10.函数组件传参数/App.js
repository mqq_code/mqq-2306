import React, { useEffect, useRef, useState } from 'react'
import Child from './components/Child'

const App = () => {
  const timer = useRef(null)
  const [num, setNum] = useState(0)

  const start = () => {
    timer.current = setInterval(() => {
      setNum(num => num + 1)
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer.current)
  }

  useEffect(() => {
    return () => {
      clearInterval(timer.current)
    }
  }, [])


  return (
    <div>
      <h1>useRef</h1>
      <p>{num}</p>
      <button onClick={start}>开始计时</button>
      <button onClick={stop}>暂停</button>
      <Child
        title="123"
        a={100}
        b={[1,2,3]}
        c={<div>我是ccccc</div>}
        num={num}
        start={start}
      >
        <p>姓名: <input type="text" /></p>
      </Child>
    </div>
  )
}

export default App