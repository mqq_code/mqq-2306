import React from 'react'

const Child = (props) => {
  console.log(props)
  return (
    <div className='box'>
      <h2>Child</h2>
      <h3>{props.title}</h3>
      <p>{props.a}</p>
      {props.c}
      <div className="box">
        父组件的num: {props.num}
        <button onClick={props.start}>开始</button>
      </div>
      {props.children}
    </div>
  )
}

export default Child