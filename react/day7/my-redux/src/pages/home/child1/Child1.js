import React, { useState, useEffect } from 'react'
import store from '../../../store'

// console.log('获取store中的数据', store.getState())
// console.log(store)

const Child1 = () => {

  const [state, setState] = useState(store.getState())

  useEffect(() => {
    // 监听store的数据修改
    const unSubscribe = store.subscribe(() => {
      console.log('store中的数据改变了', store.getState())
      setState({ num: store.getState().num })
    })
    return () => {
      // 组件销毁时清除store的监听
      unSubscribe()
    }
  }, [])

  return (
    <div>
      <h1>Child1</h1>
      {state.num}
      <button onClick={() => {
        // 改变store的数据，必须通过store的 dispatch 方法
        // dispatch 需要传入一个 action 描述本次如何修改
        // action 是一个对象，必须有 type 属性
        // 调用dispatch会触发store的reducer函数执行
        store.dispatch({ type: 'add_num' })
      }}>+</button>
    </div>
  )
}

export default Child1