import React, { useEffect, useState } from 'react'
import store from '../../../store'

const Child2 = () => {

  const [num, setNum] = useState(store.getState().num)

  useEffect(() => {
    const unSubscribe = store.subscribe(() => {
      console.log('child2 监听store改变', store.getState())
      setNum(store.getState().num)
    })
    return () => {
      // 组件销毁时清除store的监听
      unSubscribe()
    }
  }, [])

  return (
    <div>
      <h2>Child2</h2>
      <button onClick={() => {
        store.dispatch({ type: 'sub_num' })
      }}>-</button>
      {num}
    </div>
  )
}

export default Child2