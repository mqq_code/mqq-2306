import React from 'react'
import Home from './pages/home/Home'
import Child1 from './pages/home/child1/Child1'
import Child2 from './pages/home/child2/Child2'
import Child3 from './pages/home/child3/Child3'
import Child4 from './pages/home/child4/Child4'
import Child5 from './pages/home/child5/Child5'
import Login from './pages/login/Login'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'



const App = () => {
  return (
    <Switch>
      <Route path="/home" render={() => {
        return (
          <Home>
            <Switch>
              <Route path="/home/child1" component={Child1} />
              <Route path="/home/child2" component={Child2} />
              <Route path="/home/child3" component={Child3} />
              <Route path="/home/child4" component={Child4} />
              <Route path="/home/child5" component={Child5} />
              <Redirect from="/home" to="/home/child1" />
            </Switch>
          </Home>
        )
      }} />
      <Route path="/login" component={Login} />
      <Redirect from="/" to="/home" />
    </Switch>
  )
}

export default App