import { createStore } from 'redux'


const initState = {
  num: 100,
  name: '测试',
  age: 22
}

// 返回 state
const reducer = (state, action) => {
  console.log('reducer函数执行了', action, state)
  if (action.type === 'add_num') {
    return {...state, num: state.num + 1}
  } else if (action.type === 'sub_num') {
    return {...state, num: state.num - 1}
  }
  return state
}


// 创建 store, createStore(reducer)
const store = createStore(reducer, initState)

export default store

