import React, { useState } from 'react'
import style from './Login.module.scss'
import { useHistory } from 'react-router-dom'

const login = ({ username, password }) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (username === '小明' && password === '123') {
        resolve({
          code: 0,
          msg: '成功',
          token: Math.random().toString().slice(2) + Date.now()
        })
      } else {
        resolve({
          code: -1,
          msg: '用户名或密码错误'
        })
      }
    }, 1000)
  })
}

const Login = () => {
  const [form, setForm] = useState({ username: '', password: '' })
  const [loading, setLoading] = useState(false)
  const history = useHistory()
  const changeForm = (e, key) => {
    setForm({
      ...form,
      [key]: e.target.value
    })
  }

  const submit = async () => {
    setLoading(true)
    const res = await login(form)
    setLoading(false)
    if (res.code === 0) {
      alert('登录成功')
      localStorage.setItem('token', res.token)
      history.push('/')
    } else {
      alert(res.msg)
    }
  }
  return (
    <div className={style.login}>
      <div className={style.form}>
        <h2>登录</h2>
        <p><input type="text" value={form.username} onChange={e => changeForm(e, 'username')} placeholder='用户名' /></p>
        <p><input type="password" value={form.password} onChange={e => changeForm(e, 'password')} placeholder='密码' /></p>
        <p><button disabled={loading} onClick={submit}>{loading ? '登录中...' : '登录'}</button></p>
      </div>
    </div>
  )
}

export default Login