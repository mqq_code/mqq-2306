import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { setBannerAction, subNumAction, addNumAction } from '../store/actions'

const Cinema = () => {
  const state = useSelector(s => s)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(setBannerAction)
  }, [])

  return (
    <div>
      <h1>Cinema</h1>
      <Swiper
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {state.banners.map(item =>
          <SwiperSlide key={item.targetId}>
            <img src={item.imageUrl} alt="" />
          </SwiperSlide>
        )}
      </Swiper>
      <hr />
      <button onClick={() => dispatch(subNumAction())}>-</button>
      {state.num}
      <button onClick={() => dispatch(addNumAction(5))}>+5</button>
    </div>
  )
}

export default Cinema