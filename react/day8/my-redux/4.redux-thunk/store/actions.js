
import axios from 'axios'


export const SET_BANNER = 'SET_BANNER'
export const SUB_NUM = 'SUB_NUM'
export const ADD_NUM = 'ADD_NUM'
export const SET_TITLE = 'SET_TITLE'
export const ADD_LIST = 'ADD_LIST'


export const setBannerAction = (dispatch) => {
  axios.get('http://zyxcl.xyz/music_api/banner').then(res => {
    dispatch({
      type: SET_BANNER,
      payload: res.data.banners
    })
  })
}


export const subNumAction = () =>{
  return {
    type: SUB_NUM
  }
}

export const addNumAction = (payload) =>{
  return {
    type: ADD_NUM,
    payload
  }
}