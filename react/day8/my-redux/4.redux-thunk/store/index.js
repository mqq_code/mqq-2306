import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import { thunk } from 'redux-thunk'
import {
  ADD_NUM,
  SUB_NUM,
  SET_BANNER,
  SET_TITLE,
  ADD_LIST
} from './actions'

const initState = {
  list: [],
  title: 'redux',
  num: 0,
  banners: []
}

const reducer = (state = initState, action) => {
  // 深拷贝
  const newState = JSON.parse(JSON.stringify(state))
  if (action.type === ADD_NUM) {
    newState.num += action.payload
  } else if (action.type === SUB_NUM) {
    newState.num--
  } else if (action.type === SET_TITLE) {
    newState.title = action.payload
  } else if (action.type === ADD_LIST) {
    newState.list.push(action.payload)
  } else if (action.type === SET_BANNER) {
    newState.banners = action.payload
  }
  return newState
}

// applyMiddleware: 给 sotre 添加中间件，给dispatch增加功能
// logger: dispatch 的时候自动打印log
// redux-thunk: 让 dispatch 可以接受一个函数作为参数
const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store



// const reducer = (state = initState, action) => {
//   if (action.type === 'ADD_NUM') {
//     return {...state, num: state.num + action.payload}
//   } else if (action.type === 'SUB_NUM') {
//     return {...state, num: state.num - 1}
//   } else if (action.type === 'SET_TITLE') {
//     return {...state, title: action.payload}
//   }
//   return state
// }
