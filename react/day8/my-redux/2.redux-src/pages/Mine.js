import React, { useEffect, useState } from 'react'
import store from '../store'

console.log(store.getState())

const Mine = () => {
  const [num, setNum] = useState(store.getState().num)

  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      console.log('store的数据改变了', store.getState())
      setNum(store.getState().num)
    })
    return () => {
      // 清除监听
      unsubscribe()
    }
  }, [])

  return (
    <div>
      <h2>Mine</h2>
      <button onClick={() => {
        store.dispatch({
          type: 'SUB_NUM'
        })
      }}>-</button>
      {num}
      <button onClick={() => {
        store.dispatch({
          type: 'ADD_NUM'
        })
      }}>+</button>
    </div>
  )
}

export default Mine