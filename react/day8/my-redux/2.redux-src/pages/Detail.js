import React from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'

const Detail = () => {
  const location = useLocation()
  // 可以获取search参数
  const [searchParams, setSearch] = useSearchParams()
  console.log(location)
  console.log(searchParams.get('val'))

  const params = useParams()
  console.log(params)

  return (
    <div>
      <h1>Detail</h1>
      <button onClick={() => {
        setSearch({ val: 'vvvvvvvvv'})
      }}>修改地址栏 val</button>
    </div>
  )
}

export default Detail