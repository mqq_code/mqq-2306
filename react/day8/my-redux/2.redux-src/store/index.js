import { createStore } from 'redux'


const initState = {
  list: [],
  title: 'redux',
  num: 0
}

const reducer = (state = initState, action) => {
  console.log('执行了 reducer', state, action)
  if (action.type === 'ADD_NUM') {
    return {...state, num: state.num + 1}
  } else if (action.type === 'SUB_NUM') {
    return {...state, num: state.num - 1}
  }
  return state
}


const store = createStore(reducer)

export default store

