import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { setBannerAction, addNumAction } from '../store/actions'

const Mine = () => {
  const dispatch = useDispatch()
  const state = useSelector(s => s)
  console.log(state)
  const banners = useSelector(s => s.a.banners)
  const num = useSelector(s => s.b.num)

  useEffect(() => {
    dispatch(setBannerAction)
  }, [])

  return (
    <div>
      <h2>Mine</h2>
      <button onClick={() => dispatch(addNumAction(3))}>{num}</button>
      <Swiper
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {banners.map(item =>
          <SwiperSlide key={item.targetId}>
            <img src={item.imageUrl} alt="" />
          </SwiperSlide>
        )}
      </Swiper>
    </div>
  )
}

export default Mine