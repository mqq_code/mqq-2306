import React from 'react'
import {
  Outlet,
  NavLink
} from 'react-router-dom'

const Home = () => {
  return (
    <div>
      <nav>
        <NavLink to="/home/movie">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </nav>
      <Outlet />
    </div>
  )
}

export default Home