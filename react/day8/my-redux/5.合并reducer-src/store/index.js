import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import { thunk } from 'redux-thunk'
import banner from './reducers/banner'
import test from './reducers/test'

const reducer = combineReducers({
  a: banner,
  b: test
})


const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store


