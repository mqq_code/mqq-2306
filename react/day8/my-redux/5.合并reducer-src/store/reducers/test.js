
const initState = {
  list: [],
  title: 'redux',
  num: 0
}

const reducer = (state = initState, action) => {
  const newState = JSON.parse(JSON.stringify(state))
  if (action.type === 'ADD_NUM') {
    newState.num += action.payload
  } else if (action.type === 'SUB_NUM') {
    newState.num--
  } else if (action.type === 'SET_TITLE') {
    newState.title = action.payload
  } else if (action.type === 'ADD_LIST') {
    newState.list.push(action.payload)
  }
  return newState
}

export default reducer