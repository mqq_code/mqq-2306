



const initState = {
  banners: []
}

const reducer = (state = initState, action) => {
  const newState = JSON.parse(JSON.stringify(state))
  if (action.type === 'SET_BANNER') {
    newState.banners = action.payload
  }
  return newState
}

export default reducer