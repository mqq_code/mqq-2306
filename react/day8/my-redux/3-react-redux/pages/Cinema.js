import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Cinema = () => {
  const state = useSelector(s => s)
  const dispatch = useDispatch()
  
  return (
    <div>
      <h1>Cinema</h1>
      <h2>{state.title}</h2>
      <p>{state.num}</p>
      <button onClick={() => {
        dispatch({
          type: 'SUB_NUM'
        })
      }}>-</button>
    </div>
  )
}

export default Cinema