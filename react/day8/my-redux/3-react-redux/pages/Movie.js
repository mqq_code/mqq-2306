import React from 'react'
import {
  useNavigate, // useHistory => useNavigate
} from 'react-router-dom'
import { connect } from 'react-redux'


const Movie = (props) => {
  const navigate = useNavigate()
  return (
    <div>
      <h2>Movie</h2>
      <h3>
        {props.num}
        <button onClick={() => {
          props.dispatch({ type: 'ADD_NUM', payload: 3 })
        }}>+3</button>
      </h3>
      <button onClick={() => {
        props.dispatch({
          type: 'ADD_LIST',
          payload: Math.random()
        })
      }}>add list</button>
      <ul>
        {props.list.map((item, index) =>
          <li key={index} onClick={() => {
            // navigate(`/detail?val=${item}&a=100&b=200`)
            navigate(`/detail/${item}`)
          }}>{item}</li>
        )}
      </ul>
    </div>
  )
}

// connect: 连接组件和store
const mapStateToProps = state => {
  // console.log('store中的state', state)
  // return 的对象的属性会传给 Movie 的 props
  return {
    list: state.list,
    num: state.num
  }
}
export default connect(mapStateToProps)(Movie)