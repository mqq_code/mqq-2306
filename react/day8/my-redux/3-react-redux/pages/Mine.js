import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Mine = () => {
  const dispatch = useDispatch()
  // 读取store中的数据
  const num = useSelector(s => s.num)
  const title = useSelector(s => s.title)

  const changeTitle = e => {
    dispatch({
      type: 'SET_TITLE',
      payload: e.target.value
    })
  }

  return (
    <div>
      <h2>Mine</h2>
      <h3>{title}</h3>
      <input type="text" value={title} onChange={changeTitle} />
      <button onClick={() => {
        dispatch({ type: 'SUB_NUM' })
      }}>-</button>
      {num}
      <button onClick={() => {
        dispatch({ type: 'ADD_NUM', payload: 2 })
      }}>+2</button>
    </div>
  )
}

export default Mine