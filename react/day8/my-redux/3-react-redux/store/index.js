import { createStore } from 'redux'


const initState = {
  list: [],
  title: 'redux',
  num: 0
}

const reducer = (state = initState, action) => {
  console.log('reducer', state,  action)
  // 深拷贝
  const newState = JSON.parse(JSON.stringify(state))
  if (action.type === 'ADD_NUM') {
    newState.num += action.payload
  } else if (action.type === 'SUB_NUM') {
    newState.num--
  } else if (action.type === 'SET_TITLE') {
    newState.title = action.payload
  } else if (action.type === 'ADD_LIST') {
    newState.list.push(action.payload)
  }
  return newState
}


const store = createStore(reducer)

export default store



// const reducer = (state = initState, action) => {
//   if (action.type === 'ADD_NUM') {
//     return {...state, num: state.num + action.payload}
//   } else if (action.type === 'SUB_NUM') {
//     return {...state, num: state.num - 1}
//   } else if (action.type === 'SET_TITLE') {
//     return {...state, title: action.payload}
//   }
//   return state
// }
