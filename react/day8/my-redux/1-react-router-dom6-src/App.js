import React from 'react'
import Home from './pages/Home'
import Movie from './pages/Movie'
import Cinema from './pages/Cinema'
import Mine from './pages/Mine'
import Detail from './pages/Detail'
import Login from './pages/Login'
import Notfound from './pages/404'
import {
  Routes, // Switch => Routes
  Route,
  Navigate,
  useRoutes
} from 'react-router-dom'

const App = () => {
  const router = useRoutes([
    {
      path: '/home',
      element: <Home />,
      children: [
        {
          path: '/home/movie',
          element: <Movie />
        },
        {
          path: '/home/cinema',
          element: <Cinema />
        },
        {
          path: '/home/mine',
          element: <Mine />
        },
        {
          path: '/home',
          element: <Navigate to="/home/movie" />
        }
      ]
    },
    {
      path: '/detail/:id',
      element: <Detail />
    },
    {
      path: '/login',
      element: <Login />
    },
    {
      path: '/',
      element: <Navigate to="/home" />
    },
    {
      path: '*',
      element: <Notfound />
    }
  ])

  return router
}

export default App



// const App = () => {
//   return (
//     <Routes>
//       <Route path='/home' element={<Home />}>
//         <Route path='/home/movie' element={<Movie />} />
//         <Route path='/home/cinema' element={<Cinema />} />
//         <Route path='/home/mine' element={<Mine />} />
//         <Route path='/home' element={<Navigate to="/home/movie" />} />
//       </Route>
//       <Route path='/detail/:id' element={<Detail />} />
//       <Route path='/login' element={<Login />} />
//       <Route path="/" element={<Navigate to="/home" />} />
//       <Route path='*' element={<Notfound />} />
//     </Routes>
//   )
// }

// export default App