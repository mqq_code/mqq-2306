import React from 'react'
import {
  useNavigate, // useHistory => useNavigate
} from 'react-router-dom'

const arr = new Array(50).fill(0).map(Math.random)

const Movie = () => {
  const navigate = useNavigate()

  return (
    <div>
      <h2>Movie</h2>
      <ul>
        {arr.map((item, index) =>
          <li key={index} onClick={() => {
            // navigate(`/detail?val=${item}&a=100&b=200`)
            navigate(`/detail/${item}`)
          }}>{item}</li>
        )}
      </ul>
    </div>
  )
}

export default Movie