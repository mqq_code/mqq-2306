import React, { Component, createRef } from 'react'
import style from './App.module.scss'
import Keyboard from './components/keyboard/Keyboard'
import Control from './components/control/Control'
import axios from 'axios'

class App extends Component {

  state = {
    // 音乐列表
    list: [],
    // 开关
    power: true,
    // 当前的音乐
    curMusic: null,
    // bank 开关
    bank: false
  }

  audioRef = createRef()

  async componentDidMount() {
    const res = await axios.get('https://zyxcl.xyz/exam_api/music/list')
    this.setState({
      list: res.data.value.map(item => ({ ...item, isDown: false }))
    })
    this.audioRef.current.volume = 0.5
  }

  keyDown = item => {
    // 拷贝数据
    const list = [...this.state.list]
    // 根据传过来的对象查找下标
    const index = list.findIndex(v => v.id === item.id)
    // 修改按下状态
    list[index].isDown = true
    // 更新数据
    this.setState({ list })
    // 判断开关是否打开
    if (this.state.power) {
      this.setState({
        curMusic: item
      })
      this.audioRef.current?.load()
    }
  }
  keyUp = item => {
    // 拷贝数据
    const list = [...this.state.list]
    // 根据传过来的对象查找下标
    const index = list.findIndex(v => v.id === item.id)
    // 修改按下状态
    list[index].isDown = false
    // 更新数据
    this.setState({ list })
  }
  changePower = () => {
    const power = !this.state.power
    this.setState({
      power,
      curMusic: power ? this.state.curMusic : null
    })
  }
  changeBank = () => {
    const bank = !this.state.bank
    this.setState({
      bank,
      curMusic: null
    })
  }
  changeVolume = (volume) => {
    this.audioRef.current.volume = volume
  }

  render() {
    const { list, power, bank, curMusic } = this.state
    return (
      <div className={style.app}>
        <main>
          <Keyboard power={power} list={list} keyDown={this.keyDown} keyUp={this.keyUp}  />
          <Control
            power={power}
            bank={bank}
            curMusic={curMusic}
            changePower={this.changePower}
            changeBank={this.changeBank}
            changeVolume={this.changeVolume}
          />
        </main>
        <audio ref={this.audioRef} src={bank ? curMusic?.bankUrl : curMusic?.url} autoPlay />
      </div>
    )
  }
}

export default App
