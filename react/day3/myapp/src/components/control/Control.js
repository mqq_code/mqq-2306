import React, { Component, createRef } from 'react'
import style from './control.module.scss'
import classNames from 'classnames'
import Switch from '../switch/Switch'

export default class Control extends Component {
  state = {
    left: '50%'
  }
  volumeRef = createRef()
  isMouseDown = false
  mouseDown = () => {
    this.isMouseDown = true
  }
  mouesMove = (e) => {
    if (this.isMouseDown) {
      let left = e.clientX - this.volumeRef.current.getBoundingClientRect().left - 5
      if (left < 0) left = 0
      if (left > 190) left = 190
      const progress = left / 190
      this.setState({ left })
      // 修改音量
      this.props.changeVolume(progress)
    }
  }
  mouseUp = () => {
    this.isMouseDown = false
  }
  componentDidMount() {
    document.addEventListener('mousemove', this.mouesMove)
    document.addEventListener('mouseup', this.mouseUp)
  }
  componentWillUnmount() {
    document.removeEventListener('mousemove', this.mouesMove)
    document.removeEventListener('mouseup', this.mouseUp)
  }
  render() {
    const {
      power,
      changePower,
      curMusic,
      bank,
      changeBank
    } = this.props
    const { left } = this.state
    return (
      <div className={style.control}>
        <Switch title="Power" isOn={power} change={changePower} />
        <div className={style.title}>{curMusic?.id}</div>
        <div className={style.volume} ref={this.volumeRef}>
          <b style={{ left }} onMouseDown={this.mouseDown}></b>
        </div>
        <Switch title="Bank" isOn={bank} change={changeBank} />
      </div>
    )
  }
}
