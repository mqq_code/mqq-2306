import React, { Component } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'
class Keyboard extends Component {

  // audios = []
  // pushAudio = el => {
  //   this.audios.push(el)
  // }

  componentDidMount () {
    document.addEventListener('keydown', e => {
      const item = this.props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        this.props.keyDown(item)
      }
    })
    document.addEventListener('keyup', e => {
      const item = this.props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        this.props.keyUp(item)
      }
    })
  }

  render() {
    const { list, power, keyDown, keyUp } = this.props
    return (
      <div className={style.keyboard}>
        {list.map((item, index) =>
          <p
            key={item.id}
            className={classNames({
              [style.keydown]: item.isDown,
              [style.orange]: item.isDown && power
            })}
            onMouseDown={() => keyDown(item)}
            onMouseUp={() => keyUp(item)}
          >
            {item.keyTrigger}
            {/* <audio ref={this.pushAudio} src={item.url}></audio> */}
          </p>
        )}
      </div>
    )
  }
}
export default Keyboard