import React, { Component } from 'react'
import classNames from 'classnames'
import style from './switch.module.scss'

export default class Switch extends Component {
  render() {
    const { title, isOn, change } = this.props
    return (
      <div className={style.switch}>
        <h3>{title}</h3>
        <p className={classNames({ [style.on]: isOn })} onClick={change}><span></span></p>
      </div>
    )
  }
}
