import React, { Component } from 'react'
import style from './App.module.scss'

class App extends Component {

  state = {
    form: {
      name: '默认姓名',
      age: 20,
      sex: '男',
      address: 'beijing',
      married: true,
      remark: '默认备注'
    }
  }
  changeName = e => {
    // 拷贝对象
    const form = {...this.state.form}
    form.name = e.target.value
    this.setState({ form })
    
    // this.setState({
    //   form: {
    //     ...this.state.form,
    //     name: e.target.value
    //   }
    // })
  }
  changeForm = (e, key) => {
    const form = {...this.state.form}
    if (key === 'married') {
      form[key] = e.target.checked
    } else {
      form[key] = e.target.value
    }
    this.setState({ form })
  }
  submit = () => {
    console.log(this.state.form)
  }
  render() {
    const { form } = this.state
    return (
      <div className={style.app}>
        <h1>表单</h1>
        <p>姓名： <input type="text" value={form.name} onChange={e => this.changeForm(e, 'name')} /></p>
        <p>年龄： <input type="text" value={form.age} onChange={e => this.changeForm(e, 'age')} /></p>
        <p>性别： 
          <input type="radio" name="sex" id="men" checked={form.sex === '男'} value="男" onChange={e => this.changeForm(e, 'sex')} /> <label htmlFor='men'>男</label>
          <input type="radio" name="sex" id="women" checked={form.sex === '女'} value="女" onChange={e => this.changeForm(e, 'sex')} /> <label htmlFor='women'>女</label>
        </p>
        <p>地址：
          <select value={form.address} onChange={e => this.changeForm(e, 'address')} >
            <option value="">请选择</option>
            <option value="beijing">北京</option>
            <option value="shanghai">上海</option>
            <option value="guangzhou">广州</option>
          </select>
        </p>
        <p>
          婚否：<input type="checkbox" checked={form.married} onChange={e => this.changeForm(e, 'married')} />
        </p>
        <p>
          备注：
          <textarea cols="30" rows="10" value={form.remark} onChange={e => this.changeForm(e, 'remark')}></textarea>
        </p>
        {JSON.stringify(form)}
        <button onClick={this.submit}>提交</button>
      </div>
    )
  }
}

export default App
