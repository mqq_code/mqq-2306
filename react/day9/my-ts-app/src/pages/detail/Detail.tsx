import React, { useEffect, useState } from 'react'
import style from './Detail.module.scss'
import { useParams } from 'react-router-dom'
import { getDetail } from '../../services'


const Detail: React.FC = () => {
  const params = useParams<{ id: string }>()
  const [info, setInfo] = useState<DetailFilm>({} as DetailFilm)

  const getFilmDetail = async () => {
    const res = await getDetail(params.id!)
    setInfo(res.data.data.film)
  }
  useEffect(() => {
    getFilmDetail()
  }, [])

  return (
    <div className={style.detail}>
      <h2>{info.name}</h2>
      <img src={info.poster} alt="" />
    </div>
  )
}

export default Detail