import React, { useEffect, useState } from 'react'
import style from './Hot.module.scss'
import { getHotList } from '../../../../services'
import Film from '../components/film/Film'

const Hot: React.FC = () => {

  const [films, setFilms] = useState<Film[]>([])
  const [total, setTotal] = useState<number>(0)

  const getList = async () => {
    const res = await getHotList({
      cityId: 110100,
      pageNum: 1,
      pageSize: 10,
      type: 1,
      k: 2468906
    })
    setFilms(res.data.data.films)
    setTotal(res.data.data.total)
  }

  useEffect(() => {
    getList()
  }, [])
  
  return (
    <div className={style.hot}>
      {films.map(item =>
        <Film key={item.filmId} {...item} />
      )}
    </div>
  )
}

export default Hot