import React, { useState } from 'react'
import { Outlet } from 'react-router-dom'
import style from './Movie.module.scss'
import FixedNav from './components/fixednav/FixedNav'
import Nav from './components/nav/Nav'

const Movie: React.FC = () => {
  const [showFixedNav, setShow] = useState(false)

  const scroll = (e: React.UIEvent) => {
    if ((e.target as HTMLDivElement).scrollTop > 300) {
      setShow(true)
    } else {
      setShow(false)
    }
  }

  return (
    <div className={style.movie} onScroll={scroll}>
      <Nav />
      <Outlet />
      {showFixedNav && <FixedNav />}
    </div>
  )
}

export default Movie