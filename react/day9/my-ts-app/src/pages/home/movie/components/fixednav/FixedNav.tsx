import React from 'react'
import style from './FixedNav.module.scss'
import Nav from '../nav/Nav'
import { Link } from 'react-router-dom'

const FixedNav: React.FC = () => {
  return (
    <div className={style.fixNav}>
      <div className={style.title}>
        <Link to="/city">北京</Link>
        <h2>电影</h2>
      </div>
      <Nav />
    </div>
  )
}

export default FixedNav