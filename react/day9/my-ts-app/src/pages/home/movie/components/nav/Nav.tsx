import React, { useState, useEffect } from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import style from './Nav.module.scss'

const navlist = [
  { path: '/movie', title: '正在热映' },
  { path: '/movie/coming', title: '即将上映' },
]
const Nav: React.FC = () => {

  const location = useLocation()
  const [curIndex, setCurIndex] = useState(0)

  useEffect(() => {
    const index = navlist.findIndex(v => v.path === location.pathname)
    setCurIndex(index)
  }, [location])

  return (
    <nav className={style.nav}>
      {navlist.map(item =>
        <NavLink replace to={item.path} key={item.path}>{item.title}</NavLink>
      )}
      <div className={style.line} style={{ transform: `translateX(${curIndex * 100}%)` }}>
        <span></span>
      </div>
    </nav>
  )
}

export default Nav