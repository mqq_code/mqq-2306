import React from 'react'
import style from './Film.module.scss'
import { useNavigate } from 'react-router-dom'

interface IProps {
  filmId: number;
  poster: string;
  name: string;
  grade: string;
  actors: Actor[];
  nation: string;
  runtime: number;
}

const Film: React.FC<IProps> = (props) => {
  const navigate = useNavigate()
  const goDetail = () => {
    navigate(`/detail/${props.filmId}`)
  }
  return (
    <div className={style.film} onClick={goDetail}>
      <img src={props.poster} alt="" />
      <div className={style.right}>
        <h3>{props.name}</h3>
        <p>观众评分: {props.grade}</p>
        <p>主演: {props.actors.map(v => v.name)}</p>
        <p>{props.nation} | {props.runtime}分钟</p>
      </div>
    </div>
  )
}

export default Film