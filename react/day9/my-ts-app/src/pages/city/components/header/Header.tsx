import React, { useState } from 'react'
import style from './Header.module.scss'

interface IProps {
  onFilter: (text: string) => void;
  onCancel: () => void;
}

const Header: React.FC<IProps> = (props) => {

  const [showCancel, setShowCancel] = useState(false)
  const cancel = () => {
    setShowCancel(false)
    props.onCancel()
  }
  return (
    <div className={style.header}>
      <h2>标题</h2>
      <div className={style.search}>
        <div className={style.searchInpWrap}>
          <input
            type="text"
            placeholder='请输入搜索的城市或者拼音'
            onFocus={() => setShowCancel(true)}
            onChange={(e) => props.onFilter(e.target.value)}
          />
        </div>
        {showCancel && <div className={style.cencelBtn} onClick={cancel}>取消</div>}
      </div>
    </div>
  )
}

export default Header