import React, { useMemo, useRef, useState } from 'react'
import style from './List.module.scss'

interface IProps {
  cities: CityItem[];
}

interface CityData {
  type: string;
  isHot?: boolean;
  children: CityItem[];
}

const formatCities = (cities: CityItem[]): CityData[] => {
  const arr: CityData[] = []
  const hot: CityData = {
    type: '热门城市',
    isHot: true,
    children: []
  }
  cities.forEach(item => {
    const first = item.pinyin[0].toUpperCase()
    const type = arr.find(v => v.type === first)
    if (type) {
      type.children.push(item)
    } else {
      arr.push({
        type: first,
        children: [item]
      })
    }
    if (item.isHot) {
      hot.children.push(item)
    }
  })
  arr.unshift(hot)
  return arr
}

const List: React.FC<IProps> = (props) => {
  const listRef = useRef<HTMLDivElement>(null)
  const timer = useRef<any>(null)
  const [tip, setTip] = useState('')
  const citiesData = useMemo(() => {
    return formatCities(props.cities)
  }, [props.cities])

  const jump = (index: number, text: string) => {
    // 跳转楼层
    const type = listRef.current?.children[index + 1] as HTMLDivElement
    listRef.current!.scrollTop = type.offsetTop
    // 展示小黑框
    if (timer.current) clearTimeout(timer.current)
    setTip(text)
    timer.current = setTimeout(() => {
      setTip('')
    }, 2000)
  }
  return (
    <div className={style.list} ref={listRef}>
      {citiesData.map((item, index) => {
        return (
          <div key={item.type} className={item.isHot ? style.hot : style.type}>
            <h3>{item.type}</h3>
            <ul>
              {item.children.map(val =>
                <li key={val.cityId}>{val.name}</li>
              )}
            </ul>
          </div>
        )
      })}
      <div className={style.letter}>
        {citiesData.slice(1).map((item, index) =>
          <p key={item.type} onClick={() => jump(index, item.type)}>{item.type}</p>
        )}
      </div>
      <div className={style.tips} style={{ display: tip ? 'block' : 'none' }}>{tip}</div>
    </div>
  )
}

export default List