import React, { useEffect, useMemo, useState } from 'react'
import style from './City.module.scss'
import { getCityList } from '../../services'
import Header from './components/header/Header'
import List from './components/list/List'
import SearchList from './components/searchList/SearchList'

const City = () => {
  const [cities, setCities] = useState<CityItem[]>([])
  const [showResult, setShowResult] = useState(false)
  const [filteText, setFilteText] = useState('')

  // input搜索，展示搜索结果
  const filterCities = (text: string) => {
    setFilteText(text)
    setShowResult(true)
  }
  // 取消搜索
  const onCancel = () => setShowResult(false)

  useEffect(() => {
    getCityList().then(res => {
      // 根据首字母排序
      res.data.data.cities.sort((a, b) => {
        return a.pinyin.charCodeAt(0) - b.pinyin.charCodeAt(0)
      })
      setCities(res.data.data.cities)
    })
  }, [])
  
  // 根据搜索框筛选数据
  const filterResult = useMemo(() => {
    if (!filteText) return []
    return cities.filter(item => item.pinyin.includes(filteText) || item.name.includes(filteText))
  }, [filteText, cities])

  return (
    <div className={style.city}>
      <Header onFilter={filterCities} onCancel={onCancel} />
      <main>
        {showResult ?
          <SearchList list={filterResult} />
        :
          <List cities={cities} />
        }
      </main>
    </div>
  )
}

export default City