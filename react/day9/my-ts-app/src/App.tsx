import React from 'react'
import Home from './pages/home/Home'
import Movie from './pages/home/movie/Movie'
import Hot from './pages/home/movie/hot/Hot'
import Coming from './pages/home/movie/coming/Coming'
import Cinema from './pages/home/cinema/Cinema'
import News from './pages/home/news/News'
import Mine from './pages/home/mine/Mine'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import City from './pages/city/City'
import { useRoutes, Navigate } from 'react-router-dom'
// 第三方包
// 1. 自带 ts 声明文件
// 2. 没有声明文件，npm i -D @types/包名
// 3. 自己定义对应的声明文件
import Mock from 'mockjs'

// alert(window._aa_)
// alert(_bb_)


const App = () => {
  const router = useRoutes([
    {
      path: '/',
      element: <Home />,
      children: [
        {
          path: '/movie',
          element: <Movie />,
          children: [
            {
              path: '/movie',
              element: <Hot />
            },
            {
              path: '/movie/coming',
              element: <Coming />
            }
          ]
        },
        {
          path: '/cinema',
          element: <Cinema />
        },
        {
          path: '/news',
          element: <News />
        },
        {
          path: '/mine',
          element: <Mine />
        },
        {
          path: '/',
          element: <Navigate to="/movie" />
        }
      ]
    },
    {
      path: '/detail/:id',
      element: <Detail />
    },
    {
      path: '/login',
      element: <Login />
    },
    {
      path: '/city',
      element: <City />
    }
  ])

  return router
}

export default App