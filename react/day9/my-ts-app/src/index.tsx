import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import { BrowserRouter } from 'react-router-dom'

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

// .ts 只能写ts语法
// .tsx 可以使用jsx语法
// .d.ts 类型声明文件，只能写ts类型，不可以写逻辑