// 主演
interface Actor {
  avatarAddress: string;
  name: string;
  role: string;
}
// 电影
interface Film {
  filmId: number;
  name: string;
  nation: string;
  poster: string;
  category: string;
  grade: string;
  runtime: number;
  actors: Actor[];
}
// hotlit 接口返回值
interface HotFilmRes {
  status: number;
  msg: string;
  data: {
    films: Film[];
    total: number;
  }
}
// hotlist 参数类型
interface HotParams {
  cityId: number;
  pageNum: number;
  pageSize: number;
  type: number;
  k: number;
}

// 详情接口
type DetailFilm = Film & {
  photos: string[];
}
interface FilmDetailRes {
  status: number;
  msg: string;
  data: {
    film: DetailFilm
  }
}

// 城市列表返回值
interface CityItem {
  cityId: number;
  isHot: 0 | 1;
  name: string;
  pinyin: string;
}
interface CityListRes {
  status: number;
  msg: string;
  data: {
    cities: CityItem[]
  }
}