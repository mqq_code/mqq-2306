import axios from 'axios'


export const getHotList = (params: HotParams) => {
  return axios.get<HotFilmRes>('https://m.maizuo.com/gateway', {
    params,
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033","bc":"110100"}',
      'X-Host': 'mall.film-ticket.film.list'
    }
  })
}

export const getDetail = (filmId: number | string) => {
  return axios.get<FilmDetailRes>('https://m.maizuo.com/gateway', {
    params: {
      filmId,
      k: 8810555
    },
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033","bc":"110100"}',
      'X-Host': 'mall.film-ticket.film.info'
    }
  })
}


export const getCityList = () => {
  return axios.get<CityListRes>('https://m.maizuo.com/gateway', {
    params: {
      k: 6311516
    },
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033","bc":"110100"}',
      'X-Host': 'mall.film-ticket.city.list'
    }
  })
}