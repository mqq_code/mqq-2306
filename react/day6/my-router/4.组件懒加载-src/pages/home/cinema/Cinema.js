import React, { lazy, Suspense } from 'react'
// import Child from './Child'
const Child = lazy(() => import(/* webpackChunkName: 'cinema-child' */'./Child'))

const Cinema = () => {
  return (
    <div>
      <h2>Cinema</h2>
      <Suspense fallback={<div>加载中...</div>}>
        <Child />
      </Suspense>
    </div>
  )
}

export default Cinema