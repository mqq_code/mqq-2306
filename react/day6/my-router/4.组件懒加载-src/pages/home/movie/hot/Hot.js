import React, { useEffect, useState } from 'react'
import style from './Hot.module.scss'
import axios from 'axios'

const Hot = (props) => {
  const [list, setList] = useState([])
  const [total, setTotal] = useState(0)

  const getList = async () => {
    const res = await axios.get('https://m.maizuo.com/gateway', {
      params: {
        cityId: 440300,
        pageNum: 1,
        pageSize: 10,
        type: 1,
        k: 4581697
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033"}',
        'X-Host': 'mall.film-ticket.film.list'
      }
    })
    setList(res.data.data.films)
    setTotal(res.data.data.total)
  }

  useEffect(() => {
    getList()
  }, [])

  const goDetail = (filmId) => {
    // 传参数方式一：?后拼接参数
    // props.history.push(`/detail?filmId=${filmId}&a=100&b=200`)
    // props.history.replace('/detail')
    // 传参数方式二：动态路由
    // props.history.push(`/detail/${filmId}`)
    // 传参数方式三：state传值
    props.history.push(`/detail/${filmId}`, { name: '小明', id: filmId })
  }

  return (
    <div className={style.hot}>
      {list.map(item =>
        <div key={item.filmId} className={style.item} onClick={() => goDetail(item.filmId)}>
          <img src={item.poster} width={120} alt="" />
          <h3>{item.name}</h3>
        </div>
      )}
    </div>
  )
}

export default Hot