import React from 'react'
import style from './Movie.module.scss'
import {
  NavLink
} from 'react-router-dom'

const Movie = (props) => {
  return (
    <div className={style.movie}>
      <nav>
        <NavLink replace to="/home/movie/hot">正在热映</NavLink>
        <NavLink replace to="/home/movie/coming">即将上映</NavLink>
      </nav>
      <div className={style.content}>
        {props.children}
      </div>
    </div>
  )
}

export default Movie