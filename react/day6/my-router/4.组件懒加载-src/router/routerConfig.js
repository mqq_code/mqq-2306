import { lazy } from 'react'
import Home from '../pages/home/Home'
import Movie from '../pages/home/movie/Movie'
import Hot from '../pages/home/movie/hot/Hot'
import Coming from '../pages/home/movie/coming/Coming'
import Cinema from '../pages/home/cinema/Cinema'
// import News from '../pages/home/news/News'
// import Mine from '../pages/home/mine/Mine'
// import Detail from '../pages/detail/Detail'
import City from '../pages/city/City'
import Login from '../pages/login/Login'

// 异步加载组件
const Detail = lazy(() => import(/* webpackChunkName: 'detail' */'../pages/detail/Detail'))
const News = lazy(() => import(/* webpackChunkName: 'news' */'../pages/home/news/News'))
const Mine = lazy(() => import(/* webpackChunkName: 'mine' */'../pages/home/mine/Mine'))

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home/movie',
        component: Movie,
        children: [
          {
            path: '/home/movie/hot',
            component: Hot
          },
          {
            path: '/home/movie/coming',
            component: Coming
          },
          {
            path: '/home/movie',
            to: '/home/movie/hot'
          }
        ]
      },
      {
        exact: true,
        path: '/home/cinema',
        component: Cinema
      },
      {
        exact: true,
        path: '/home/news',
        component: News
      },
      {
        exact: true,
        path: '/home/mine',
        component: Mine
      },
      {
        exact: true,
        path: '/home',
        to: '/home/movie'
      }
    ]
  },
  {
    exact: true,
    path: '/detail/:id',
    component: Detail
  },
  {
    exact: true,
    path: '/city',
    component: City
  },
  {
    exact: true,
    path: '/login',
    component: Login
  },
  {
    exact: true,
    path: '/',
    to: '/home'
  }
]

export default routes