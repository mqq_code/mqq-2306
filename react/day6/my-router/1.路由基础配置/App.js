import React from 'react'
import Home from './pages/Home'
import Glass from './pages/Glass'
import User from './pages/User'
import {
  Route, // 配置路由
  Switch, // 只渲染匹配到的第一个路由
  Redirect, // 重定向组件
  Link, // 跳转路由
  NavLink // 跳转路由，会自动添加高亮类名
} from 'react-router-dom'

const App = () => {
  return (
    <div>
      <nav>
        <NavLink activeClassName="aaa" to="/home">首页</NavLink>
        <NavLink activeClassName="aaa" to="/glass">放大镜</NavLink>
        <NavLink activeClassName="aaa" to="/user">个人中心</NavLink>
      </nav>
      <Switch>
        <Route exact path="/home" component={Home} />
        <Route exact path="/glass" component={Glass} />
        <Route path="/user" component={User} />
        {/* exact: true 精准匹配 */}
        <Redirect exact from="/" to="/home" />
      </Switch>
    </div>
  )
}

export default App