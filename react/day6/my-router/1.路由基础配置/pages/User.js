import React from 'react'
import Wallet from './Wallet'
import Coupon from './Coupon'
import {
  Route,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

const User = () => {
  return (
    <div className='user'>
      <h1>个人中心</h1>
      <NavLink to="/user/wallet">钱包</NavLink>
      <NavLink to="/user/coupon">优惠券</NavLink>
      <div className="user-content">
        <Switch>
          <Route exact path="/user/wallet" component={Wallet} />
          <Route exact path="/user/coupon" component={Coupon} />
          <Redirect exact from="/user" to="/user/wallet" />
        </Switch>
      </div>
    </div>
  )
}

export default User