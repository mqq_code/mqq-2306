import React from 'react'
import RouterView from './router/RouterView'
import routes from './router/routerConfig'

const App = () => {
  return (
    <RouterView routes={routes} />
  )
}

export default App



// const App = () => {
//   return (
//     <Switch>
//       <Route path="/home" render={routeInfo =>
//         <Home {...routeInfo}>
//           <Switch>
//             <Route path="/home/movie" render={routeInfo =>
//               <Movie {...routeInfo}>
//                 <Switch>
//                   <Route exact path="/home/movie/hot" component={Hot} />
//                   <Route exact path="/home/movie/coming" component={Coming} />
//                   <Redirect exact from="/home/movie" to="/home/movie/hot" />
//                 </Switch>
//               </Movie>
//             } />
//             <Route exact path="/home/cinema" component={cinema} />
//             <Route exact path="/home/news" component={News} />
//             <Route exact path="/home/mine" component={Mine} />
//             <Redirect exact from="/home" to="/home/movie" />
//           </Switch>
//         </Home>
//       } />
//       <Route exact path="/city" component={City} />
//       <Route exact path="/detail/:id" component={Detail} />
//       <Route exact path="/login" component={Login} />
//       <Redirect exact from="/" to="/home" />
//     </Switch>
//   )
// }
