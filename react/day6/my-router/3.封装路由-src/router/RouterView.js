import {
  Route, // 配置路由
  Switch, // 只渲染匹配到的第一个路由
  Redirect // 重定向组件
} from 'react-router-dom'


const RouterView = (props) => {
  const routes = props.routes.filter(v => v.component)
  const redirects = props.routes.filter(v => v.to)
  return (
    <Switch>
      {routes.map(item =>
        <Route key={item.path} exact={item.exact} path={item.path} render={routeInfo => {
          const Com = item.component
          if (item.children) {
            return (
              <Com {...routeInfo}>
                <RouterView routes={item.children} />
              </Com>
            )
          }
          return <Com {...routeInfo} />
        }} />
      )}
      {redirects.map(item =>
        <Redirect key={item.path} exact={item.exact} from={item.path} to={item.to} />
      )}
    </Switch>
  )
}

export default RouterView