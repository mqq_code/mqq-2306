import React from 'react'
import style from './Home.module.scss'
import {
  NavLink
} from 'react-router-dom'

const Home = (props) => {
  return (
    <div className={style.home}>
      <main>
        {props.children}
      </main>
      <footer>
        <NavLink activeClassName={style.active} to="/home/movie">电影</NavLink>
        <NavLink activeClassName={style.active} to="/home/cinema">影院</NavLink>
        <NavLink activeClassName={style.active} to="/home/news">资讯</NavLink>
        <NavLink activeClassName={style.active} to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home