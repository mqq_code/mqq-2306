import React, { useEffect, useState } from 'react'
import axios from 'axios'
import {
  useLocation,
  useParams
} from 'react-router-dom'

const query = (search) => {
  const obj = {}
  const arr = search.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })
  return obj
}

const Detail = (props) => {
  const location = useLocation() // props.location
  const params = useParams() // props.match.params
  console.log(location)
  console.log(params)

  const [info, setInfo] = useState({})
  const getInfo = async (filmId) => {
    const res = await axios.get('https://m.maizuo.com/gateway', {
      params: {
        filmId,
        k: 1274227
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033"}',
        'X-Host': 'mall.film-ticket.film.info'
      }
    })
    console.log(res.data.data.film)
    setInfo(res.data.data.film)
  }

  useEffect(() => {
    // console.log('获取 query 参数', props.location.search)
    // const queryObj = query(props.location.search)
    // console.log('获取动态路由参数', props.match.params.id)
    console.log('获取state参数', props.location.state)
    getInfo( props.match.params.id)
  }, [])

  return (
    <div className='detail'>
      <h1>详情页面</h1>
      <h2>{info.name}</h2>
      <img src={info.poster} width={200} alt="" />
      <p>{info.category}</p>
      <p>{info.synopsis}</p>
      <div className="actors">
        <ul>
          {info.actors?.map(item =>
            <li key={item.name}>
              <img src={item.avatarAddress} alt="" />
              <h4>{item.name}</h4>
              <p>{item.role}</p>
            </li>
          )}
        </ul>
      </div>
      <div className="photos">
        <ul>
          {info.photos?.map(url =>
            <li key={url}>
              <img src={url} alt="" />
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Detail