import React from 'react'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import City from './pages/city/City'
import Login from './pages/login/Login'

import {
  Route, // 配置路由
  Switch, // 只渲染匹配到的第一个路由
  Redirect // 重定向组件
} from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route path="/home" component={Home} />
      {/* 动态路由 */}
      <Route exact path="/detail/:id" component={Detail} />
      <Route exact path="/city" component={City} />
      <Route exact path="/login" component={Login} />
      <Redirect from="/" to="/home" />
    </Switch>
  )
}

export default App