import React from 'react'
import style from './Home.module.scss'
import Movie from './movie/Movie'
import Cinema from './cinema/Cinema'
import News from './news/News'
import Mine from './mine/Mine'
import {
  Route,
  Switch,
  Redirect,
  NavLink
} from 'react-router-dom'

const Home = () => {
  return (
    <div className={style.home}>
      <main>
        <Switch>
          <Route path="/home/movie" component={Movie} />
          <Route exact path="/home/cinema" component={Cinema} />
          <Route exact path="/home/news" component={News} />
          <Route exact path="/home/mine" component={Mine} />
          <Redirect exact from="/home" to="/home/movie" />
        </Switch>
      </main>
      <footer>
        <NavLink activeClassName={style.active} to="/home/movie">电影</NavLink>
        <NavLink activeClassName={style.active} to="/home/cinema">影院</NavLink>
        <NavLink activeClassName={style.active} to="/home/news">资讯</NavLink>
        <NavLink activeClassName={style.active} to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home