import React, { useEffect, useState } from 'react'
import style from './Coming.module.scss'
import axios from 'axios'
import {
  withRouter,
  useHistory,
  useLocation,
  useParams
} from 'react-router-dom'

const Coming = (props) => {
  const [list, setList] = useState([])
  const [total, setTotal] = useState(0)
  const history = useHistory() // ==> props.history

  const getList = async () => {
    const res = await axios.get('https://m.maizuo.com/gateway', {
      params: {
        cityId: 440300,
        pageNum: 1,
        pageSize: 10,
        type: 2,
        k: 5050170
      },
      headers: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"1697785287468761320620033"}',
        'X-Host': 'mall.film-ticket.film.list'
      }
    })
    console.log(res.data.data.films)
    setList(res.data.data.films)
    setTotal(res.data.data.total)
  }

  useEffect(() => {
    getList()
  }, [])

  console.log(history)

  return (
    <div className={style.coming}>
      {list.map(item =>
        <div key={item.filmId} className={style.item} onClick={() => history.push(`/detail/${item.filmId}`)}>
          <img src={item.poster} width={120} alt="" />
          <h3>{item.name}</h3>
        </div>
      )}
    </div>
  )
}

export default Coming
// withRouter: 高阶组件，可以把路由信息传给组件的props
// export default withRouter(Coming)