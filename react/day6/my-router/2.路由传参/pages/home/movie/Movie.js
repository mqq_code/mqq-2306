import React from 'react'
import style from './Movie.module.scss'
import Hot from './hot/Hot'
import Coming from './coming/Coming'
import {
  Switch,
  Route,
  Redirect,
  NavLink
} from 'react-router-dom'

const Movie = () => {
  return (
    <div className={style.movie}>
      <nav>
        <NavLink replace to="/home/movie/hot">正在热映</NavLink>
        <NavLink replace to="/home/movie/coming">即将上映</NavLink>
      </nav>
      <div className={style.content}>
        <Switch>
          <Route exact path="/home/movie/hot" component={Hot} />
          <Route exact path="/home/movie/coming" render={() => <Coming />} />
          {/* <Route exact path="/home/movie/coming" render={routeInfo => {
            console.log('路由信息', routeInfo)
            return <Coming {...routeInfo} />
          }} /> */}
          <Redirect exact from="/home/movie" to="/home/movie/hot" />
        </Switch>
      </div>
    </div>
  )
}

export default Movie