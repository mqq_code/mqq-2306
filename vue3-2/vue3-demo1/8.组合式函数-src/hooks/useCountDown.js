import { onBeforeUnmount, onMounted, ref } from "vue"

export const useCountDown = (end = '2025-1-1') => {

  const endTime = new Date(end).getTime()
  const str = ref('')
  let timer = null

  const format = () => {
    const t = endTime - Date.now()
    const d = Math.floor(t / 1000 / 60 / 60 / 24)
    const h = Math.floor(t / 1000 / 60 / 60 % 24)
    const m = Math.floor(t / 1000 / 60 % 60)
    const s = Math.floor(t / 1000 % 60)

    str.value = `${d}天 ${h}:${m}:${s}`
  }

  format()

  onMounted(() => {
    timer = setInterval(() => {
      format()
    }, 1000)
  })

  onBeforeUnmount(() => {
    clearInterval(timer)
  })

  return str
}