import { onBeforeUnmount, onMounted, ref } from 'vue'

export const usePos = () => {
  const pos = ref({ x: 0, y: 0 })

  const setPos = e => {
    pos.value = {
      x: e.clientX,
      y: e.clientY
    }
  }

  onMounted(() => {
    document.addEventListener('mousemove', setPos)
  })
  onBeforeUnmount(() => {
    document.removeEventListener('mousemove', setPos)
  })


  return pos
}