import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Child1 from '../views/Child1.vue'
import Child2 from '../views/Child2.vue'
import Child3 from '../views/Child3.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      redirect: '/child1',
      children: [
        {
          path: '/child1',
          name: 'child1',
          component: Child1
        },
        {
          path: '/child2',
          name: 'child2',
          component: Child2
        },
        {
          path: '/child3',
          name: 'child3',
          component: Child3
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
