import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
 
  const username = ref('默认名字')
  const age = ref(22)

  const changeName = name => {
    username.value = name
  }


  return {
    username,
    age,
    changeName
  }
})
