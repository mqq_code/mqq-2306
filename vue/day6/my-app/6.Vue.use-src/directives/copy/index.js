
export default {
  bind (el, binding) {
    // el: 绑定的元素
    // binding: 指令的信息
    console.log('bind 指令第一次绑定到元素', el.parentNode)
    console.log(binding, binding.value)
    el.$copyValue = binding.value
    el.$copyEventListener = () => {
      // 创建 textarea
      const textarea = document.createElement('textarea')
      // 把要复制的文字存到 textarea
      textarea.value = el.$copyValue
      // 把 textarea 添加到页面
      document.body.appendChild(textarea)
      // 选中 textarea 中的文字
      textarea.select()
      // js 调用复制功能
      document.execCommand('copy')
      // 删除 textarea
      document.body.removeChild(textarea)
      alert(`复制成功: ${el.$copyValue}`)
    }
    el.addEventListener('click', el.$copyEventListener)
  // let n = 0
  // el.$timer = setInterval(() => {
    //   console.log(++n)
    // }, 1000)
  },
  inserted (el, binding) {
    // 可以获取父元素
    // console.log('inserted 元素添加到父节点', el.parentNode)
    // console.log(binding.name, binding.value)
  },
  update (el, binding) {
    // 组件更新时执行
    console.log('update', binding.value)
    // 把最新的数据存到 el 中
    el.$copyValue = binding.value
  },
  unbind (el, binding) {
    // 指令与元素解绑时执行, 清除异步操作
    console.log('unbind 指令与元素解绑时执行')
    el.removeEventListener('click', el.$copyEventListener)
    // clearInterval(el.$timer)
  }
}
