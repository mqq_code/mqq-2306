import copy from './copy'
import copy1 from './copy1'
import copy2 from './copy2'

export default {
  install (Vue) {
    // 注册自定义指令
    Vue.directive('copy', copy)
    Vue.directive('copy1', copy1)
    Vue.directive('copy1', copy2)
  }
}
