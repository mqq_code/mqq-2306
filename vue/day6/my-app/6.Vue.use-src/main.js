import Vue from 'vue'
import App from './App.vue'
import 'animate.css'
import directives from './directives'

Vue.use(directives)

new Vue({
  render: h => h(App)
}).$mount('#app')
