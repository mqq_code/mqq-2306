import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Movie from '../pages/Movie.vue'
import Hot from '../pages/Hot.vue'
import Coming from '../pages/Coming.vue'
import Cinema from '../pages/Cinema.vue'
import Mine from '../pages/Mine.vue'
import Detail from '../pages/Detail.vue'
import City from '../pages/City.vue'
import Login from '../pages/Login.vue'
import Order from '../pages/Order.vue'
import History from '../pages/History.vue'

// 给 vue 安装路由插件
Vue.use(VueRouter)

const router = new VueRouter({
  // 配置路由地址和组件的对应关系
  routes: [
    {
      path: '/',
      // 重定向
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          redirect: '/home/movie/hot',
          children: [
            {
              path: '/home/movie/hot',
              name: 'hot',
              component: Hot
            },
            {
              path: '/home/movie/coming',
              name: 'coming',
              component: Coming
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      // 动态路由
      path: '/detail/:id',
      name: 'detail',
      component: Detail,
      meta: {
        title: '详情'
      },
    },
    {
      path: '/city',
      name: 'city',
      component: City,
      meta: {
        title: '城市'
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        title: '登录'
      },
    },
    {
      path: '/order',
      name: 'order',
      component: Order,
      meta: { // 路由元信息
        title: '订单',
        isAuth: true
      }
      // 路由独享守卫
      // beforeEnter: (to, from, next) => {
      //   console.log('只有 /order 会执行此守卫')
      //   next()
      // }
    },
    {
      path: '/history',
      name: 'history',
      meta: {
        title: '历史记录',
        isAuth: true
      },
      component: History
    },
    {
      path: '/history/:id',
      name: 'history',
      component: History
    }
  ]
})

// 全局前置守卫
router.beforeEach((to, from, next) => {
  // to: 目标地址
  // from: 来源地址
  // next: 是否跳转
  // console.log('from', from)
  console.log('to', to)
  document.title = to.meta.title
  if (to.meta.isAuth) {
    const token = localStorage.getItem('token')
    if (!token) {
      // 如果没有 token 就跳转到登录页面
      next('/login')
      return 
    }
  }
  next()
})

// 抛出路由实例对象
export default router
