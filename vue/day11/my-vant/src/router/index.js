import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Shop from '../views/home/shop/Shop.vue'
import Cart from '../views/home/cart/Cart.vue'
import Address from '../views/address/Address.vue'
import Create from '../views/create/Create.vue'
import Notfound from '../views/404.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/shop',
    children: [
      {
        path: '/shop',
        name: 'shop',
        component: Shop
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      }
    ]
  },
  {
    path: '/address',
    name: 'address',
    component: Address
  },
  {
    path: '/create',
    name: 'create',
    component: Create
  },
  {
    path: '/404',
    name: 'notfound',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
