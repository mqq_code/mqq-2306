import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    chosenAddressId: '',
    addressList: [
      // {
      //   id: '1',
      //   name: '张三',
      //   tel: '13000000000',
      //   address: '浙江省杭州市西湖区文三路 138 号东方通信大厦 7 楼 501 室',
      // }
    ],
    cartList: [], // 购物车商品列表
    shopList: [] // 所有商品列表
  },
  mutations: {
    pushAddressList(state, payload) {
      state.addressList.push({
        id: Date.now(),
        ...payload,
        address: `${payload.province} ${payload.city} ${payload.county} ${payload.addressDetail}`
      })
    },
    selectAddress(state, id) {
      state.chosenAddressId = id
    },
    setShopList(state, payload) {
      state.shopList = payload
    },
    addCart(state, payload) {
      if (payload.count === 0) {
        state.cartList.push(payload)
      }
      payload.count += 1
    },
    changeCount(state, { id, num }) {
      const index = state.cartList.findIndex(v => v.id === id)
      state.cartList[index].count += num
      if (state.cartList[index].count === 0) {
        state.cartList.splice(index, 1)
      }
    }
  },
  getters: {
    curAddress(state) {
      return state.addressList.find(v => v.id === state.chosenAddressId)?.address || '请选择地址'
    }
  }
})
