const { defineConfig } = require('@vue/cli-service')
const Mock = require('mockjs')

const data = Mock.mock({
  "list|20": [{
    "id": "@id",
    "name": "@cname",
    "price|5-40": 5,
    "img": "@image(100x100, @color)",
    "count": 0
  }]
})

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, { app }) => {
      // 本地模拟接口

      app.get('/api/list',(req, res) => {
        res.send(data.list)
      })

      return middlewares
    }
  }
})
