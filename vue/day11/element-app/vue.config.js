const { defineConfig } = require('@vue/cli-service')
const express = require('express')
const fs = require('fs')
const path = require('path')

const readData = () => {
  return JSON.parse(fs.readFileSync(path.join(__dirname, './data/index.json')))
}
const writeData = (data) => {
  fs.writeFileSync(path.join(__dirname, './data/index.json'), JSON.stringify(data))
}

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, { app }) => {
      // 本地模拟接口
      app.use(express.json())
      app.post('/api/login', (req, res) => {
        const { user, pwd } = req.body
        if (user === 'aa' && pwd === '123') {
          res.send({
            code: 0,
            msg: '成功',
            token: '123456789'
          })
        } else {
          res.send({
            code: -1,
            msg: '用户名或密码错误'
          })
        }
      })

      app.get('/api/list', (req, res) => {
        let data = readData()
        const { page, pagesize, ...params } = req.query

        if (Object.keys(params).length > 0) {
          data = data.filter(item => {
            return Object.keys(params).every(k => {
              if (typeof item[k] === 'string') {
                return item[k].includes(params[k])
              } else {
                return item[k] == params[k]
              }
            })
          })
        }

        res.send({
          code: 0,
          msg: '成功',
          values: {
            list: data.slice((page - 1) * pagesize, page * pagesize),
            total: data.length,
            page,
            pagesize
          }
        })
      })

      app.post('/api/del', (req, res) => {
        const { id } = req.body
        const data = readData()
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          data.splice(index, 1)
          writeData(data)

          res.send({
            code: 0,
            msg: '删除成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '数据不存在'
          })
        }
      })

      let n = 1000

      app.post('/api/create', (req, res) => {
        const { name, age, sex, address, num } = req.body
        const data = readData()
        data.unshift({
          name,
          age,
          sex,
          address,
          num,
          id: Date.now() + '',
          index: n++
        })
        writeData(data)
        res.send({
          code: 0,
          msg: '添加成功'
        })
      })

      app.post('/api/update', (req, res) => {
        const { id } = req.body
        const data = readData()
        const index = data.findIndex(v => v.id === id)
        if (index > -1) {
          data.splice(index, 1, req.body)
          writeData(data)

          res.send({
            code: 0,
            msg: '更新成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '数据不存在'
          })
        }
      })



      return middlewares
    }
  }
})
