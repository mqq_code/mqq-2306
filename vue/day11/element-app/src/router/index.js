import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Dashboard from '../views/home/dashboard/Dashboard.vue'
import Students from '../views/home/students/Students.vue'
import Child3 from '../views/home/Child3.vue'
import Child4 from '../views/home/Child4.vue'

import Login from '../views/login/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/students',
        name: 'students',
        component: Students
      },
      {
        path: '/child3',
        name: 'child3',
        component: Child3
      },
      {
        path: '/child4',
        name: 'child4',
        component: Child4
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
