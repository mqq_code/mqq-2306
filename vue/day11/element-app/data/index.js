const Mock = require('mockjs')
const fs = require('fs')

const data = Mock.mock({
  "list|400": [{
    "id": "@id",
    "index|+1": 1,
    "name" :"@cname",
    "sex|0-2": 0,
    "age|18-30": 18,
    "num|0-100": 0,
    "address": "@city(true)"
  }]
})

console.log(data)
fs.writeFileSync('./index.json', JSON.stringify(data.list))


