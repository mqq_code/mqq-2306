import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  // mode: 路由模式，hash | history
  // hash 模式：地址栏有 # ，通过 hashchange 事件监听 # 后的内容改变，切换不同的组件
  // history 模式：HTML5的API pushState 添加历史记录, 上线后需要后端配置支持，
  // 服务端增加一个覆盖所有情况的候选资源：如果 URL 匹配不到任何静态资源，则应该返回同一个 index.html 页面
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
