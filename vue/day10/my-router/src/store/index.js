import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const fetch = async () => {
  return Math.random().toString().split('')
}

export default new Vuex.Store({
  // 存数据
  state: {
    num: 0,
    title: '测试一下',
    arr: [1,2,3,4,5,6],
    list: []
  },
  // 修改数据的唯一方式，必须是同步函数
  mutations: {
    setNum(state, n) {
      state.num += n
    },
    setList(state, list) {
      state.list = list
    }
  },
  // 处理异步操作
  actions: {
    async getListAPI(context, params) {
      // context: 类似 store 的对象
      // params: 组件调用时传的参数
      const res = await fetch()
      context.commit('setList', res)
    }
  },
  // 类似组件的计算属性 
  getters: {
    total(state) {
      return state.arr.reduce((prev, val) => prev + val)
    }
  }
})
