const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, { app }) => {
      // 本地模拟接口
      app.get('/api/list', (req, res) => {
        const data = require('./data.json')
        res.send(data)
      })

      return middlewares
    }
  }
})
