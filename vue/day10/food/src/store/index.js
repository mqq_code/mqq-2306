import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    // 所有食物
    list: [],
    // 选中的食物
    selected: []
  },
  mutations: {
    setList (state, payload) {
      state.list = payload
    },
    setFoodSelect (state, food) {
      Vue.set(food, 'isSelected', !food.isSelected)
      if (food.isSelected) {
        state.selected.push(food)
      } else {
        const index = state.selected.indexOf(food)
        state.selected.splice(index, 1)
      }
    }
  },
  actions: {
    async getListApi(context) {
      const res = await axios.get('/api/list')
      context.commit('setList', res.data)
    }
  }
})
