import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Movie from '../pages/Movie.vue'
import Cinema from '../pages/Cinema.vue'
import Mine from '../pages/Mine.vue'
import Detail from '../pages/Detail.vue'
import City from '../pages/City.vue'

// 给 vue 安装路由插件
Vue.use(VueRouter)

const router = new VueRouter({
  // 配置路由地址和组件的对应关系
  routes: [
    {
      path: '/',
      // 重定向
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      path: '/detail',
      name: 'detail',
      component: Detail
    },
    {
      path: '/city',
      name: 'city',
      component: City
    }
  ]
})

// 抛出路由实例对象
export default router
