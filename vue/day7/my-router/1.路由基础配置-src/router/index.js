import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Detail from '../pages/Detail.vue'
import City from '../pages/City.vue'

// 给 vue 安装路由插件
Vue.use(VueRouter)

const router = new VueRouter({
  // 配置路由地址和组件的对应关系
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/detail',
      name: 'detail',
      component: Detail
    },
    {
      path: '/city',
      name: 'city',
      component: City
    }
  ]
})

// 抛出路由实例对象
export default router
