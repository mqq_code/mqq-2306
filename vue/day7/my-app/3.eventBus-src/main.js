import Vue from 'vue'
import App from './App.vue'
// eventBus（发布订阅模式）
Vue.prototype.$bus = new Vue()

new Vue({
  render: h => h(App)
}).$mount('#app')
