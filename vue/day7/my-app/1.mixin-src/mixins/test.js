export default {
  data () {
    return {
      title: '我是 test 混入',
      testNum: 0
    }
  },
  methods: {
    add (n) {
      this.testNum += n
    }
  },
  created () {
    console.log('test.js created')
  }
}
