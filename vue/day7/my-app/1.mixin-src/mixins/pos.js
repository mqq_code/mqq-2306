// 混入对象
export default {
  data () {
    return {
      pos: {
        x: 0,
        y: 0
      }
    }
  },
  created () {
    console.log('我是 pos.js')
  },
  mounted () {
    document.addEventListener('mousemove', (e) => {
      this.pos = {
        x: e.clientX,
        y: e.clientY
      }
    })
  }
}
