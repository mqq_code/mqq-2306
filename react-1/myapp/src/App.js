import React from 'react'
import { useRoutes, Navigate } from 'react-router-dom'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import Detail from './pages/detail/Detail'
import Child1 from './pages/home/Child1'
import Child2 from './pages/home/Child2'
import Child3 from './pages/home/Child3'

const App = () => {

  const routes = useRoutes([
    {
      path: '/',
      element: <Home />,
      children: [
        {
          path: '/',
          // 重定向
          element: <Navigate to="/child1" />
        },
        {
          path: '/child1',
          element: <Child1 />
        },
        {
          path: '/child2',
          element: <Child2 />
        },
        {
          path: '/child3',
          element: <Child3 />
        }
      ]
    },
    {
      path: '/detail',
      element: <Detail />
    },
    {
      path: '/login',
      element: <Login />
    },
    {
      path: '*',
      element: <div>404</div>
    }
  ])

  return routes
}

export default App