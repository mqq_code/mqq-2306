import React from 'react'
import { useLocation, useSearchParams } from 'react-router-dom'

const Detail = () => {
  const location = useLocation()
  const [searchParams, setSearchParams] = useSearchParams()

  console.log(location)
  console.log(searchParams.get('a'))
  return (
    <div>
      <h2>Detail</h2>
      <button onClick={() => {
        setSearchParams({ c: 'CCCCC' })
      }}>修改search</button>
    </div>
  )
}

export default Detail