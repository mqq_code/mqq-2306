import React from 'react'
import { Outlet, NavLink, useNavigate } from 'react-router-dom'

const Home = () => {
  const navigate = useNavigate()

  return (
    <div>
      <h1>Home</h1>
      <nav>
        <NavLink to="/child1">导航1</NavLink>
        <NavLink to="/child2">导航2</NavLink>
        <NavLink to="/child3">导航3</NavLink>
      </nav>
      <nav>
        <button onClick={() => navigate('/login')}>跳转登录</button>
        <button onClick={() => {
          navigate({
            pathname: '/detail',
            search: `a=100&b=200`
          })
        }}>跳转详情</button>
      </nav>
      <Outlet />
    </div>
  )
}

export default Home