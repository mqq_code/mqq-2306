import React, { useState } from 'react'
import Child from './components/Child'
import { Provider } from './context/ctx'

const App = () => {
  const [num, setNum] = useState(0)
  const changeNum = n => {
    setNum(num + n)
  }

  return (
    <Provider value={{ num, changeNum }}>
      <div>
        <h2>组件</h2>
        {num} <button onClick={() => changeNum(1)}>+</button>
        <Child
          num={num}
          changeNum={changeNum}
          left={<div>left</div>}
        >
          <ul>
            <li>1</li>
            <li>2</li>
            <li>3</li>
            <li>4</li>
            <li>5</li>
          </ul>
        </Child>
      </div>
    </Provider>
  )
}

export default App