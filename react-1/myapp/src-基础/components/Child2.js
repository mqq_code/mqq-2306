import React, { useContext } from 'react'
import ctx from '../context/ctx'

const Child2 = () => {
  const value = useContext(ctx)
  console.log(value)
  return (
    <div className='box'>
      <h2>Child2</h2>
      {value.num}
      <button onClick={() => value.changeNum(6)}>+6</button>
    </div>
  )
}

export default Child2