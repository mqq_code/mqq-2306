import React from 'react'
import Child2 from './Child2'

const Child = (props) => {
  return (
    <div className='box'>
      <h2>Child</h2>
      <div>接收父组件参数: {props.num} <button onClick={() => props.changeNum(3)}>+3</button></div>
      {props.left}
      {props.children}
      <Child2 />
    </div>
  )
}

export default Child