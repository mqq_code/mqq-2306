import React, { useState, useMemo, useEffect, useRef } from 'react'

const App = () => {
  const [list, setList] = useState(JSON.parse(localStorage.getItem('todo-list')) || [])
  const [val, setVal] = useState('')
  const submit = () => {
    setList([...list, {
      text: val,
      done: false,
      id: Date.now()
    }])
    setVal('')
  }
  const keyDown = (e) => {
    if (e.keyCode === 13) {
      submit()
    }
  }
  const changeDone = (id, index) => {
    console.log('完成', id, index)
    const newList = [...list]
    newList[index].done = !newList[index].done
    setList(newList)
  }
  const del = (id, index) => {
    const newList = [...list]
    newList.splice(index, 1)
    setList(newList)
  }
  const desc = useMemo(() => {
    const doneLen = list.filter(v => v.done).length
    console.log('useMemo')
    return `已完成的数量是${doneLen}`
  }, [list])
  useEffect(() => {
    localStorage.setItem('todo-list', JSON.stringify(list))
  }, [list])
  return (
    <div>
      <input type="text" value={val} onChange={e => setVal(e.target.value)} onKeyDown={keyDown} />
      <button onClick={submit}>提交</button>
      <ul>
        {list.map((val, index) =>
          <li key={val.id}>
            {val.done ? <s>{val.text}</s> : <span>{val.text}</span>}
            <button onClick={() => changeDone(val.id, index)}>完成</button>
            <button onClick={() => del(val.id, index)}>删除</button>
          </li>
        )}
      </ul>
      <hr />
      {desc}
    </div>
  )
}

export default App