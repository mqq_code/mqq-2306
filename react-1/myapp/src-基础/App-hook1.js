import React, { useState, useEffect, useRef } from 'react'

const App = () => {
  const [num, setNum] = useState(0)
  const [count, setCount] = useState(0) // 数据改变页面自动更新
  const timer = useRef(null) // 存数据，数据改变页面不会更新

  const add = () => {
    setNum(num + 1)
  }
  const sub = () => {
    setNum(n => {
      console.log('最新值', n)
      return n - 1
    })
    setNum(n => {
      console.log(n)
      return n - 1
    })
    setNum(n => {
      console.log(n)
      return n - 1
    })
    setNum(n => {
      console.log(n)
      return n - 1
    })
  }

  useEffect(() => {
    console.log('num改变了', num)
  }, [num])

  useEffect(() => {
    timer.current = setInterval(() => {
      setCount(c => c - 1)
    }, 1000)
    return () => {
      // 组件销毁执行此函数
      clearInterval(timer.current)
    }
  }, [])

  const stop = () => {
    console.log('停止')
    clearInterval(timer.current)
  }
  const start = () => {
    timer.current = setInterval(() => {
      setCount(c => c - 1)
    }, 1000)
  }

  return (
    <div>
      <button onClick={sub}>-</button>
      {num}
      <button onClick={add}>+</button>
      <hr />
      count: {count}
      <button onClick={stop}>停止定时器</button>
      <button onClick={start}>开始定时器</button>
    </div>
  )
}

export default App