import React, { useState } from 'react'

const App = () => {
  const [num, setNum] = useState(0)

  const start = () => {
    setNum(10)
    const timer = setInterval(() => {
      setNum(n => {
        if (n - 1 <= 0) {
          clearInterval(timer)
        }
        console.log(n - 1)
        return n - 1
      })
    }, 1000)
  }

  return (
    <div>
      <button disabled={num > 0} onClick={start}>{num > 0 ? num : '按钮'}</button>
    </div>
  )
}

export default App