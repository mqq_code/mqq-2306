"use strict";
{
    // 枚举类型: 使用一个单词描述一个数字或者字符串
    let Sex;
    (function (Sex) {
        Sex[Sex["men"] = 100] = "men";
        Sex[Sex["women"] = 200] = "women";
    })(Sex || (Sex = {}));
    console.log(Sex.men);
    let Direction;
    (function (Direction) {
        Direction[Direction["Up"] = 87] = "Up";
        Direction[Direction["Down"] = 83] = "Down";
        Direction[Direction["Left"] = 65] = "Left";
        Direction[Direction["Right"] = 68] = "Right";
    })(Direction || (Direction = {}));
    const box = document.querySelector('.box');
    document.addEventListener('keydown', (e) => {
        if (e.keyCode === Direction.Left) {
            box.style.left = box.offsetLeft - 5 + 'px';
        }
        else if (e.keyCode === Direction.Up) {
            box.style.top = box.offsetTop - 5 + 'px';
        }
        else if (e.keyCode === Direction.Right) {
            box.style.left = box.offsetLeft + 5 + 'px';
        }
        else if (e.keyCode === Direction.Down) {
            box.style.top = box.offsetTop + 5 + 'px';
        }
    });
}
