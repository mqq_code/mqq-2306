"use strict";
{
    // 对象类型
    const obj = {
        name: '小明',
        age: 22
    };
    const obj1 = {
        name: '小刚',
        age: 20,
        info: {
            a: true,
            b: {
                c: {
                    d: [1, 2, 3, 4]
                }
            }
        }
    };
    const obj11 = {
        name: 'xm',
        age: 19
    };
    const obj2 = {
        name: '小红',
        age: 30,
        sex: '男',
        say(text) {
            console.log(text);
        }
    };
    const list = [{ title: '苹果', price: 10, count: 1 }];
    const routes = [
        {
            path: '/home',
            component: '组件1',
            children: [
                {
                    path: '/home/movie',
                    component: '组件1-1'
                },
                {
                    path: '/home/cinema',
                    component: '组件1-2'
                }
            ]
        },
        {
            path: '/detail',
            component: '组件2'
        },
        {
            path: '/login',
            component: '组件3'
        }
    ];
}
