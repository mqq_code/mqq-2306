"use strict";
{
    function fn(a) {
        // 联合类型只能访问所有类型中共有的属性和方法
        return a.length;
    }
    const xm = {
        name: '小明',
        sex: '女'
    };
    function tip(type) {
        alert(type);
    }
    tip('success');
    const xh = {
        name: '小红',
        age: 22
    };
    const p1 = { name: 'p1', age: 20 };
    const p2 = { name: 'p2', age: 22 };
    const d1 = {
        name: '李医生',
        age: 33,
        job: '外科医生'
    };
}
