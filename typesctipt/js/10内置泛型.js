"use strict";
{
    // keyof 和 typeof
    const obj = { a: 100, b: 'bbb', c: true };
    const a = 'a';
    const fn = (a, b) => {
        return true;
    };
}
