"use strict";
{
    const xm = {
        name: 'xiaoming',
        age: 22,
        say(text) {
            return text.length;
        },
        aaa: 'AAAA',
        bbb: 'BBBBB'
    };
    const fn = (a) => {
    };
    const arr = ['a', 'b', 'c'];
    const aa = {
        a: 100,
        b: 'bbbb',
        d: true,
        c: [1, 3, 4, 5]
    };
    // type 不可以重名
    // type Base1 = { a: string }
    // type Base1 = { b: number }
}
