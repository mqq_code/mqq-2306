"use strict";
{
    // 类型断言、可选链、非空断言
    // 类型断言，当开发者比ts更确定变量的时候可以使用类型断言
    const app = document.querySelector('.app');
    const inp = document.querySelector('.inp');
    // 非空断言
    const h1 = document.querySelector('h1');
    const btn = document.querySelector('.btn');
    app.style.cssText = 'background: red';
    h1.innerHTML = '我是标题';
    inp.value = '默认值';
    // 可选链，类似 btn && btn.addEventListener
    btn === null || btn === void 0 ? void 0 : btn.addEventListener('click', () => {
        console.log(inp.value);
    });
    app.addEventListener('mousemove', (e) => {
        console.log(e.clientX);
    });
    inp.addEventListener('input', (e) => {
        console.log(e.target.value);
    });
}
