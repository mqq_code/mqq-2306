"use strict";
{
    // 泛型: 类型的行参，调用时才能确定具体的类型
    function useState(init) {
        return init;
    }
    const arr = useState([]);
    function add(a, b) {
        return a + b;
    }
    const a = add('a', 'b');
    const arr2 = [[[1, [2, 3]], 4, 5], [6, [7, [8, 9]], 10], 11];
    const xm = {
        name: '王小明',
        age: 22,
        hobby: '打篮球',
        sayHobby() {
            return this.hobby;
        }
    };
    const xm1 = {
        name: '王小明',
        age: 22,
        hobby: 100,
        sayHobby() {
            return this.hobby;
        }
    };
    // 泛型约束
    function getLen(a) {
        return a.length;
    }
    console.log(getLen({ a: 100, length: 1 }));
}
