"use strict";
{
    // 定义函数
    function fn1(a, b) {
        return b + a.toFixed(2);
    }
    const fn2 = function (a, b) {
        console.log(a, b);
    };
    const fn3 = (a, b) => {
        return b + a.toFixed(2);
    };
    const obj = {
        name: '小明',
        say(text) {
            return this.name;
        }
    };
    // 可选参数
    function fn5(a, b, c) {
        console.log(a, b);
        b === null || b === void 0 ? void 0 : b.split('');
    }
    fn5(100);
    // 参数默认值
    function fn6(a, b = 0) {
        console.log(a, b);
    }
    fn6(100);
    // 剩余参数
    const add = (a, ...rest) => {
        console.log(a, rest);
    };
    console.log(add(1, 2, 3, 4, 5, 6, 7));
    function fn7(a, b) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(b);
        }
    }
    const a = fn7('abcdefg');
    const b = fn7(100, 2);
    console.log(a, b);
}
