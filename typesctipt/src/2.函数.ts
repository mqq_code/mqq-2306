{

  // 定义函数
  function fn1(a: number, b: string): string {
    return b + a.toFixed(2)
  }

  const fn2 = function (a: number, b: string): void {
    console.log(a, b)
  }

  const fn3 = (a: number, b: string): string => {
    return b + a.toFixed(2)
  }

  const obj = {
    name: '小明',
    say(text: string): string {
      return this.name
    }
  }

  // 可选参数
  function fn5(a: number, b?: string, c?: boolean): void {
    console.log(a, b)
    b?.split('')
  }

  fn5(100)

  // 参数默认值
  function fn6(a: number, b: number = 0) {
    console.log(a, b)
  }

  fn6(100)

  // 剩余参数
  const add = (a: number, ...rest: number[]) => {
    console.log(a, rest)
  }
  console.log(add(1,2,3,4,5,6,7))



  // 函数重载
  function fn7(a: string): number
  function fn7(a: number, b: number): string
  function fn7(a: any, b?: any): any  {
    if (typeof a === 'string') {
      return a.length
    } else {
      return a.toFixed(b)
    }
  }

  const a = fn7('abcdefg')
  const b = fn7(100, 2)
  
  console.log(a, b)




















}