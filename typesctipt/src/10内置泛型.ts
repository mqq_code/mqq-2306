{
  // 内置泛型

  interface Person {
    name: string;
    age: number;
    sex: '男' | '女';
    hobby?: string[];
    address?: string;
    say?: () => string;
  }

  type readonlyPerson = Readonly<Person> // 返回所有属性都只读的类型
  type PartialPerson = Partial<Person> // 返回所有属性都可选的类型
  type RequiredObj = Required<Person> // 返回所有属性都必填的类型
  type PickPerson = Pick<Person, 'name' | 'age' | 'sex'> // 挑选对象中的部分属性组成新类型
  type OmitPerson = Omit<Person, 'name' | 'age' | 'sex'> // 排除对象中的部分属性组成新类型

  type RecordObj = Record<'a' | 'b' | 'c', string> // 返回一个由第一个参数组成的对象，每个的属性类型都是第二个参数
  
  type ExcludeObj = Exclude<1 | 2 | 3 | 4, 2 | 3> // 排除两个参数中重合的类型
  type ExtractObj = Extract<1 | 2 | 3 | 4, 2 | 3> // 保留两个参数重合的类型
  type NonNullableKey = NonNullable<string | number | null> // 去除null和undefined


  // keyof 和 typeof
  const obj = { a: 100, b: 'bbb', c: true }

  // typeof 返回一个变量的类型
  type Iobj = typeof obj

  // keyof 返回由对象的key组成的联合类型
  type keys = keyof Iobj
  const a: keys = 'a'


  const fn = (a: number, b: string): boolean => {
    return true
  }
  // 获取函数返回值类型
  type fnReturn = ReturnType<typeof fn>



}