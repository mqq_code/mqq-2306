{
  // 泛型: 类型的行参，调用时才能确定具体的类型
  function useState<T>(init: T): T {
    return init
  }

  const arr = useState<number[]>([])


  function add<T>(a: T, b: T): T {
    return a + b
  }

  const a = add<string>('a', 'b')


  // const arr1: Array<number> = [1,2,3,4,5]


  type deepArray<T> = T | deepArray<T>[]

  const arr2: deepArray<number>[] = [[[1,[2,3]],4,5],[6,[7,[8,9]],10], 11]


  interface Person<T> {
    name: string;
    age: number;
    hobby: T;
    sayHobby: () => T
  }

  const xm: Person<string> = {
    name: '王小明',
    age: 22,
    hobby: '打篮球',
    sayHobby(): string {
      return this.hobby
    }
  }

  const xm1: Person<number> = {
    name: '王小明',
    age: 22,
    hobby: 100,
    sayHobby(): number {
      return this.hobby
    }
  }



  // 泛型约束
  function getLen<T extends { length: number }>(a: T): number {
    return a.length
  }


  console.log(getLen({ a: 100, length: 1 }))


}