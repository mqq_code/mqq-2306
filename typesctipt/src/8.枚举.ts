{

// 枚举类型: 使用一个单词描述一个数字或者字符串
enum Sex {
  men = 100,
  women = 200
}
console.log(Sex.men)


  enum Direction {
    Up = 87,
    Down = 83,
    Left = 65,
    Right = 68
  }

  const box = document.querySelector('.box') as HTMLDivElement
  document.addEventListener('keydown', (e: KeyboardEvent) => {
    if (e.keyCode === Direction.Left) {
      box.style.left = box.offsetLeft - 5 + 'px'

    } else if (e.keyCode === Direction.Up) {
      box.style.top = box.offsetTop - 5 + 'px'

    } else if (e.keyCode === Direction.Right) {
      box.style.left = box.offsetLeft + 5 + 'px'

    } else if (e.keyCode === Direction.Down) {
      box.style.top = box.offsetTop + 5 + 'px'

    }
  })

}