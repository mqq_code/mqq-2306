{
  // 接口
  interface Person {
    // 只读属性
    readonly name: string;
    age: number;
    hobby?: string[];
    say: (text: string) => number;
    // 其他属性
    [key: string]: any;
  }

  const xm: Person = {
    name: 'xiaoming',
    age: 22,
    say(text: string): number {
      return text.length
    },
    aaa: 'AAAA',
    bbb: 'BBBBB'
  }


  // 接口定义函数
  type f = (a: number) => void
  interface f1 {
    (a: number): void;
  }

  const fn: f1 = (a: number) => {
  }


  // 定义数组类型
  interface IArr {
    [i: number]: string
  }

  const arr: IArr = ['a', 'b', 'c']

  // 继承
  interface Base {
    a: number;
    b: string;
  }
  // interface 重名会进行合并
  interface Base {
    d: boolean;
  }


  interface Test extends Base {
    c: number[];
  }

  const aa: Test = {
    a: 100,
    b: 'bbbb',
    d: true,
    c: [1,3,4,5]
  }

  // type 不可以重名
  // type Base1 = { a: string }
  // type Base1 = { b: number }



}