// js类型: number、string、boolean、null、undefined、symbol、array、function、object
// ts类型: number、string、boolean、null、undefined、symbol、any、void、unknown、never

{

// 添加类型注解
let a: number = 100
let b: string = '100'
let c: boolean = true
let n: null = null
let d: undefined = undefined
let s: symbol = Symbol('sssss')

// any: 任意类型，相当于放弃了类型校验
// let an: any = 100
// an = 'str'
// an = true

// void: 没有值，当函数没有返回值时使用
function fn(): void {
  console.log('fn')
}

// never: 永远不会出现的值
let e: number & string

// unknown: 暂时不确定类型，后续使用类型断言确定类型
let u: unknown

setTimeout(() => {
  const num = Math.random()
  if (num > 0.5) {
    u = '我是字符串'
    // 使用类型断言确定类型
    console.log((u as string).length)
  } else {
    u = num
  }
}, 2000)


// 定义数组
const arr1: number[] = [1,2,3,4,5]
const arr2: string[] = ['a', 'b']
const arr3: Array<number> = [2,3,4]

// object: 变量是引用类型
// const o: object = []

// Object: 变量不可以是 null 和 undefined
// const o1: Object = 100





}