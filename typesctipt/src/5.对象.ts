{

  // 对象类型
  const obj: {
    name: string;
    age: number
  } = {
    name: '小明',
    age: 22
  }

  // 使用类型别名
  type IObj = {
    name: string;
    age: number;
    // 可选属性
    info?: {
      a: boolean;
      b: {
        c: {
          d: number[];
        }
      }
    }
  }

  const obj1: IObj = {
    name: '小刚',
    age: 20,
    info: {
      a: true,
      b: {
        c: {
          d: [1,2,3,4]
        }
      }
    }
  }

  const obj11: IObj = {
    name: 'xm',
    age: 19
  }


  // 定义接口
  interface IPerson {
    name: string;
    age: number;
    sex: string;
    say: (text: string) => void;
  }
  const obj2: IPerson = {
    name: '小红',
    age: 30,
    sex: '男',
    say(text: string) {
      console.log(text)
    }
  }


  interface IGoods {
    title: string;
    price: number;
    count: number;
  }
  const list: IGoods[] = [{ title: '苹果', price: 10, count: 1 }]


  interface IRoute {
    path: string;
    component: string;
    children?: IRoute[];
  }

  const routes: IRoute[] = [
    {
      path: '/home',
      component: '组件1',
      children: [
        {
          path: '/home/movie',
          component: '组件1-1'
        },
        {
          path: '/home/cinema',
          component: '组件1-2'
        }
      ]
    },
    {
      path: '/detail',
      component: '组件2'
    },
    {
      path: '/login',
      component: '组件3'
    }
  ]




}