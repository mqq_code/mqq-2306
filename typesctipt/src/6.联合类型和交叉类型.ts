{

  // 联合类型
  type Ia = string | string[]

  function fn(a: string | string[]) {
    // 联合类型只能访问所有类型中共有的属性和方法
    return a.length
  }


  // 文字类型
  type Iobj = {
    name: string;
    sex: '男' | '女'
  }

  const xm: Iobj = {
    name: '小明',
    sex: '女'
  }

  function tip(type: 'success' | 'error' | 'default') {
    alert(type)
  }

  tip('success')



  // 类型别名，对已有的类型起一个别名
  type a = string | number
  type Iobj1 = { name: string; age: number; }
  const xh: Iobj1 = {
    name: '小红',
    age: 22
  }



  // 交叉类型
  type Base = {
    name: string;
    age: number;
  }

  const p1: Base = { name: 'p1', age: 20 }
  const p2: Base = { name: 'p2', age: 22 }


  type Doctor = Base & { job: string; }

  const d1: Doctor = {
    name: '李医生',
    age: 33,
    job: '外科医生'
  }




}