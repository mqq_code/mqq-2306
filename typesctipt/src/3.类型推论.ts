{

  // 类型推论, 根据值反推变量类型
  let a = 100

  const fn = (a: number, b: number): number => {
    return a + b
  }

  let b = fn(1, 2)




}