// const mockjs = require('mockjs')
// const md5 = require('md5')
const express = require('express')
const ip = require('ip')

// 使用 express 创建服务器应用
const app = express()

// 处理静态资源
app.use(express.static('./public'))
// 让 post 可以接受 json 类型的参数
app.use(express.json())
// 让 post 可以接受 urlencoded 类型的参数
app.use(express.urlencoded())

// 定义 get 请求接口
app.get('/api/list', (req, res) => {
  // 获取前端传过来的参数
  console.log(req.query)

  // 给浏览器返回的内容
  res.send({
    code: 0,
    msg: '成功',
    data: [1,2,3,4,5,6]
  })
})

// 定义post请求接口
app.post('/api/update', (req, res) => {
  // 获取前端传的请求头
  console.log(req.headers)
  // 获取post请求的参数
  console.log(req.body)

  res.send({
    code: 0,
    msg: '成功'
  })
})





const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://${ip.address()}:${port}`)
})

