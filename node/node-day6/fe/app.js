const express = require('express')
const ip = require('ip')
const axios = require('axios')
const { createProxyMiddleware } = require('http-proxy-middleware');


const app = express()
app.use(express.static('./public'))

// 添加请求代理
app.use('/', createProxyMiddleware({ target: 'http://192.168.28.11:9000', changeOrigin: true }));


// app.get('/list', (req, res) => {
//   // 调用另一个服务器应用的接口
//   axios.post('http://192.168.28.11:9000/list/abc').then(response => {
//     console.log(response.data)
//     res.send(response.data)
//   })
// })


app.listen(8000, () => {
  console.log(`running http://${ip.address()}:8000`)
  console.log(`running http://localhost:8000`)
})