const express = require('express')
const ip = require('ip')


const app = express()
app.use(express.json())

// jsonp 后端处理
app.get('/jsonp/list1', (req, res) => {
  const { successCallback } = req.query
  res.setHeader('Content-Type', 'application/javascript;charset=utf-8')
  // 返回js代码给前端，调用前端定义的函数
  res.send(`${successCallback}([1,2,3,4])`)
})

app.get('/jsonp/list2', (req, res) => {
  const { successCallback } = req.query
  res.setHeader('Content-Type', 'application/javascript;charset=utf-8')
  const obj = {
    code: 200,
    msg: '成功',
    data: [{ name: '小明' }]
  }
  res.send(`${successCallback}(${JSON.stringify(obj)})`)
})


//  设置允许跨域访问的响应头
// app.use((req, res, next) => {
//   // 设置允许跨域访问的来源
//   res.setHeader('Access-Control-Allow-Origin', '*')
//   // 设置允许跨域访问的请求头
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   // 设置允许跨域访问的请求方式
//   res.header('Access-Control-Allow-Methods', '*');
//   next()
// })


app.get('/list', (req, res) => {
  res.send([1,2,3,4,5,6,7])
})

app.post('/list/abc', (req, res) => {

  res.send({
    code: 0,
    msg: '成功',
    data: {
      name: '参数内容',
      body: req.body
    }
  })
})


app.listen(9000, () => {
  console.log(`running http://${ip.address()}:9000`)
  console.log(`running http://localhost:9000`)
})