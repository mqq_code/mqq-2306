// commonJS 规范:
// require 引入模块 
// module.exports = 抛出模块

// 模块:
// 1. node 内置模块: path、fs、url、http
// 2. 自定义模块: 自己定义的 js 文件，require(文件路径) 引入
// 3. 第三方包: 通过 npm 下载的包

// require 引入包的流程
// 1. 先查找引入的是不是内置模块
// 2. 再查找引入的是不是包
//  包的查找流程
//  2.1 先查找当前目录的 node_modules
//  2.2 往父级目录的 node_modules 查找，如果没有就一直上一级查找，直到根目录
//  3.3 根目录也没有会查找全局包目录， NODE_PATH 目录
// 3. 再查找具体路径

const fs = require('fs')
const obj = require('./utils/a.js')
const ip = require('ip')
const md5 = require('md5')
const test123 = require('test123')

console.log(test123)
// console.log(obj)
// console.log(ip.address())
// console.log(md5('12345'))