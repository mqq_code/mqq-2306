const fs = require('fs')
const path = require('path')



function rm(src) {
  src = path.resolve(__dirname, src)
  // 读取子目录
  const children = fs.readdirSync(src)
  // 遍历子目录依次删除
  children.forEach(item => {
    const a = path.join(src, item)
    const info = fs.statSync(a)
    // 判断是不是文件
    if (info.isFile()) {
      fs.unlinkSync(a)
    } else {
      rm(a)
    }
  })
  // 清空目录之后删除文件夹
  fs.rmdirSync(src)
}

rm('./commonJS规范123')


