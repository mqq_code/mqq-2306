const path = require('path')


// console.log(__dirname) // 当前文件所在目录的 绝对路径
// console.log(__filename) // 当前文件的 绝对路径
// console.log(path.dirname('/a/b/c/d/e/f.js')) // 返回文件目录
// console.log(path.basename('/a/b/c/d/e/f.js')) // index.js 返回文件名称
// console.log(path.extname('/a/b/c/d/e/f.js'))  // 返回后缀名

 // 拼接路径
// console.log(path.join('a/b/c/d/e/f/g', '../../a', 'b', '/c.js'))

// 返回绝对路径，从后往前合并，如果找到绝对路径就直接返回，没找到就往前拼接当前目录的绝对路径
// console.log(path.resolve('/aaa', '/a', './b', 'c.js')) // 返回一个绝对路径




