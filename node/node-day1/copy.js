const fs = require('fs')
const path = require('path')


function copy(src, target) {
  src = path.resolve(__dirname, src)
  target = path.resolve(__dirname, target)
  // 获取要拷贝的文件信息
  const info = fs.statSync(src)
  // 判断是文件还是文件夹
  if (info.isFile()) {
    fs.copyFileSync(src, target)
  } else {
    // 判断目标文件夹是否存在
    if (!fs.existsSync(target)) {
      fs.mkdirSync(target)
    }
    // 读取子目录
    const children = fs.readdirSync(src)
    // 遍历子目录，依次复制到目标目录
    children.forEach(item => {
      const a = path.join(src, item)
      const b = path.join(target, item)
      copy(a, b)
    })
  }
}

copy('./commonJS规范', './commonJS规范123')

