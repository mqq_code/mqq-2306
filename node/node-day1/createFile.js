const fs = require('fs')
const path = require('path')


function createFile({ path: filePath, isFile, content, children = [] }) {
  filePath = path.resolve(__dirname, filePath)
  // 判断是不是文件
  if (isFile) {
    fs.writeFileSync(filePath, content)
  } else {
    // 创建文件夹
    fs.mkdirSync(filePath)
    // 遍历子目录，依次创建子目录
    children.forEach(item => {
      createFile({
        ...item,
        path: path.resolve(filePath, item.path)
      })
    })
  }
}

const obj = {
  path: '测试',
  isFile: false,
  children: [
    { path: '测试1.js', isFile: true, content: 'const a = 100' },
    { path: 'b.js', isFile: true, content: 'const b = "bbbbbbbbbb"' },
    {
      path: '测试2',
      isFile: false,
      children: [
        { path: '测试3.txt', isFile: true, content: 'alsdjflajsdlfkasdkllfalkds' },
        {
          path: '测试22',
          isFile: false,
          children: [
            { path: '测试33.txt', isFile: true, content: 'alsdjflajsdlfkasdkllfalkds' },
            {
              path: '测试222',
              isFile: false,
              children: [
                { path: '测试333.txt', isFile: true, content: 'alsdjflajsdlfkasdkllfalkds' },
              ]
            }
          ]
        }
      ]
    }
  ]
}
createFile(obj)


