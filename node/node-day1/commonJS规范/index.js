// 引入并且立即执行该 js 文件, 每个 js 文件都是一个单独的作用域，变量不会互相影响
require('./b.js')
const objA = require('./a.js')

const a = 'index 的 a'
console.log('index.js 运行了', a)

console.log(objA)

