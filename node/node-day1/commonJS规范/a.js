const cc = require('./test/cc.js')

console.log('第 3 行', cc)

const a = 'aaaaa'
console.log(a)

for (let i = 0; i < 10; i ++) {
  console.log(i)
}

const add = (a, b) => {
  return a + b
}
// 抛出变量方式一：
// module.exports = {
//   a,
//   arr: ['a', 'b', 'c', 'd'],
//   add
// }

// 方式二：单独抛出
exports.a = a
exports.arr = ['a', 'b', 'c', 'd']
exports.add = add
exports.abc = 'ABC'

