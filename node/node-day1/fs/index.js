const fs = require('fs')
const path = require('path')

// 注意：fs 操作文件时最好写绝对路径，如果使用相对路径是相对于执行命令行时所在终端的路径，并不是相对于当前文件

fs.readFile(path.resolve(__dirname, './data.json'), 'utf-8', (err, data) => {
  if (err) {
    // 读取失败
    console.log(err)
  } else {
    // 读取成功
    console.log(data)
  }
})



// 读取文件内容，文本文件
// 异步读取文件
// fs.readFile('./data.json', 'utf-8', (err, data) => {
//   if (err) {
//     // 读取失败
//     console.log(err)
//   } else {
//     // 读取成功
//     console.log(data)
//   }
// })
// 同步读取文件内容
// try {
//   const a = fs.readFileSync('./data.json', 'utf-8')
//   console.log(111, JSON.parse(a))
// } catch(e) {
//   // 捕获异常，处理兜底逻辑
//   console.log(222)
// }
// console.log('end')


// 修改文件，直接覆盖文件所有内容，如果没有此文件就直接创建文件
// fs.writeFile('./a.txt', 'ccccccccc', err => {
//   if (err) {}
// })
// fs.writeFileSync('./a.txt', 'bbbbbbbbb')
// const data = JSON.parse(fs.readFileSync('./data.json', 'utf-8'))
// data.forEach(item => {
//   if (item.name === '小王') {
//     item.name = '大王'
//     item.hobby.push('吃饭')
//   }
// })
// fs.writeFileSync('./data.json', JSON.stringify(data))

// 追加文件内容
// fs.appendFile('a.txt', '------------', err => {})
// fs.appendFileSync('a.txt', '1234567')


// 删除文件
// fs.unlink('./a.text', err => {})
// fs.unlinkSync('./a.txt')

// 判断文件是否存在
// console.log(fs.existsSync('./a.txt'))

// 拷贝文件
// fs.copyFile('./data.json', './data1.json', err => {})
// fs.copyFileSync('./data.json', './data/data1.json')

// 修改文件名、移动文件位置
// fs.renameSync('./test/data2.json', './data3.json')

// 获取文件信息
// fs.stat('./test', (err, data) => {
//   console.log(data.isFile()) // 判断是不是文件
//   console.log(data.isDirectory()) // 判断是不是文件夹
// })

// 创建文件夹
// fs.mkdir('./test1', err => {})

// 删除文件夹
// fs.rmdir('./test1', err => {})

// 读取文件夹目录
// fs.readdir('./commonJS规范', (err, data) => {
//   console.log(data) // 该文件夹的子目录
// })

// console.log('当前文件夹路径', __dirname)
// console.log('当前文件路径', __filename)