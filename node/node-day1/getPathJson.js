const fs = require('fs')
const path = require('path')


function getPathJson(src) {
  src = path.resolve(__dirname, src)
  const info = fs.statSync(src)

  const obj = {
    path: src,
    isFile: info.isFile()
  }

  if (info.isDirectory()) {
    const children = fs.readdirSync(src)
    obj.children = children.map(item => {
      return getPathJson(path.join(src, item))
    })
  }
  return obj
}

const data = getPathJson('../')
fs.writeFileSync(path.resolve(__dirname, './data.json'), JSON.stringify(data))