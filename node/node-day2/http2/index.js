
const fs = require('fs')
const path = require('path')
const http = require('http')
const url = require('url')

// 浏览器向服务器发送请求的方式
// 1. url 地址栏
// 2. link 标签
// 3. img 标签
// 4. script 标签
// 5. a 标签


// 创建服务器应用
const app = http.createServer((request, response) => {
  // request：请求信息
  // response：响应信息

  // 解析url
  const { pathname } = url.parse(request.url)
  console.log('用户访问的 path', pathname)

  if (request.url === '/') {
    response.end(fs.readFileSync(path.join(__dirname, './public/index.html')))
    return
  }

  const fullPath = path.join(__dirname, './public', pathname)
  console.log(fullPath)

  if (fs.existsSync(fullPath)) {
    response.end(fs.readFileSync(fullPath))
    return
  }

  // 设置状态码
  response.statusCode = 404
  // 设置响应头
  response.setHeader('Content-Type', 'text/html;charset=utf-8;')
  // 设置返回内容
  response.end('<h1>访问的页面不存在</h1>')
})

// 监听端口，端口范围 0-65535
app.listen(10086, () => {
  console.log('应用启动成功，访问 http://127.0.0.1:10086')
  console.log('应用启动成功，访问 http://192.168.28.11:10086')
})

// 用户通过 ip 地址查找要访问的服务器，通过端口号找到要访问的服务器应用
// http://192.168.1.98:8080
// http://baidu.com 域名时为了方便记忆，访问域名时会先查找对应的ip地址，访问对应的服务器


