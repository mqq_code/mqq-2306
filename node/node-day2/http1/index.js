// 服务器: 本质上就是一台电脑，性能比较好，常年不关机
// 服务器应用: 服务器这台电脑上启动的服务
// http 模块：创建服务器应用
const fs = require('fs')
const path = require('path')
const http = require('http')

// 创建服务器应用
const app = http.createServer((request, response) => {
  // request：请求信息
  // response：响应信息
  // 有人访问此应用程序时执行此函数

  console.log('用户访问的 path', request.url)
  if (request.url === '/index.html') {
    // 用户访问服务器应用时返回内容
    response.setHeader('Content-Type', 'text/html;charset=utf-8;')
    response.end(fs.readFileSync(path.resolve(__dirname, './public/index.html'), 'utf-8'))
    return
  } else if (request.url === '/style.css') {
    response.setHeader('Content-Type', 'text/css;charset=utf-8;')
    response.end(fs.readFileSync(path.resolve(__dirname, './public/style.css'), 'utf-8'))
    return
  } else if (request.url === '/index.js') {
    response.setHeader('Content-Type', 'application/javascript;charset=utf-8;')
    response.end(fs.readFileSync(path.resolve(__dirname, './public/index.js'), 'utf-8'))
    return
  } else if (request.url === '/1.png') {
    response.setHeader('Content-Type', 'image/png')
    response.end(fs.readFileSync(path.resolve(__dirname, './public/1.png')))
    return
  }

  // 设置状态码
  response.statusCode = 404
  // 设置响应头
  response.setHeader('Content-Type', 'text/html;charset=utf-8;')
  // 设置返回内容
  response.end('<h1>访问的页面不存在</h1>')
})

// 监听端口，端口范围 0-65535
app.listen(10086, () => {
  // 访问本机地址
  console.log('应用启动成功，访问 http://127.0.0.1:10086')
  console.log('应用启动成功，访问 http://localhost:10086')
  // 通过ip地址访问应用
  console.log('应用启动成功，访问 http://192.168.28.11:10086')
})

// 用户通过 ip 地址查找要访问的服务器，通过端口号找到要访问的服务器应用
// http://192.168.1.98:8080
// http://baidu.com 域名时为了方便记忆，访问域名时会先查找对应的ip地址，访问对应的服务器


