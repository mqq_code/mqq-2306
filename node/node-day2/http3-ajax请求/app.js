
const fs = require('fs')
const path = require('path')
const http = require('http')
const url = require('url')

// url、img、a、link、script 发送的是 get 类型的请求

// 创建服务器应用
const app = http.createServer((request, response) => {
  // 解析url
  const { pathname } = url.parse(request.url)
  if (request.url === '/') {
    response.end(fs.readFileSync(path.join(__dirname, './public/index.html')))
    return
  }
  // 查找静态资源
  const fullPath = path.join(__dirname, './public', pathname)
  if (fs.existsSync(fullPath)) {
    response.end(fs.readFileSync(fullPath))
    return
  }

  // 定义接口
  // 请求方式: GET、POST、PUT、DELETE
  console.log('获取请求方式', request.method)
  if (pathname === '/abc' && request.method === 'POST') {
    response.end('[1,2,3,4,5,6,7]')
    return
  }

  // 设置状态码
  response.statusCode = 404
  // 设置响应头
  response.setHeader('Content-Type', 'text/html;charset=utf-8;')
  // 设置返回内容
  response.end('<h1>访问的页面不存在</h1>')
})

// 监听端口，端口范围 0-65535
app.listen(10086, () => {
  console.log('应用启动成功，访问 http://127.0.0.1:10086')
  console.log('应用启动成功，访问 http://192.168.28.11:10086')
})

