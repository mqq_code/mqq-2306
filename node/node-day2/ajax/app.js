// node 内置模块
const fs = require('fs') // 操作文件
const path = require('path') // 处理路径
const http = require('http') // 创建服务器应用
const url = require('url') // 解析url

const join = p => path.join(__dirname, './dist', p)

// const urlstr = 'http://www.baidu.com/a/b/c/d/e/f/g.html?a=100&b=200'
// console.log(url.parse(urlstr, true))


// 创建服务器应用
const app = http.createServer((req, res) => {
  // req: 请求的对象
  const { pathname, query } = url.parse(req.url, true)

  const fullPath = join(pathname)
  // 判断文件是否存在
  if (fs.existsSync(fullPath)) {
    res.end(fs.readFileSync(fullPath))
    return
  }

  if (pathname === '/api/list' && req.method === 'GET') {
    console.log('接收前端传过来的参数', query)
    const { page, pagesize } = query
    // 读取 json 文件
    const jsonData = fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8')
    // json 转对象
    const data = JSON.parse(jsonData)
    
    // 根据分页截取数据
    const resData = data.slice((page - 1) * pagesize, page * pagesize)

    console.log(resData)
    res.setHeader('Content-Type', 'application/json;charset=utf-8')
    res.end(JSON.stringify(resData))
    return
  }

  
  // 用户访问此服务时，返回的内容
  res.statusCode = 404
  res.setHeader('Content-Type', 'text/html;charset=utf-8')
  res.end('404 页面不存在')
})

// 0-65535
const port = 8000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://192.168.28.11:${port}`)
})

