
let isLogin = true

$('.login-btn').addEventListener('click', () => {
  isLogin = true
  $('.title').innerHTML = '欢迎登录'
  $('.login-btn').classList.add('active')
  $('.register-btn').classList.remove('active')
  $('.submit').innerHTML = '登录'
  $('.confirm-pwd').style.display = 'none'
})

$('.register-btn').addEventListener('click', () => {
  isLogin = false
  $('.title').innerHTML = '欢迎注册'
  $('.register-btn').classList.add('active')
  $('.login-btn').classList.remove('active')
  $('.submit').innerHTML = '注册'
  $('.confirm-pwd').style.display = 'block'
})

$('.submit').addEventListener('click', async () => {
  if (isLogin) {
    const user = $('.user').value.trim()
    const pwd = $('.pwd').value.trim()
    if (!user || !pwd) {
      alert('用户名或密码不能为空')
      return
    }
    const res = await axios.post('/api/login', { user, pwd })
    if (res.data.code === 0) {
      // cookie、localStorage、sessionStorage
      localStorage.setItem('token', res.data.token)
      localStorage.setItem('avatar', res.data.avatar)
      alert('登录成功')
      location.href = '../index.html'
    } else {
      alert(res.data.msg)
    }
  } else {
    register()
  }
})
// 注册
async function register() {
  if ($('.pwd').value.trim() !== $('.confirm').value.trim()) {
    alert('两次密码不一致')
    return
  }
  const res = await axios.post('/api/register', {
    user: $('.user').value.trim(),
    pwd: $('.pwd').value.trim()
  })

  if (res.data.code === 0) {
    alert('注册成功')
    $('.user').value = ''
    $('.pwd').value = ''
    $('.confirm').value = ''
    $('.login-btn').click()
  } else {
    alert(res.data.msg)
  }
}