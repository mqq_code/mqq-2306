function $(el) {
  return document.querySelector(el)
}
function $gets(el) {
  return [...document.querySelectorAll(el)]
}
$('.back').addEventListener('click', () => {
  history.back()
})
$('nav span').addEventListener('click', () => {
  location.href = '/user.html'
})

function auth() {
  const token = localStorage.getItem('token')
  const isLogin = location.pathname === '/login/index.html' || location.pathname === '/login/'
  if (!token && !isLogin) {
    alert('请先登录')
    location.href = '../login/'
  }
}

auth()