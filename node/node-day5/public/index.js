$('.publish').addEventListener('click', () => {
  location.href = './edit/'
})

async function getList() {
  try {
    const res = await axios.get('/api/list', {
      params: {
        token: localStorage.getItem('token')
      }
    })
    if (res.data.data.length > 0) {
      $('.empty').style.display = 'none'
      // 渲染列表
      $('.list').innerHTML = res.data.data.map(item => `
        <div class="item">
          <h4>${item.title}</h4>
          <div class="info">
            <p></p>
            <p>发布人：${item.author}</p>
            <p>发布时间：${new Date(item.createTime).toLocaleString()}</p>
          </div>
          <div class="btns">
            <button>编辑</button>
            <button class="del" data-id="${item.id}">删除</button>
          </div>
          <div class="go-detail" data-id="${item.id}"></div>
        </div>
      `).join('')
    } else {
      $('.list').style.display = 'none'
      $('.empty').style.display = 'block'
    }
  } catch(e) {
    alert(e.response.data)
    if (e.response.status === 401) {
      location.href = '../login/'
    }
  }
}

getList()


$('.list').addEventListener('click', async e => {
  if (e.target.classList.contains('del')) {
    const id = e.target.getAttribute('data-id')
    if (confirm('确定要删除吗')) {
      const res = await axios.post('/api/del', {
        id,
        token: localStorage.getItem('token')
      })
      if (res.data.code === 0) {
        alert('删除成功')
        getList()
      } else {
        alert(res.data.msg)
      }
    }
  } else if (e.target.classList.contains('go-detail')) {
    const id = e.target.getAttribute('data-id')
    location.href = `./detail/?id=${id}`
  }
})