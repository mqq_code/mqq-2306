const express = require('express')
const path = require('path')
const fs = require('fs')
const ip = require('ip')
const md5 = require('md5')
const multer  = require('multer')
// 定义文件上传到哪个文件夹
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads')
  },
  filename: function (req, file, cb) {
    // console.log('前端上传的文件信息', file)
    // console.log('文件后缀名', path.extname(file.originalname))
    cb(null, file.fieldname + '_' + Date.now() + path.extname(file.originalname))
  }
})
const upload = multer({ storage: storage })


const app = express()
app.use(express.static('./public'))
app.use(express.static('./uploads'))
app.use(express.json())

const readFile = (filename) => JSON.parse(fs.readFileSync(path.join(__dirname, './data', filename)))
const writeFile = (filename, file) => fs.writeFileSync(path.join(__dirname, './data', filename), JSON.stringify(file))

// 登录接口
app.post('/api/login', (req, res) => {
  let { user, pwd } = req.body
  user = user.trim()
  pwd = pwd.trim()
  const userlist = readFile('userlist.json')
  const curUser = userlist.find(v => v.user === user && v.pwd === pwd)
  if (curUser) {
    res.send({
      code: 0,
      msg: '登录成功',
      token: curUser.token,
      avatar: curUser.avatar
    })
  } else {
    res.send({
      code: -3,
      msg: '用户名或者密码错误'
    })
  }
})
// 注册接口
app.post('/api/register', (req, res) => {
  let { user, pwd } = req.body
  user = user.trim()
  pwd = pwd.trim()
  if (!user || !pwd) {
    res.send({
      code: -1,
      msg: '用户名或者密码不能为空'
    })
  } else {
    const userlist = readFile('userlist.json')
    const index = userlist.findIndex(v => v.user === user)
    if (index > -1) {
      res.send({
        code: -2,
        msg: '用户名已存在'
      })
    } else {
      userlist.push({ user, pwd, token: md5(user + pwd + Date.now()) })
      writeFile('userlist.json', userlist)
      res.send({
        code: 0,
        msg: '注册成功'
      })
    }
  }
})

// 博客列表
app.get('/api/list', (req, res) => {
  const { token } = req.query
  const userlist = readFile('userlist.json')
  const curUser = userlist.find(v => v.token === token)
  if (!curUser) {
    // 登录信息失效
    res.statusCode = 401
    res.send('登录失效，请重新登录')
    return
  }
  res.send({
    code: 0,
    msg: '成功',
    data: readFile('bloglist.json')
  })
})

// 博客详情
app.get('/api/list/detail', (req, res) => {
  const { token, id } = req.query
  const userlist = readFile('userlist.json')
  const curUser = userlist.find(v => v.token === token)
  if (!curUser) {
    // 登录信息失效
    res.statusCode = 401
    res.send('登录失效，请重新登录')
    return
  }
  const bloglist = readFile('bloglist.json')
  const blog = bloglist.find(v => v.id === id)
  if (blog) {
    res.send({
      code: 0,
      msg: '成功',
      data: blog
    })
  } else {
    res.send({
      code: -6,
      msg: '参数错误，数据不存在'
    })
  }
})

// 发布博客
app.post('/api/publish', (req, res) => {
  const { title, content, token } = req.body
  const userlist = readFile('userlist.json')
  const curUser = userlist.find(v => v.token === token)
  if (!curUser) {
    // 登录信息失效
    res.statusCode = 401
    res.send('登录失效，请重新登录')
    return
  }
  const bloglist = readFile('bloglist.json')
  bloglist.push({
    id: Date.now() + Math.random().toString().slice(2),
    title,
    content,
    author: curUser.user,
    createTime: Date.now(),
    avatar: ''
  })
  writeFile('bloglist.json', bloglist)

  res.send({
    code: 0,
    msg: '成功'
  })
})

// 删除博客
app.post('/api/del', (req, res) => {
  const { id, token } = req.body
  const userlist = readFile('userlist.json')
  const curUser = userlist.find(v => v.token === token)
  if (!curUser) {
    // 登录信息失效
    res.statusCode = 401
    res.send('登录失效，请重新登录')
    return
  }

  const bloglist = readFile('bloglist.json')
  const index = bloglist.findIndex(v => v.id === id)
  if (index > -1) {
    bloglist.splice(index, 1)
    writeFile('bloglist.json', bloglist)
    res.send({
      code: 0,
      msg: '成功'
    })
  } else {
    res.send({
      code: -6,
      msg: '参数错误，文章不存在'
    })
  }
})

// 上传头像
app.post('/api/upload', upload.single('avatar'), (req, res) => {
  console.log(req.file)
  console.log(req.body)

  const userlist = readFile('userlist.json')
  const index = userlist.findIndex(v => v.token === req.body.token)
  if (index === -1) {
    // 登录信息失效
    res.statusCode = 401
    res.send('登录失效，请重新登录')
    return
  }
  const avatar = `http://${ip.address()}:${PORT}/${req.file.filename}`
  const user = {
    ...userlist[index],
    avatar: avatar
  }
  userlist.splice(index, 1, user)
  writeFile('userlist.json', userlist)


  res.send({
    code: 0,
    msg: '成功',
    data: {
      url: avatar
    }
  })
})


const PORT = 9000
app.listen(PORT, () => {
  console.log(`running http://localhost:${PORT}`)
  console.log(`running http://${ip.address()}:${PORT}`)
})
