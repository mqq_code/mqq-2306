

var page = 1
var pagesize = 5


var table = new Table({
  el: '.table1', // 要渲染的元素
  columns: [
    {
      title: '学号',
      key: 'id'
    },
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '年龄',
      key: 'age'
    },
    {
      title: '性别',
      key: 'sex'
    },
    {
      title: '分数',
      key: 'num',
      render: function (row) {
        if (row.num >= 90) {
          return `<b style="color: green">${row.num}</b>`
        } else if (row.num >= 60 && row.num < 90) {
          return `<b style="color: orange">${row.num}</b>`
        } else {
          return `<b style="color: red">${row.num} 不及格</b>`
        }
      }
    },
    {
      title: '操作',
      key: 'action',
      render: function (row) {
        return `
          <button class="edit" data-info='${JSON.stringify(row)}'>修改</button>
          <button class="del" data-id="${row.id}">删除</button>
        `
      }
    }
  ], // 渲染表头的数据
  data: []
})

var page1 = new Pagination({
  el: '.page1',
  total: 0,
  page,
  pagesize,
  onChange: function(curpage, curpageisze) {
    console.log('分页改变了', curpage, curpageisze)
    page = curpage
    pagesize = 5
    getList(page, pagesize)
  }
})


function getList(page, pagesize) {
  // 从后端获取数据
  const xhr = new XMLHttpRequest()
  // get 请求传参数通过 地址？拼接
  xhr.open('get', `http://192.168.28.11:9000/api/list?page=${page}&pagesize=${pagesize}`)
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        // 把数据转成对象
        const res = JSON.parse(xhr.responseText)
        console.log(res)
        // 更新 table 和分页数据
        table.updateData(res.data)
        page1.updateTotal(res.total)
      }
    }
  }
xhr.send()
}

getList(page, pagesize)



let editId = ''
const tableEl = document.querySelector('.table1')
tableEl.addEventListener('click', e => {
  // 删除
  if (e.target.classList.contains('del')) {
    const id = e.target.getAttribute('data-id')
    const xhr = new XMLHttpRequest()
    xhr.open('post', 'http://192.168.28.11:9000/api/list/del')
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          const data = JSON.parse(xhr.responseText)
          console.log(data)
          if (data.code === 0) {
            // 删除成功，重新渲染数据
            getList(page, pagesize)
          } else {
            alert(data.msg)
          }
        }
      }
    }
    // post 参数通过 send 发送请求体
    // post 请求需要设置 Content-Type 请求头，通知后端发送的数据类型

    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8')
    xhr.send(JSON.stringify({ id, a: 100 }))

    // xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8')
    // xhr.send(`id=${id}&a=200&c=300`)

    // const form = new FormData()
    // form.append('id', id)
    // form.append('a', 300)
    // xhr.send(form)
  } else if (e.target.classList.contains('edit')) {
    const rowInfo = JSON.parse(e.target.getAttribute('data-info'))
    console.log('编辑按钮', rowInfo)
    $('.name').value = rowInfo.name
    $('.age').value = rowInfo.age
    $('.sex').value = rowInfo.sex
    editId = rowInfo.id
  }
})

$('.cancel').addEventListener('click', () => {
  $('.name').value = ''
  $('.age').value = ''
  $('.sex').value = ''
  editId = ''
})
$('.ok').addEventListener('click', () => {
  const obj = {
    id: editId,
    name: $('.name').value,
    age: $('.age').value,
    sex: $('.sex').value
  }
  console.log(obj)
  const xhr = new XMLHttpRequest()
  xhr.open('post', 'http://192.168.28.11:9000/api/list/update')
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.responseText)
        console.log(data)
        if (data.code === 0) {
          // 修改成功，重新渲染数据
          getList(page, pagesize)
        } else {
          alert(data.msg)
        }
      }
    }
  }

  const params = {}
  Object.keys(obj).forEach(key => {
    if (String(obj[key]).trim()) {
      params[key] = String(obj[key]).trim()
    }
  })
  console.log(params)
  xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8')
  xhr.send(JSON.stringify(params))
})



function $(el) {
  return document.querySelector(el)
}