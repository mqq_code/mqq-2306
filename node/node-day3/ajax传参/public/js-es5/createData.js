function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
function createData(num) {
  var firstName = ['赵', '常', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '楚', '魏', '诸葛', '西门', '慕容', '欧阳', '轩辕', '宇文', '南宫', '东方', '拓跋', '皇甫']
  var lastName = ['铁柱', '翠花', '小明', '旺财', '子璇', '来福', '威', '铁蛋', '狗剩', '慧娟', '红旗', '二狗']
  var arr = []
  for (var i = 0; i < num; i ++) {
    arr.push({
      id: i + 1,
      name: firstName[random(0, firstName.length - 1)] + lastName[random(0, lastName.length - 1)],
      sex: Math.random() >= 0.5 ? '男' : '女',
      age: random(18, 40),
      num: random(0, 100)
    })
  }
  return arr
}
var data = createData(50)