const path = require('path')
const fs = require('fs')
const url = require('url')
const http = require('http')


const app = http.createServer((request, response) => {
  const urlObj = url.parse(request.url, true) // 解析请求的 url
  if (urlObj.pathname === '/') {
    urlObj.pathname = '/index.html'
  }
  const fullPath = path.join(__dirname, './public', urlObj.pathname) // 拼接绝对路径
  if (fs.existsSync(fullPath)) {
    response.end(fs.readFileSync(fullPath))
    return
  }

  // 获取数据接口
  if (urlObj.pathname === '/api/list' && request.method === 'GET') {
    // 解析前端通过 get 请求传过来的参数
    const { page, pagesize } = urlObj.query
    // 读取 json 文件转成对象
    const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
    // 根据前端参数截取数据
    const arr = data.slice((page - 1) * pagesize, page * pagesize)

    // 返回给前端
    response.setHeader('Content-Type', 'application/json;charset=utf-8')
    response.end(JSON.stringify({
      total: data.length,
      page,
      pagesize,
      data: arr
    }))
    return
  }
  // 删除接口
  if (urlObj.pathname === '/api/list/del' && request.method === 'POST') {
    // 获取 post 参数
    let str = ''
    request.on('data', e => {
      str += e
    })
    request.on('end', () => {
      const params = JSON.parse(str)
      console.log('前端请求参数', params)
      // 获取数据
      const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
      // 根据前端传过来的id查找下标
      const index = data.findIndex(v => v.id === Number(params.id))
      response.setHeader('Content-Type', 'application/json;charset=utf-8')
      if (index > -1) {
        data.splice(index, 1)
        // 把删除后的数据写入json
        fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))
        response.end(JSON.stringify({
          code: 0,
          msg: '删除成功'
        }))
      } else {
        response.end(JSON.stringify({
          code: -1,
          msg: '参数错误，删除的数据不存在'
        }))
      }
    })
  } else if (urlObj.pathname === '/api/list/update' && request.method === 'POST') {
    // 编辑接口
    // 获取 post 参数
    let str = ''
    request.on('data', e => {
      str += e
    })
    request.on('end', () => {
      const params = JSON.parse(str)
      console.log('前端请求参数', params)
      // 获取数据
      const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
      // 根据前端传过来的id查找下标
      const index = data.findIndex(v => v.id === Number(params.id))
      response.setHeader('Content-Type', 'application/json;charset=utf-8')
      if (index > -1) {
        data.splice(index, 1, {
          ...data[index],
          ...params
        })
        // 把删除后的数据写入json
        fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))
        response.end(JSON.stringify({
          code: 0,
          msg: '修改成功'
        }))
      } else {
        response.end(JSON.stringify({
          code: -1,
          msg: '参数错误，编辑的数据不存在'
        }))
      }
    })
  } else {
    response.statusCode = 404
    response.setHeader('Content-Type', 'text/html;charset=utf-8')
    response.end('404，访问的页面不存在')
  }
})

const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://192.168.28.11:${port}`)
})





// json 格式: 一种特定规范的字符串
// 1. json 支持的类型: []、{}、字符串、数字、boolean
// 2. 字符串必须双引号
// 3. js中 json字符串转对象 JSON.parse(变量)
// 3. js中 对象转json字符串 JSON.stringify(变量)
