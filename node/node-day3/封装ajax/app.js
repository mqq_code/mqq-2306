const path = require('path')
const fs = require('fs')
const url = require('url')
const http = require('http')
const actions = require('./actions')



const app = http.createServer((request, response) => {
  
  const urlObj = url.parse(request.url, true) // 解析请求的 url
  console.log(request.headers)
  // 查找用户访问的url是否存在 public 目录中，存在就直接返回文件内容
  if (urlObj.pathname === '/') {
    urlObj.pathname = '/index.html'
  }
  const fullPath = path.join(__dirname, './public', urlObj.pathname) // 拼接绝对路径
  if (fs.existsSync(fullPath)) {
    response.end(fs.readFileSync(fullPath))
    return
  }

  const key = `${request.method} ${urlObj.pathname}`
  if (actions[key]) {
    actions[key](request, response)
  } else {
    response.statusCode = 404
    response.setHeader('Content-Type', 'text/html;charset=utf-8')
    response.end('404，访问的页面不存在')
  }

})

const port = 9000
app.listen(port, () => {
  console.log(`服务启动成功 http://localhost:${port}`)
  console.log(`服务启动成功 http://127.0.0.1:${port}`)
  console.log(`服务启动成功 http://192.168.28.11:${port}`)
})





// json 格式: 一种特定规范的字符串
// 1. json 支持的类型: []、{}、字符串、数字、boolean
// 2. 字符串必须双引号
// 3. js中 json字符串转对象 JSON.parse(变量)
// 3. js中 对象转json字符串 JSON.stringify(变量)






