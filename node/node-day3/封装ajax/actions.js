const path = require('path')
const fs = require('fs')
const url = require('url')
const delFn = require('./del')
const updateFn = require('./update')

const actions = {
  'GET /api/list': (request, response) => {
    const urlObj = url.parse(request.url, true) // 解析请求的 url
    // 解析前端通过 get 请求传过来的参数
    const { page, pagesize } = urlObj.query
    // 读取 json 文件转成对象
    const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
    // 根据前端参数截取数据
    const arr = data.slice((page - 1) * pagesize, page * pagesize)

    // 返回给前端
    response.setHeader('Content-Type', 'application/json;charset=utf-8')
    response.end(JSON.stringify({
      total: data.length,
      page,
      pagesize,
      data: arr
    }))
  },
  'POST /api/list/del': delFn,
  'POST /api/list/update': updateFn
}

module.exports = actions