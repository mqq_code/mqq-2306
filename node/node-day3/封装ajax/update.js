const path = require('path')
const fs = require('fs')
const url = require('url')


const updateFn = (request, response) => {
  // 编辑接口
  // 获取 post 参数
  let str = ''
  request.on('data', e => {
    str += e
  })
  request.on('end', () => {
    const params = JSON.parse(str)
    console.log('前端请求参数', params)
    // 获取数据
    const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data.json'), 'utf-8'))
    // 根据前端传过来的id查找下标
    const index = data.findIndex(v => v.id === Number(params.id))
    response.setHeader('Content-Type', 'application/json;charset=utf-8')
    if (index > -1) {
      data.splice(index, 1, {
        ...data[index],
        ...params,
        id: Number(params.id)
      })
      // 把删除后的数据写入json
      fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))
      response.end(JSON.stringify({
        code: 0,
        msg: '修改成功'
      }))
    } else {
      response.end(JSON.stringify({
        code: -1,
        msg: '参数错误，编辑的数据不存在'
      }))
    }
  })
}

module.exports = updateFn