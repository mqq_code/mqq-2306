

class Pagination extends Base {
  constructor(options) {
    super()
    this.el = this.$(options.el)
    this.total = options.total // 共几条数据
    this.page = options.page || 1 // 当前第几页
    this.pagesize = options.pagesize || 5 // 每页几条
    this.totalpage = Math.ceil(this.total / this.pagesize) // 共几页数据
    this.onChangeCallback = options.onChange
    this.init()
  }
  init() {
    var _this = this
    function selected(value) {
      return _this.pagesize === value ? 'selected' : ''
    }
    this.el.innerHTML = `
      <span class="total">共 ${this.total} 条数据</span>
      <button class="prev">上一页</button>
      <div class="btns"></div>
      <button class="next">下一页</button>
      <select>
        <option ${selected(5)} value="5">每页 5 条</option>
        <option ${selected(10)} value="10">每页 10 条</option>
        <option ${selected(15)} value="15">每页 15 条</option>
        <option ${selected(20)} value="20">每页 20 条</option>
      </select>
    `
    this.renderBtns()
    this.bindEvent()
  }
  renderBtns() {
    var str = ''
    for (var i = 1; i <= this.totalpage; i ++) {
      if (i === this.page) {
        str += `<button data-page="${i}" class="active">${i}</button>`
      } else {
        str += `<button data-page="${i}">${i}</button>`
      }
    }
    this.$('.btns', this.el).innerHTML = str
  }
  bindEvent() {
    var _this = this
    this.$('.prev', this.el).addEventListener('click', function() {
      if (_this.page - 1 < 1) return
      _this.change(_this.page - 1)
    })
    this.$('.next', this.el).addEventListener('click', function() {
      if (_this.page + 1 > _this.totalpage) return
      _this.change(_this.page + 1)
    })
    this.$('.btns', this.el).addEventListener('click', function(e) {
      if (e.target.nodeName === 'BUTTON') {
        var cur = e.target.getAttribute('data-page')
        _this.change(cur * 1)
      }
    })
    this.$('select', this.el).addEventListener('change', function() {
      _this.pagesize = this.value * 1
      _this.totalpage = Math.ceil(_this.total / _this.pagesize) // 共几页数据
      _this.renderBtns()
      _this.change(1)
    })
  }
  change(page) {
    this.page = page
    this.$('.active', this.el).classList.remove('active')
    this.$gets('.btns button', this.el)[this.page - 1].classList.add('active')
    this.onChangeCallback && this.onChangeCallback(this.page, this.pagesize)
  }
  updateTotal(total) {
    this.total = total
    this.totalpage = Math.ceil(this.total / this.pagesize) // 共几页数据
    if (this.totalpage < this.page) {
      this.page = this.totalpage
      this.change(this.page)
    }
    this.$('.total', this.el).innerHTML = `共 ${this.total} 条数据`
    this.renderBtns()
  }
}