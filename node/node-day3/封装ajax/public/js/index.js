

var page = 1
var pagesize = 5


var table = new Table({
  el: '.table1', // 要渲染的元素
  columns: [
    {
      title: '学号',
      key: 'id'
    },
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '年龄',
      key: 'age'
    },
    {
      title: '性别',
      key: 'sex'
    },
    {
      title: '分数',
      key: 'num',
      render: function (row) {
        if (row.num >= 90) {
          return `<b style="color: green">${row.num}</b>`
        } else if (row.num >= 60 && row.num < 90) {
          return `<b style="color: orange">${row.num}</b>`
        } else {
          return `<b style="color: red">${row.num} 不及格</b>`
        }
      }
    },
    {
      title: '操作',
      key: 'action',
      render: function (row) {
        return `
          <button class="edit" data-info='${JSON.stringify(row)}'>修改</button>
          <button class="del" data-id="${row.id}">删除</button>
        `
      }
    }
  ], // 渲染表头的数据
  data: []
})

var page1 = new Pagination({
  el: '.page1',
  total: 0,
  page,
  pagesize,
  onChange: function(curpage, curpageisze) {
    console.log('分页改变了', curpage, curpageisze)
    page = curpage
    pagesize = 5
    getList(page, pagesize)
  }
})


function getList(page, pagesize) {
  ajax({
    // url 如果没有协议端口域名，发送请求时会自动使用当前页面的协议端口域名
    url: `/api/list?page=${page}&pagesize=${pagesize}`,
  })
  .then(res => {
    // 更新 table 和分页数据
    table.updateData(res.data)
    page1.updateTotal(res.total)
  })
}

getList(page, pagesize)

let editId = ''
const tableEl = document.querySelector('.table1')
tableEl.addEventListener('click', e => {
  // 删除
  if (e.target.classList.contains('del')) {
    const id = e.target.getAttribute('data-id')
    ajax({
      method: 'post',
      url: '/api/list/del',
      data: JSON.stringify({ id }),
      headers: {
        'Content-Type': 'application/json;charset=utf-8;'
      }
    })
    .then(res => {
      if (res.code === 0) {
        getList(page, pagesize)
      } else {
        alert(res.msg)
      }
    })
  } else if (e.target.classList.contains('edit')) {
    const rowInfo = JSON.parse(e.target.getAttribute('data-info'))
    console.log('编辑按钮', rowInfo)
    $('.name').value = rowInfo.name
    $('.age').value = rowInfo.age
    $('.sex').value = rowInfo.sex
    editId = rowInfo.id
  }
})

$('.cancel').addEventListener('click', () => {
  $('.name').value = ''
  $('.age').value = ''
  $('.sex').value = ''
  editId = ''
})
$('.ok').addEventListener('click', () => {
  const obj = {
    id: editId,
    name: $('.name').value,
    age: $('.age').value,
    sex: $('.sex').value
  }
  // 筛选掉空值
  const params = {}
  Object.keys(obj).forEach(key => {
    if (String(obj[key]).trim()) {
      params[key] = String(obj[key]).trim()
    }
  })

  ajaxCallback({
    method: 'post',
    url: '/api/list/update',
    data: JSON.stringify(params),
    headers: {
      'Content-Type': 'application/json;charset=utf-8;'
    },
    success: res => {
      if (res.code === 0) {
        // 修改成功，重新渲染数据
        getList(page, pagesize)
      } else {
        alert(res.msg)
      }
    }
  })
})


// promise 版本
function ajax({ url, method = 'get', data, headers = {} }) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(method, url)
    // 设置请求头
    Object.keys(headers).forEach(key => {
      xhr.setRequestHeader(key, headers[key])
    })
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          const data = JSON.parse(xhr.responseText)
          // 成功
          resolve(data)
        } else {
          // 失败
          reject(xhr)
        }
      }
    }
    if (method.toUpperCase() === 'GET') {
      xhr.send(null)
    } else {
      if (data) {
        xhr.send(data)
      } else {
        xhr.send()
      }
    }
  })
}








// 回调函数版
function ajaxCallback({ url, method = 'get', data, headers = {}, success, fail }) {
  const xhr = new XMLHttpRequest()
  xhr.open(method, url)
  // 设置请求头
  Object.keys(headers).forEach(key => {
    xhr.setRequestHeader(key, headers[key])
  })
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        const data = JSON.parse(xhr.responseText)
        // 成功
        success && success(data)
      } else {
        // 失败
        fail && fail(xhr)
      }
    }
  }
  if (method.toUpperCase() === 'GET') {
    xhr.send(null)
  } else {
    if (data) {
      xhr.send(data)
    } else {
      xhr.send()
    }
  }
}






function $(el) {
  return document.querySelector(el)
}