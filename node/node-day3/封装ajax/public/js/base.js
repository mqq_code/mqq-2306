
class Base {
  $ (el, parent) {
    parent = parent || document
    return parent.querySelector(el)
  }
  $gets (el, parent) {
    parent = parent || document
    return [...parent.querySelectorAll(el)]
  }
}