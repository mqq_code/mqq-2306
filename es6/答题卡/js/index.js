
class DateCount {
  constructor({ el, endTime, onFinish }) {
    this.el = el
    this.endTime = endTime
    this.onFinish = onFinish
    this.start()
    this.timer = setInterval(() => {
      this.start()
      if (this.endTime < 0) {
        clearInterval(this.timer)
        this.onFinish && this.onFinish()
      }
    }, 1000)
  }
  start() {
    const h = this.addZero(Math.floor(this.endTime / 1000 / 60 / 60))
    const m = this.addZero(Math.floor(this.endTime / 1000 / 60 % 60))
    const s = this.addZero(Math.floor(this.endTime / 1000 % 60))
    this.el.innerHTML = `${h}:${m}:${s}`
    this.endTime -= 1000
  }
  addZero(n) {
    return n >= 10 ? n : '0' + n
  }
  clear() {
    clearInterval(this.timer)
  }
}


class OnlineTest {
  constructor({ el, data, onSubmit }) {
    this.el = this.$(el, document)
    this.data = data
    this.onSubmit = onSubmit
    this.init()
  }

  init() {
    this.$('h2 span').innerHTML = `(共${this.data.length}题，每题${this.data[0].score}分)`
    this.renderQuestion()
    this.renderCard()
    this.nums = this.gets('.card .num p')
    this.bindEvent()
    this.count = new DateCount({
      el: this.$('.card .date'),
      endTime: 100000,
      onFinish: () => {
        // 倒计时结束
        this.submit()
        this.$('.card .date').innerHTML = '已交卷'
      }
    })
  }

  renderQuestion() {
    const arr = ['A', 'B', 'C', 'D']
    this.$('.list').innerHTML = this.data.map((item, index) => {
      const options = item.options.map((op, i) => {
        return `
          <li>
            <input type="radio" name="${index}" id="op${index}-${i}" />
            <label for="op${index}-${i}">${arr[i]}. ${op}</label>
          </li>
        `
      }).join('')
      return `
        <div class="question">
          <h3>${index + 1}. ${item.question} (${item.score}分)</h3>
          <ul>${options}</ul>
        </div>
      `
    }).join('')
  }

  renderCard() {
    this.$('.card .num').innerHTML = this.data.map((item, index) => {
      return `<p data-index="${index}">${index + 1}</p>`
    }).join('')
  }

  bindEvent() {
    const arr = ['A', 'B', 'C', 'D']
    this.gets('.question').forEach((question, index) => {
      this.gets('input', question).forEach((option, i) => {
        option.addEventListener('change', () => {
          this.nums[index].classList.add('active')
          this.data[index].answer = arr[i]
        })
      })
    })
    this.$('.submit').addEventListener('click', () => {
      // 提交按钮，清除定时器，提交试卷
      this.count.clear()
      this.$('.card .date').innerHTML = '已交卷'
      this.submit()
    })
    this.$('.card .num').addEventListener('click', e => {
      const index = e.target.getAttribute('data-index')
      if (index) {
        // console.log(this.gets('.question')[index], this.gets('.question')[index].offsetTop)
        // 获取指定元素到顶部的距离，修改滚动条元素的 scrollTop 
        document.documentElement.scrollTop = this.gets('.question')[index].offsetTop - 100
        // this.gets('.question')[index].scrollIntoView()
      }
    })
  }

  submit() {
    this.$('.submit').disabled = true
    const score = this.data.reduce((prev, val) => {
      return prev + (val.answer === val.result ? 3 : 0)
    }, 0)
    this.onSubmit && this.onSubmit(score, this.data)
    this.data.forEach((item, index) => {
      if (item.answer === item.result) {
        this.nums[index].style.background = 'green'
      } else {
        this.nums[index].style.background = 'red'
      }
    })
  }

  $(el, parent = this.el) {
    return parent.querySelector(el)
  }
  gets(el, parent = this.el) {
    return [...parent.querySelectorAll(el)]
  }
}


new OnlineTest({
  el: '.container',
  data,
  onSubmit: (score, result) => {
    alert(`考试分数: ${score}`)
    console.log('交卷结果', result)
  }
})


// 吸顶灯
const title = document.querySelector('.title')
const h2 = document.querySelector('.title h2')
window.addEventListener('scroll', () => {
  if (document.documentElement.scrollTop + 100 > title.offsetTop) {
    h2.classList.add('fixed')
  } else {
    h2.classList.remove('fixed')
  }
})
