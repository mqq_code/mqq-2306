### 一、file上传文件
```
<input type="file" multiple>
```
```
multiple 设置多选

通过change事件监听是否上传文件

files 属性获取上传的文件集合
```

### 二、FileReader接口
用来把文件读入内存，并且读取文件中的数据。

```
1. 创建读取文件对象 var f = new FileReader(); 

2. 读取文件 f.readAsDataUrl(file) 或 f.readAsText(file);

3. 获取读取到的结果 f.result;
```

FileReader方法 | 描述
---|---
readAsBlnaryString | 读取为二进制编码
==readAsText== | 读取文本文件的文本内容
==readAsDataURL== | 读取文件的路径


FileReader事件 | 描述
---|---
abort | 读取中断
error | 读取出错
loadstart | 读取开始
progress | 正在读取
==load== | 读取成功
loadend | 读取完成，无论成功失败




