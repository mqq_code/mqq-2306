### 一、拖放事件
###### 1. 三个对象
```
源对象    --  被拖放的元素

过程对象  --  经过的元素

目标对象  --  到达的元素
```
###### 2. 源对象中的事件
要想让某个元素可以拖拽需要设置`draggable="true"`大多数的标签该属性默认值是false，但是img标签和a标签的draggable属性默认是true
```
dragstart 源对象开始拖放

drag 源对象拖放过程中

dragend 源对象拖放结束
```
###### 3. 过程对象中的事件
```
dragenter 源对象开始进入过程对象范围( 鼠标进入 )

dragover 源对象在过程对象范围内移动

dragleave 源对象离开过程对象范围
```
###### 4. 目标对象
```
drop 源对象被拖放到目标对象内
```
==++**pay attention to ! ! !**++== <br> 
- 页面中的元素默认都是过程对象
- 过程对象的dragover事件有一个默认行为！！！
- 那就是当dragover触发时，drop会失效！！！！
- 所以需要阻止dragover的默认行为才能触发drop

###### 5. 简单拖放步骤
```
1. 将源对象draggable属性值置为true,允许元素可拖放

2. 给源对象绑定开始拖放事件,规定当元素被拖动时会发生什么

3. 给过程对象绑定拖动经过事件,规定经过过程对象时会发生什么

4. 阻止目标对象的dragover事件的默认行为

5. 给目标对象绑定(drop)事件 ,规定放开后会发生什么
```
### 二、dataTransfer
==dataTransfer== 是拖放事件对象中的属性,该属性用于源对象和目标对象之间的数据传输。==数据类型不区分大小写==。
```
 1. 在源对象里面存储数据 e.dataTransfer.setData("数据类型","值") 
 
 2. 在目标对象里面获取数据 e.dataTransfer.getData("数据类型") 
 
 3. 在源对象里面清除数据 e.dataTransfer.clearData("数据类型(可省,省则清除所有)")
 
 4. effectAllowed 和 dropEffect 这两个属性结合起来设置拖放的视觉效果  copy、link、move
 
 5. e.dataTransfer.setDragImage(img,100,100);  鼠标拖动时显示的图标
     第一个参数：img对象
     第二个参数：鼠标相对于图片的x方向偏移量
     第三个参数：鼠标相对于图片的y方向偏移量

```