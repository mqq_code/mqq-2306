### 一、音频audio、视频video
#### 1. 如何使用
```
<audio src="horse.mp3" controls loop="loop" autoplay="autoplay">
  <source src="horse.ogg" type="audio/ogg">
  <source src="horse.wav" type="audio/mpeg">
  <source src="horse.acc" type="audio/mpeg">
  您的浏览器版本太low不支持 audio 元素,请更新。
</audio>

1. audio 标签用于存放一个音频文件
2. source 标签用于指定文件的路径和文件的类型
3. 加载多个类型的音频文件是为了兼容不同的浏览器
4. 当浏览器不支持播放音频是可用一段文字提示
```
#### 2. 相关属性和方法

常用属性 | 功能 
---|---
controls | 设置或返回是否显示播放控件
autoplay | 设置或返回是否加载完毕后自动播放
loop | 设置或返回是否循环播放
muted | 设置或返回是否静音
volume | 设置或返回音量
paused | 设置或返回是否暂停
currentTime | 设置或返回当前播放时间 
duration | 设置或返回总时长（秒）

常用API | 功能
---|---
play() | 播放
pause() | 暂停
load() | 重新加载

事件 | 功能
---|---
canplaythrough | 当文件缓冲/加载完成后触发
timeupdate | 当前播放位置更改时触发
ended | 当播放结束时触发