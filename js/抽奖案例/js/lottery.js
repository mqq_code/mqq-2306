function Lottery(options) {
  this.el = this.$(options.el)
  this.data = [...options.data] || []
  this.curIndex = -1
  this.indexList = [0, 1, 2, 5, 8, 7, 6, 3]
  this.onEnd = options.onEnd
  this.init()
}
Lottery.prototype = {
  constructor: Lottery,
  init: function () {
    this.render()
    this.bindEvent()
  },
  render: function() {
    this.data.splice(4, 0, 'btn')
    var list = this.data.map(function (item, i) {
      if (i === 4) {
        return `<li class="centerBtn"><button>抽奖</button></li>`
      }
      return `<li>
        <i>${item.icon}</i>
        <p>${item.title}</p>
      </li>`
    }).join('')
    this.el.innerHTML = `<ul class="list">${list}</ul>`
  },
  bindEvent: function() {
    var list = this.$gets('li', this.el)
    var _this = this
    var count = 0 // 圈数
    var btn = this.$('.centerBtn button', this.el)
    btn.addEventListener('click', function() {
      btn.disabled = true
      btn.style.opacity = 0.4
      // 中奖结果的id
      var resId = _this.getResult()
      // 中奖的下标
      var redIndex = _this.data.findIndex(function(item) {
        return item.id === resId
      })
      var timer = setInterval(function(){
        _this.curIndex++ // 高亮开始转动
        if (_this.curIndex >= _this.indexList.length) {
          _this.curIndex = 0
          count++ // 圈数加1
          console.log(count)
        }
        var index = _this.indexList[_this.curIndex] // 当前高亮奖品下标
        _this.$('.active', _this.el) && _this.$('.active', _this.el).classList.remove('active')
        list[index].classList.add('active')
        if (count >= 4 && index === redIndex) {
          clearInterval(timer)
          count = 0
          _this.onEnd && _this.onEnd(_this.data[redIndex])
          btn.disabled = false
          btn.style.opacity = 1
        }
      }, 100)
    })
  },
  getResult: function() {
    return Math.floor(Math.random() * 8) + 1
  },
  $: function (el, parent) {
    parent = parent || document
    return parent.querySelector(el)
  },
  $gets: function (el, parent) {
    parent = parent || document
    return [...parent.querySelectorAll(el)]
  }
}
