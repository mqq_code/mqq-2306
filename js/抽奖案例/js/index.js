

function toast(options) {
  var params = {}
  if (typeof options === 'string') {
    params.title = options
  } else {
    params = options
  }
  var toastWrap = document.createElement('div')
  toastWrap.style.cssText = `
    position: fixed;
    top: 30%;
    left: 50%;
    transform: translateX(-50%);
    background: rgba(0,0,0,0.8);
    border-radius: 10px;
    padding: 16px;
    min-width: 120px;
    text-align: center;
    color: #ffffff;
  `
  var subtitle = params.subtitle ? `<p>${params.subtitle}</p>` : ''
  toastWrap.innerHTML = `<b>${params.title}</b>${subtitle}`
  document.body.appendChild(toastWrap)
  setTimeout(() => {
    // document.body.removeChild(toastWrap)
    toastWrap.remove()
  }, 3000)
}



let a = new Lottery({
  el: '.app',
  data: data,
  onEnd: function(res) {
    console.log('抽奖结束', res)
    toast({
      title: '恭喜中奖🎉',
      subtitle: `奖品为：${res.icon} ${res.title}`
    })
  }
})

let b = new Lottery({
  el: '.app1',
  data: data,
  onEnd: function(res) {
    console.log('抽奖结束', res)
    toast({
      title: '恭喜中奖🎉',
      subtitle: `奖品为：${res.icon} ${res.title}`
    })
  }
})

// let c = new Lottery({
//   el: '.app2',
//   data: data,
//   onEnd: function(res) {
//     console.log('抽奖结束', res)
//   }
// })
