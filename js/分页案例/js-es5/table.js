
// table表格
function Table(options) {
  this.el = this.$(options.el) // 渲染的元素
  this.columns = options.columns || [] // 渲染表头的数据
  this.data = options.data || [] // 渲染的内容数据
  this.init()
}
// 继承
Table.prototype = new Base()
// 初始化
Table.prototype.init = function () {
  this.el.innerHTML = `
    <table>
      <thead>
        <tr></tr>
      </thead>
      <tbody></tbody>
    </table>
  `
  this.renderHead()
  this.renderData()
}
// 渲染表头
Table.prototype.renderHead = function () {
  this.$('thead tr', this.el).innerHTML = this.columns.map(function(item) {
    return `<th>${item.title} - ${item.key}</th>`
  }).join('')
}
// 渲染内容
Table.prototype.renderData = function () {
  var _this = this
  // 遍历 data 生成每一行数据
  this.$('tbody', this.el).innerHTML = this.data.map(function (row) {
    // 遍历表头渲染当前行每一个单元格的内容
    var tds = _this.columns.map(function(val) {
      if (typeof val.render === 'function') {
        return `<td>${val.render(row)}</td>`
      }
      return `<td>${row[val.key]}</td>`
    }).join('')

    return `<tr>
      ${tds}
    </tr>`
  }).join('')
}
// 更新内容
Table.prototype.updateData = function (data) {
  this.data = data
  this.renderData()
}
