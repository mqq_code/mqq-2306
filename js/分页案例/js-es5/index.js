

var page = 1
var pagesize = 5
// 1  5   data.slice(0, 5)
// 2  5   data.slice(5, 10)
// 3  5   data.slice(10, 15)
// data.slice(page * pagesize - pagesize, page * pagesize)

var table = new Table({
  el: '.table1', // 要渲染的元素
  columns: [
    {
      title: '学号',
      key: 'id'
    },
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '年龄',
      key: 'age'
    },
    {
      title: '性别',
      key: 'sex'
    },
    {
      title: '分数',
      key: 'num',
      render: function (row) {
        if (row.num >= 90) {
          return `<b style="color: green">${row.num}</b>`
        } else if (row.num >= 60 && row.num < 90) {
          return `<b style="color: orange">${row.num}</b>`
        } else {
          return `<b style="color: red">${row.num} 不及格</b>`
        }
      }
    },
    {
      title: '操作',
      key: 'action',
      render: function (row) {
        return `
          <button data-info="${JSON.stringify(row)}">修改</button>
          <button class="del" data-id="${row.id}">删除</button>
        `
      }
    }
  ], // 渲染表头的数据
  data: data.slice(page * pagesize - pagesize, page * pagesize) // 渲染多少行
})

var page1 = new Pagination({
  el: '.page1',
  total: data.length,
  page: page,
  pagesize: pagesize,
  onChange: function(curpage, curpageisze) {
    console.log('分页改变了', curpage, curpageisze)
    page = curpage
    pagesize = curpageisze
    // 更新table数据
    table.updateData(data.slice(page * pagesize - pagesize, page * pagesize))
  }
})

var table1 = document.querySelector('.table1')
table1.addEventListener('click', function(e) {
  if (e.target.nodeName === 'BUTTON' && e.target.classList.contains('del')) {
    var id = e.target.getAttribute('data-id')
    var index = data.findIndex(function (item, index, array) {
      return item.id === id * 1
    })
    data.splice(index, 1)
    table.updateData(data.slice(page * pagesize - pagesize, page * pagesize))
    page1.updateTotal(data.length)
  }
})


// new Table({
//   el: '.table2', // 要渲染的元素
//   columns: [
//     {
//       title: '股票名称',
//       key: 'code'
//     },
//     {
//       title: '股票价格',
//       key: 'price'
//     }
//   ], // 渲染表头的数据
//   data: [
//     {
//       code: '恒大',
//       price: 100
//     },
//     {
//       code: '阿里',
//       price: 120
//     },
//     {
//       code: '腾讯',
//       price: 90
//     }
//   ] // 渲染多少行
// })




