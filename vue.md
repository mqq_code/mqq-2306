## Vue 
1. MVVM 模式
- M: model 模型、数据
- V: view 视图
- VM: viewModel 视图模型、连接数据和视图
2. 数据驱动视图改变
3. 组件化开发
4. 响应式数据，数据改变页面自动更新

### 声明式渲染
```html
  <!-- 在模版的 {{ }} 内可以渲染表达式，不可以渲染语句 -->
  <div>{{ title }}</div>
```

### 指令
> 带有前缀 v- 的特殊属性，提供底层 dom 操作
1. v-text 类似 js 中的 innerText
2. v-html 类似 js 中的 innerHTML
3. v-if、v-else-if、v-else 条件渲染，会添加删除dom元素
4. v-show 条件渲染，会添加删除元素的 display: none
5. v-for 渲染列表，可以渲染 数组、对象、数字、字符串
6. v-on 绑定事件 简写 @
> 事件修饰符: stop、prevent、once、self、capture、
7. v-bind: 让标签的属性可以使用变量,简写 : , style 和 class 可以传入对象和数组
8. v-model 表单双向绑定
- 修饰符：lazy、number、trim
9. v-slot: 具名插槽

### 生命周期
1. 创建阶段
- beforeCreate: 实例创建之前，vue 相关数据和方法都不可用
- created: 实例创建完成，可以使用数据和方法，在函数内可以请求接口
2. 挂载阶段
- beforeMount: 组件挂载之前，可以使用数据和方法，不可以操作dom
- mounted: 组件挂载完成，可以操作dom
3. 更新阶段 (数据修改触发更新阶段执行)
- beforeUpdate: 数据更新后，页面更新前，无法获取到最新的dom
- updated: 页面更新完成，可以获取到最新的dom
4. 销毁阶段
- beforeDestroy: 组件销毁前，组件实例在此阶段还可以使用但是修改数据不会再执行更新，可以清除异步操作（定时器，dom原生事件）
- destroyed: 组件销毁后

### computed 计算属性
> 使用场景: 当一个变量是由其他变量计算生成时使用（例如: 计算总价、从原数据筛选新数据）
1. 计算属性的用法和 data 中的变量一致，但是不可修改，计算属性必须有返回值
2. 计算属性有缓存，当计算属性内依赖的响应式数据没有发生改变时直接从缓存中读取值，依赖项改变才会执行函数重新计算结果
3. 计算属性默认是不可以直接赋值修改，需要通过修改依赖项更新数据，如果想要给计算属性赋值可以添加 get 和 set 函数

### watch
> 使用场景: 监听变量改变执行对应的函数
1. 如果监听的变量是引用类型，无法监听到属性的修改，想要监听深层次数据的更新可以使用深度监听：deep: true
2. 进入页面立即执行一次函数: immediate: true

### data 组件数据
1. 组件中的data必须是一个函数：防止组件复用时组件中的数据互相影响，每次调用组件都会执行 data 函数重新创建新数据

### 组件传参
1. 父 -> 子：父组件通过 props 传给子组件，子组件通过 props 属性接收
2. 子 -> 父：子组件通过 $emit('事件名', 参数) 调用父组件传过来的自定义事件，把数据通过事件的参数传给父组件
3. provide\inject: 跨级传参，父级组件通过 provide 给后代组件提供数据，后代组件使用 inject 获取数据
4. 无关系组件: eventBus（发布订阅模式）

### 插槽
> 使用场景：当组件内的某一部分内容调用时才能确定具体内容时可以使用 `<slot></slot>` 占位，调用组件时组件双标签内的具体元素会替换 slot
1. 默认插槽
```html
<!-- Child.vue -->
<div>
  <h3>child组件</h3>
  <slot></slot>
</div>

<!-- 父组件 -->
<div>
  <Child>
    <p>此标签会替换 child.vue 中的 slot </p>
  </Child>
</div>
```
2. 具名插槽
- 组件内容可以使用多个 slot，通过 name 属性定义名字
3. 作用域插槽
- 在插槽内使用组件中的变量

### 组件上的 v-model 和 .sync 修饰符
>  v-model 和 .sync 都是语法糖
1. v-model
```html
<!-- 完整写法 -->
<Button :value="num" @input="num = $event" />
<!-- 使用 v-model 简写 -->
<Button v-model="num" />
```
2. .sync
```html
<!-- 完整写法 -->
<Button :count="num" @update:count="num = $event" />
<!-- 使用 v-model 简写 -->
<Button count.sync="num" />
```

### keep-alive
> 缓存组件状态，缓存的组件不会销毁
1. 缓存的组件有两个特定的生命周期
- activated: 组件显示时执行
- deactivated: 组件隐藏时执行

### 动态组件
> 需要切换多个组件时使用，可以代替多个 v-if 切换组件
```html
<component :is="组件名"></component>
```

### 异步组件
> 可以用来性能优化，打包时把该组件打包成单独的 js 文件，再调用该组件时再动态加载对应的 js
```js
export default {
  components: {
    Button: () => import('组件路径')
  }
}
```

### mixin 混入
> 抽离组件中的公用逻辑，组件中使用时会把混入的属性、方法和当前组件进行合并，会执行混入的内容
```js
// a.js 
export default {
  data () {
    return {
      num: 10
    }
  },
  created () {}
}

// app.vue
import a from './a.js'

export default {
  // 把 a.js 中定义的变量和方法，合并到当前组件
  mixins: [a],
  data () {
    return {
      title: 'title'
    }
  }
}
```

### 自定义指令
```js
Vue.directive('自定义指令名', {
  bind (el, binding) { // 指令绑定到元素时执行，无法获取父元素
    // el: 绑定指令的元素
    // binging: 绑定指令的信息
  },
  inserted (el, binding) { // 元素插入到父节点时执行，可以获取父元素，但是不保证父元素在页面
  },
  update (el, { value, oldValue }) { // 组件更新时执行
  },
  componentUpdated () { // 组件更新完成执行，可以获取到最新的dom
  },
  unbind (el, binding) { // 只调用一次，指令与元素解绑时调用。
  }
})
```

### 注册插件
```js
Vue.use(plugin)
// plugin: 如果插件是一个对象，必须提供 install 方法。如果插件是一个函数，它会被作为 install 方法。install 方法调用时，会将 Vue 作为参数传入。
// 该方法需要在调用 new Vue() 之前被调用。
```

### $set
> 给对象添加新的响应式属性\通过数组下标修改数据
```js
this.$set(对象, key, value)
this.$set(数组, index, value)
```

### $nextTick
> 在下次 DOM 更新循环结束之后执行延迟回调。在修改数据之后立即使用这个方法，获取更新后的 DOM。
```js
this.$nextTick(() => {
  // 页面更新后执行此函数
})
```

### $refs
> 获取 dom 元素或者获取子组件实例对象
```html
<div>
  <h1 ref="title">标题</h1>
  <Child ref="child"></Child>
</div>
```
```js
export default {
  mounted () {
    this.$refs.title // h1 dom元素
    this.$refs.child // 子组件实例对象，可以调用子组件内的数据和方法
  }
}
```

### 路由配置
```js
new VueRuter({
  routes: [
    {
      path: '路径',
      component: '当前地址匹配 path 成功时渲染的组件',
      name: '路由名称',
      redirect: '重定向的path',
      meta: {
        // 自定义数据，在组件内的 $route 可以获取
      },
      children: [
        // 子路由
      ]
    }
  ]
})
```

### 路由跳转
1. 标签跳转 `<router-link to="path"></router-link>`
2. js 跳转
```js
this.$router.push('path')
this.$router.replace('path')
```

### 路由传参
1. query 传参数，url ? 后拼接参数
```js
// 传参数
this.$router.push({
  path: 'path',
  query: {
    // 参数
  }
})

// 获取query
this.$route.query
```
2. 动态路由(必须先配置)
```js
// 传参数
this.$router.push({
  name: 'name',
  params: {
    // 参数
  }
})

// 获取动态路由参数
this.$route.params
```

### 路由守卫
1. 全局守卫: 所有路由跳转时都会执行
- beforeEach: 全局前置钩子
- beforeResolve: 全局解析守卫
- afterEach: 全局后置钩子
2. 路由独享守卫: 只针对特定的路由进行拦截
- beforeEnter
3. 组件内守卫
```js
export default {
  beforeRouteEnter(to, from, next) {
    // 在渲染该组件的对应路由被 confirm 前调用
    // 不！能！获取组件实例 `this`
    // 因为当守卫执行前，组件实例还没被创建
  },
  beforeRouteUpdate(to, from, next) {
    // 在当前路由改变，但是该组件被复用时调用
    // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，
    // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
    // 可以访问组件实例 `this`
  },
  beforeRouteLeave(to, from, next) {
    // 导航离开该组件的对应路由时调用
    // 可以访问组件实例 `this`
  }
}
```

### $router 和 $route 区别
1. $router: 路由实例对象，可以跳转页面
2. $route: 当前路由信息，包含当前地址，路由参数...

